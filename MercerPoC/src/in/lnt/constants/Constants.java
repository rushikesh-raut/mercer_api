package in.lnt.constants;

public interface Constants {

	public static int ENDPOINT_DEFAULT = -1;
	public static int ENDPOINT_UPLOAD_MULTIPLE_FILE = 1;
	public static int ENDPOINT_VALIDATE_AGGREGATES = 2;
	public static int ENDPOINT_CROSS_SECTION_CHECKS = 3;
	public static int ENDPOINT_PERFORM_DEFAULT_ACTIONS = 4;
	public static String COLUMNS = "columns";
	public static String VALIDATIONS = "validations";
	public static String VALIDATION_TYPE = "validationType";
	public static String DATA_TYPE = "dataType";
	public static String VALIDATION_ERRORS = "validationErrors";
	public static String VALIDATION_RESULTS = "validationResults";
	public static String EXPRESSION = "expression";
	public static String ERROR_TYPE = "errorType";
	public static String DEPENDENT_COLUMNS = "dependentColumns";
	public static String DEPENDENT_COLUMN = "dependentColumn";
	public static String ISMDAREQUEST = "isMDARequest";
	public static String ERROR_GROUP = "errorGroup";
	public static String PERSONAL = "Personal";
	public static String DISPLAYLABEL = "displayLabel";
	public static String AGGREGATE_DISPLAY_LABEL = "aggregateDisplayLabel";
	public static String MEDIAN_PAY = "Median Pay";
	public static String CATEGORY = "category";
	public static String PARAMETER = "parameter";
	public static String JOB_INVERSION = "job_inversion";
	public static String ERROR = "ERROR";
	public static String ALERT = "ALERT";
	public static String REQUIRED = "REQUIRED";
	public static String AUTOCORRECT = "AUTOCORRECT";
	public static String PHONE = "PHONE";
	public static String EMAIL = "EMAIL";
	public static String AGGREGATE = "AGGREGATE";
	public static String FIELD = "field";
	public static String ANNUAL_BASE = "ANNUAL_BASE";
	public static String MESSAGE = "message";
	public static String MESSAGE_KEY = "messageKey";
	public static String JOB_INVERSION_ERRORS = "Job Inversion Errors";
	public static String CODE = "code";
	public static String QUESTION_TYPE = "questionType";
	public static String DROPDOWN = "dropdown";
	public static String RADIO_BUTTONS = "radio_buttons";
	public static String REPLACE_FROM = " and ";
	public static String REPLACE_TO = " && ";
	public static String OTHERSECTIONDATAKEY = "otherSectionsData";
	public static String CONTEXTDATAKEY = "contextData";
	public static String ENTITIES = "entities";
	public static String CONTEXTDATA_DATEFORMAT = ".dateFormat";
	public static String CONTEXTDATA_NUMBERFORMAT = ".numberFormat";
	public static String DATA = "data";
	public static String SECTION_STRUCTURE = "sectionStructure";
	public static String EXPRESSION_STRING = "EXPRESSIONSTRING";
	public static String ENHANCEMENT = "ENHANCEMENT";
	public static String SECTIONS = "sections";
	public static String SECTIONCODE = "sectionCode";
	public static String MAPPED_COLUMN_NAME = "mappedColumnName";
	public static String BASE_FIELD = "baseField";
	public static String MESSAGE_PARAMS = "messageParams";
	// public static String EXP_PATTERN =
	// "(\\\\s*)?(this|contextData)(.[\\w.!@#$%^&*()_~+]*)?";
	// public static String EXP_PATTERN =
	// "(\\s*)?(this|contextData)(.[\\w.!@#$%^&*()_~+]*)?";
	public static String EXP_PATTERN = "(this|contextData)(.[a-zA-Z0-9_.!@#$%^&*]*)";
	// public static String
	// EXP_PATTERN="this.[\\w\\s*]+\\.[\\w\\s*]+\\.[\\w\\s*\\$*\\#*]+|this.[\\w\\s*\\$*\\#*\\@*\\%*\\^*\\-*]+\\.[\\[*\\'*\\.\\w\\s*\\'*\\]*\\$*\\#*\\@*\\%*\\^*\\-*]+|this.[\\w\\s*\\$*\\#*\\@*\\%*\\^*\\-*]+";
	public static String EXP_PATTERN_CHECKBOX = "(this|contextData)(.[a-zA-Z0-9_.!@#$%^&*]*)";

	public static String INT_PATTERN = "^([+-]?[0-9]\\d*|0)$";
	public static String DOUBLE_PATTERN = "\\d+\\.\\d+";
	public static String LONG_PATTERN = "^-?\\d{1,19}$";
	public static String CURLY_BRACES = "\\{|\\}";
	public static String SKIP = "skip";
	public static String ID = "_id";
	public static String MDA = "mda";
	public static String EXECLUE_ROW = "exclude_row";
	public static String CLEAR_VALUE = "clear_value";
	//public static String EXCLUDE_FLAG = "EXCLUDE_FLAG";
	public static String EXCLUDE_FLAG = "SYSTEM_EXCLUDE_EMPLOYEE"; //Value changed for PE-7771
	public static String DUPLICATE = "Duplicate";
	public static String MISSING = "Missing";
	public static String EMPLOYEE_EEID = "YOUR_EEID";
	public static String EEID_FORMAT = "yyyyMMddSSSSSmm";
	public static String EEID_FORMAT2 = "yyyyMMddhh";
	public static String ORIGINAL_VALUE = "OriginalValue";
	public static String BLANK = "";
	public static String DOT = ".";
	public static String MINIMUM_ERROR = "minError";
	public static String MAXIMUM_ERROR = "maxError";
	public static String MINIMIUM_ALERT = "minAlert";
	public static String MAXIMUM_ALERT = "maxAlert";
	public static String VALIDATION_RANGE = "range";
	public static String REFERENCE_TABLE_NAME = "referenceTableName";
	public static String RANGE_VALIDATION_REF_TABLE = "rangeValidationRefTable";
	public static String ERROR_MIN = "ERROR_MIN";
	public static String ERROR_MIN_MSG = "value is less than minimum required Error value";
	public static String ALERT_MIN = "ALERT_MIN";
	public static String ALERT_MIN_MSG = "value is less than minimum required Alert value";
	public static String ALERT_MAX = "ALERT_MAX";
	public static String ALERT_MAX_MSG = "value is greater than maximum required Alert value";
	public static String ERROR_MAX = "ERROR_MAX";
	public static String ERROR_MAX_MSG = "value is greater than maximum required Error value";

	public static int DATA_LEVEL1 = 0;

	/**************** ERRORS ****************/
	public static String INVALID_DATATYPE_FOR_ERROR = "Invalid datatype found for errortype 'ERROR'";
	public static String MANDATORY_VALIDATIONTYPE = "Mandatory attribute validationType is required.";
	/* CrossSectionChecks */
	public static String ERROR_CANNOT_FIND_VARIABLE_IN_EXPR = "Cannot find variable '%s' in expression '%s'.";
	public static String ERROR_N_VALUES_FOUND_FOR_VARIABLE_IN_EXPR = "%s values found for %s in expression %s";
	public static String INVALID_FILE = "Invalid file type , only csv , json and xlsx expected.";
	public static String INVALID_INPUT_JSON = "Invalid input JSON";

	// Logger constants..
	public static String LOG_REQJSON = " Requested json....%s";
	public static String LOG_METHOD_UPLOADMULTIPLEFILEHANDLER = "  >> uploadMultipleFileHandler:";
	public static String LOG_METHOD_MERCERDEFINEDACTIONSHANDLER = "  >> mercerDefinedActionsHandler:";
	public static String LOG_METHOD_PERFORMDEFAULTACTIONS = "  >> performDefaultActions:";
	public static String LOG_METHOD_VALIDATEFORM = "  >> validateForm:";
	public static String LOG_METHOD_DOWNLOAD = "  >> downLoadData:";
	public static String LOG_PARSEANDVALIDATE = "  >> parseAndValidate:";
	public static String LOG_PARSEJSONOBJECTMAP = "  >> parseJsonObjectMap:";
	public static String LOG_VALIDATE = "  >> validate:";
	public static String LOG_EXPRESSIONEVALUATOR = "  >> expressionEvaluator:";
	public static String LOG_CHECKMANDATORYFIELDS = "  >> checkMandatoryFields:";
	public static String LOG_CHECKDATATYPE = "  >> checkDataType:";
	public static String LOG_CHECKVALIDATIONTYPEONEOF = "  >> checkValidationTypeOneOf:";
	public static String LOG_PREPAREOUTPUTJSON = "  >> prepareOutputJson:";
	public static String LOG_CHECKUNIQUENESSFOREEID = "  >> checkUniqueNessForEEID:";
	public static String LOG_PREPAREEEIDAUTOCORRECTOBJECT = "  >> prepareEeidAutoCorrectObject:";
	public static String LOG_GENERATEUNIQUEEID = "  >> generateUniqueEeid:";
	public static String LOG_PARSECSVFILE = "  >> parseFile:";
	public static String LOG_PARSEXSLXFILE = "  >> prseXslxFile:";
	public static String LOG_PREPAREREFENCERANGEOBJECT = "  >> prepareRefernceRangeObject:";
	public static String LOG_PERFORML0VALIDATION = "  >> performL0Validation:";
	public static String LOG_CONVERXLSXTOMAP = "  >> convertXlsxToMap:";
	public static String LOG_CROSSSECTIONCHECK = "  >> crossSectionCheck:";

	public static String INTIATE = "Executing..";
	public static String EXIT = "Exiting..";

	public static final int HTTPSTATUS_422 = 422;
	public static final int HTTPSTATUS_400 = 400;

	public static final int HTTPSTATUS_500 = 500;
	
	/*public static final String ERROR_1_ENTITY_SHOULD_BE_IN_CSV_FILE = "There should be only 1 number of entity for CSV file.";
	public static final String ERROR_ENTITIES_NUMBER_DONT_MATCH_SHEETS_NUMBER = "Number of entities(%s) do not match with number of sheets(%s).";
	public static final String ERROR_REPEATED_ENTITY_FOR_COMPANY_COUNTRY = "Repeated entity for Company Code : %s & Country Code : %s";
	public static final String ERROR_SHEET_NOT_FOUND = "Sheet %s not found.";
	public static final String ERROR_ENTITY_SEQUENCE_DONT_MATCH_WITH_SHEETS = "Entities do not match with Sheet sequence.";
	public static final String ERROR_ENTITIESLIST_DONT_MATCH_SHEETLIST = "Entities list do not match the sheets list.";
	public static final String DUPLICATE_EEID="Duplicate Employee ID's exist within this file, please correct this issue and try again.";
	public static final String MISSING_EEID="Employee ID information not found,  please correct this issue and try again.";*/
	public static final String ERROR_TABLE_NOTEXIST = " Missing  table name";
	public static final String ERROR_LOOKUP_COLUMN_MISSING_IN_DB = "Lookup column is missing in the  (%s) table for range validation.";
	// PE -5568
	public static final String ANSWER_YES = "Y";
	public static final String ANSWER_NO = "N";
	public static final String PARAMS = "_Params";
	public static final String OPTIONS = "options";  
	public static final String REFERENCETABLE = "referenceTable";
	public static final String DIMENSIONS = "dimensions";
	public static final String DIMENSION = "dimension";
	public static String CHECKBOXES = "checkboxes";
	public static final String SEQUENTIAL_CHECK = "sequentialCheck";
	public static final String INT = "int";
	public static final String DATE = "date";//added for 8421
	public static final String INTEGER = "integer";
	public static final String BIGINT = "bigint";
	public static final String DOUBLE = "double";
	public static final String FLOAT = "float";
	public static final String ENTITY_NAME_COLUMN_CODE="entityNameColumnCode";
	public static final String ENTITY_COUNTRY_COLUMN_CODE="entityCountryColumnCode";  
	public static final String MAPPED_COMPANY = "mappedCompany";
	public static final String INVALID_EXP = "invalid expression";
	public static final String PERCENTAGE = "percentage";
	public static final String HIDDEN = "hidden";
	public static final String COTEXT_COUNTRY_CODE = "contextData.ctryCode";
	public static final String COTEXT_INDUSTRY_SECTOR = "contextData.industry.sector";
	public static final String COTEXT_INDUSTRY_SUPERSECTOR = "contextData.industry.superSector";
	public static final String COTEXT_INDUSTRY_SUBERSECTOR = "contextData.industry.subSector";
	
}