package in.lnt.exceptions;

import java.util.ArrayList;
import java.util.List;

public class CustomStatusException extends Exception{
	private static final long serialVersionUID = 1L;

	private int  httpStatusCode; 	// status: the HTTP status code
	private String message; 		// message: the error message associated with exception
	private Throwable detail; 		// error: List of constructed error messages
	private String params;
	private List<String> paramValues;
	
	public CustomStatusException() {
		
	}
	public CustomStatusException(int statusCode, String errorMessage) {
		this.httpStatusCode = statusCode;
		this.message = errorMessage;
	}
	public CustomStatusException(int statusCode, String errorMessage,String errorParams,ArrayList<String> paramValues) {
		this.httpStatusCode = statusCode;
		this.message = errorMessage;
		this.params = errorParams;
		this.paramValues=paramValues;
	}
	
	public CustomStatusException(int statusCode, Throwable statusDescription, String errorMessage) {
		this.httpStatusCode = statusCode;
		this.detail = statusDescription;
		this.message = errorMessage;
	}

	/**
	 * @return the _httpStatus
	 */
	public int getHttpStatus() {
		return httpStatusCode;
	}

	/**
	 * @param _httpStatus the httpStatusCode to set
	 */
	public void setHttpStatus(int httpStatus) {
		httpStatusCode = httpStatus;
	}

	/**
	 * @return the _message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the _message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the detail
	 */
	public Throwable getDetail() {
		return detail;
	}

	/**
	 * @param statusDescription the detail to set
	 */
	public void setDetail(Throwable statusDescription) {
		detail = statusDescription;
	}
	
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	
	public List<String> getParamValues() {
		return paramValues;
	}
	public void setParamValues(List<String> paramValues) {
		this.paramValues = paramValues;
	}

	@Override
	/*public String toString(){
		return  getHttpStatus()+ ", "+getMessage()+", "+getDetail();
	}*/
	public String toString(){
		return  getHttpStatus()+ ", "+getMessage()+", "+getDetail();
	}

}
