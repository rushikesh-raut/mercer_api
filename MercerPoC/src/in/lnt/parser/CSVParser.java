package in.lnt.parser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;

public class CSVParser {
	
	private static final Logger logger = LoggerFactory.getLogger(CSVParser.class);
	
	public static ArrayList<String[]> dataForAggregation = null;
	public static final int MAX_COLMNS = 1600;
	

	public static HashMap<String, Object> parseFile(InputStream inputStreamCSV){
		List<String> header = new ArrayList<String>();
		HashMap<String,Object> map = new HashMap<String,Object>();
		List<ArrayList<String>> rows = new ArrayList<ArrayList<String>>();
		List<HashMap<String, String>> allRecords = new ArrayList<HashMap<String,String>>();
		HashSet<String> eeidSet = new HashSet<String>();
		boolean eeidFlag=true;
		//boolean eeidColFlag=false;
		
		// TODO Auto-generated method stub
		CsvParserSettings settings = new CsvParserSettings();
		settings.getFormat().setLineSeparator("\n");
		
		//Set max no of columns from 512 to MAX_COLUMNS 
		settings.setMaxColumns(MAX_COLMNS);
		
		// creates a CSV parser
		CsvParser parser = new CsvParser(settings);
		
		// parses all rows in one go.
		try {
			List<String[]> allRows = parser.parseAll(inputStreamCSV);
			dataForAggregation = (ArrayList<String[]>) allRows;
			int count = 0;
			for (String[] row : allRows) {
				if (count == 0)
					header = Arrays.asList(row);
				else if(count == 1){ 	
				}
				else {
					ArrayList<String> tempRow = new ArrayList<String>(Arrays.asList(row));
					rows.add(tempRow);
				}
				count++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			for (ArrayList<String> row : rows) {
				 
				HashMap<String,String> record = new HashMap<String,String>(); 
				for (int i = 0; i < header.size(); i++) {
					 
					if(header.get(i).equalsIgnoreCase(Constants.EMPLOYEE_EEID))
					{
						try{
						if(row.get(i)!=null && !row.get(i).equals(""))
							eeidFlag = eeidSet.add(row.get(i));
						}
						catch(IndexOutOfBoundsException e)
						{
							eeidFlag=true;
						}
					}
					if(eeidFlag)
						try{
					record.put(header.get(i),  (!StringUtils.isEmpty(row.get(i)) ? row.get(i) : ""));
						}
					catch(IndexOutOfBoundsException e)
					{
						record.put(header.get(i), "");
					}
					else 
						{
						map.put("isEeidDuplicate", "true");
						eeidSet=null;
						return map;
						}
					
				}
				allRecords.add(record);
			}
			map.put("allRecords", allRecords);
			eeidSet=null;
		return map;
	}
	public static HashMap<String, Object> parseFile(InputStream inputStreamCSV,boolean flag) throws Exception{
		logger.info(Constants.INTIATE + Constants.LOG_PARSECSVFILE);
		HashMap<String,Object> map = new HashMap<String,Object>();
		List<String> header = new ArrayList<String>();
		List<ArrayList<String>> rows = new ArrayList<ArrayList<String>>();
		List<HashMap<String, String>> allRecords = new ArrayList<HashMap<String,String>>();
		HashSet<String> eeidSet = new HashSet<String>();
		boolean eeidFlag=true;
		ArrayList<String> headerList = null;
		boolean eeidColFlag = false;
		CsvParserSettings settings = new CsvParserSettings();
		settings.getFormat().setLineSeparator("\n");
		
		//Due to ML Integration need to increase csv column from 512 to  1600.
		settings.setMaxColumns(MAX_COLMNS);
		// creates a CSV parser
		CsvParser parser = new CsvParser(settings);
		String[] blankRow = null;
		String[] nullRow = null;
		// parses all rows in one go.
			
			List<String[]> allRows = parser.parseAll(inputStreamCSV);
			logger.info("allRows......."+allRows);
			dataForAggregation = (ArrayList<String[]>) allRows;
			int count = 0;
			for (String[] row : allRows) {
				if (count == 0)
				{
					header = Arrays.asList(row);
					logger.info("header......."+header);
					//PE-8575: Reject the file in case of Header is missing for one or multiple columns
					if(header.contains(null))
					{
						throw new CustomStatusException(Constants.HTTPSTATUS_400,
								Cache.getPropertyFromError(ErrorCacheConstant.ERR_064));
					}
					nullRow = new String[header.size()];
					blankRow = new String[header.size()];
					for(int c = 0; c<header.size(); c++) {
						blankRow[c] = "";
					}

				}
				else if(count == 1){ 	
				}
				else {
					if(null!=blankRow && !Arrays.equals(blankRow,row) 
							&& null!=nullRow && !Arrays.equals(nullRow,row)) {
						ArrayList<String> tempRow = new ArrayList<String>(Arrays.asList(row));
						rows.add(tempRow);
					}
				}
				count++;
			}
		for (ArrayList<String> row : rows) {

			HashMap<String, String> record = new HashMap<String, String>();
			for (int i = 0; i < header.size(); i++) {

				// PE : 2449 to check duplication of EEID in CSV
				if (header.get(i).equalsIgnoreCase(Constants.EMPLOYEE_EEID)) {
					eeidColFlag = true;
					try {
						if (row.get(i) != null && !row.get(i).equals(Constants.BLANK))
							eeidFlag = eeidSet.add(row.get(i));
					} catch (IndexOutOfBoundsException e) {
						eeidFlag = true;
					}
				}
				if (eeidFlag)
					try {
						// PE-6874
						record.put(header.get(i), (!StringUtils.isEmpty(row.get(i)) ? row.get(i) : null));
					} catch (IndexOutOfBoundsException e) {
						record.put(header.get(i), Constants.BLANK);
					}
				else {
					map.put("isEeidDuplicate", "true");
					//PE-8690
					map.put("eeidColFlag", eeidColFlag);
					eeidSet = null;
					return map;
				}
			}
			if (!eeidColFlag) {
				headerList = new ArrayList<String>();
				headerList.addAll(header);
				headerList.add(Constants.EMPLOYEE_EEID);
			}
			allRecords.add(record);
		}
			if(headerList!=null)
			{
				map.put("header", headerList);
			}
			else
			{
				map.put("header", header);
			}
			//PE-8690
			map.put("eeidColFlag", eeidColFlag);
			map.put("allRecords", allRecords);
			eeidSet=null;
			logger.info("map......"+map);
			logger.info(Constants.EXIT + Constants.LOG_PARSECSVFILE);
		return map;
	}
	
/*	public static Reader getReader(String relativePath) {
		File initialFile = new File(relativePath);
		try {
			InputStream targetStream = new FileInputStream(initialFile);
			Reader inputStreamReader = new InputStreamReader(targetStream);
			return inputStreamReader;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}*/
}
