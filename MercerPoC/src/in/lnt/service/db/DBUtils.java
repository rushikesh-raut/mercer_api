package in.lnt.service.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.derby.MercerDBCP;
import com.lti.mosaic.function.def.CustomFunctions;
import in.lnt.constants.Constants;

public class DBUtils {

	private static final Logger logger = LoggerFactory.getLogger(DBUtils.class); 
	
	
	public static List<String> getRefTableMetaData(String tableName) throws SQLException, IOException
	{
		List<String> list = new ArrayList<>();
		
		Connection connection = null;
		Statement stmt =null;
		ResultSet rs = null;
		
		ResultSetMetaData rsmetaData=null;
		int columnCount = 0;
		String query="SELECT * FROM "+tableName+ CustomFunctions.FIRST_ROW;
		
		long startTime = 0;
			try {
				startTime  = System.currentTimeMillis();
				
				connection = MercerDBCP.getDataSource().getConnection();
				stmt = connection.createStatement();
				
				rs = stmt.executeQuery(query);	
				rsmetaData = rs.getMetaData();
				
				columnCount = rsmetaData.getColumnCount();
				for(int i=1;i<=columnCount;i++)
				{
					list.add(rsmetaData.getColumnName(i));
				}
				
				logger.debug("Time taken for creating connection and getting result statement for getRefTableMetaData() "
						+ "in DBUtiils..", (System.currentTimeMillis() - startTime));
			} catch (SQLException ex) {
				logger.error("Exception occured in getRefTableMetaData.."+ex.getMessage());
				throw ex;
			}
			finally
			{
				if(rs!=null)
				{
					rs.close();
				}
				if(stmt!=null)
				{
					stmt.close();
				}
				if (connection != null) {
	              try {
	                connection.close();
	              } catch(Exception e) {
	                logger.error("Error while closing connection {}",e);
	              }
	            }
			}
		return list;
	}
	
	/**
	 * 
	 * @param tableName
	 * @return Map with column name and respective data types
	 * @throws SQLException
	 * @throws IOException
	 */
	
	public static Map<String,String> getTableSchemaData(String tableName) throws SQLException, IOException
	{
		Map<String,String> metaMap = new HashMap<>();
		
		// DB Connection Object
		Connection dbConnection = null;
		//Result Set MetaData Object
		ResultSetMetaData rsmd =null;
		//Statement Object
		Statement statementObj =null;
		//Result Set Object
		ResultSet resultSet = null;
		
		long startTime = 0;
		
		String query = " SELECT * FROM "+tableName+ " "+CustomFunctions.FIRST_ROW;
		try {
				//Derby Start time
				startTime  = System.currentTimeMillis();
				
				//Create DataBase connection
				dbConnection = MercerDBCP.getDataSource().getConnection();
				
				 //Using query base approach
				 statementObj = dbConnection.createStatement(); 
				 resultSet = statementObj.executeQuery(query);
				 rsmd = resultSet.getMetaData();
				 
				 for(int j = 1 ; j <= rsmd.getColumnCount(); j++)
				    {
				      //System.out.print(rsmd.getColumnName(j) + "  " + rsmd.getColumnType(j));
				      getActualDataType(metaMap, rsmd, j);                                   
				    }
				logger.debug("Time taken for creating connection and getting result statement for getRefTableMetaData() "
						+ "in DBUtiils..", (System.currentTimeMillis() - startTime));
			} catch (SQLException ex) {
				logger.error("Exception occured in getRefTableMetaData.."+ex.getMessage());
				throw ex;
			}
			finally
			{
				if(null != resultSet)
				{
					resultSet.close();
				}
				if(null != rsmd) {
					rsmd = null;
				}
				if ( null != statementObj ) {
					statementObj.close();
				}
				if (dbConnection != null) {
	              try {
	                dbConnection.close();
	              } catch(Exception e) {
	                logger.error("Error while closing connection {}",e);
	              }
	            }
			}
		return metaMap;
	}

	/**
	 * @param metaMap
	 * @param rsmd
	 * @param j
	 * @throws SQLException
	 */
	public static void getActualDataType(Map<String, String> metaMap, ResultSetMetaData rsmd, int j)
			throws SQLException {
		switch(rsmd.getColumnType(j) )  
		  {
		    case java.sql.Types.BIGINT:  		 
		    	 metaMap.put(rsmd.getColumnName(j), "BIGINT");  
		    	 break;
		    case java.sql.Types.FLOAT:   		 
		    	 metaMap.put(rsmd.getColumnName(j), "FLOAT");     
		         break;
		    case java.sql.Types.REAL:  			 
		    	 metaMap.put(rsmd.getColumnName(j), "REAL");     
		         break;
		    case java.sql.Types.DOUBLE: 		
		    	 metaMap.put(rsmd.getColumnName(j), "DOUBLE");     
		         break;
		    case java.sql.Types.NUMERIC:  		 	
		    	 metaMap.put(rsmd.getColumnName(j), "NUMERIC");     
		         break;
		    case java.sql.Types.DECIMAL:  		 
		    	 metaMap.put(rsmd.getColumnName(j), "DECIMAL");     
		         break;
		    case java.sql.Types.CHAR:  			 
		    	 metaMap.put(rsmd.getColumnName(j), "CHAR");     
		         break;
		    case java.sql.Types.VARCHAR:  		 
		    	 metaMap.put(rsmd.getColumnName(j), "VARCHAR");     
		         break;
		    case java.sql.Types.LONGVARCHAR:	  
		    	 metaMap.put(rsmd.getColumnName(j), "LONGVARCHAR");     
		         break;
		    case java.sql.Types.DATE: 			
		    	 metaMap.put(rsmd.getColumnName(j), "DATE");     
		         break;
		    case java.sql.Types.TIME: 			 
		    	 metaMap.put(rsmd.getColumnName(j), "TIME");     
		         break;
		    case java.sql.Types.TIMESTAMP: 		 
		    	 metaMap.put(rsmd.getColumnName(j), "TIMESTAMP");     
		         break;
		     case java.sql.Types.BINARY:  	     
		    	 metaMap.put(rsmd.getColumnName(j), "BINARY");     
		         break;
		     case java.sql.Types.VARBINARY:  	 
		    	 metaMap.put(rsmd.getColumnName(j), "VARBINARY");     
		         break;
		     case java.sql.Types.LONGVARBINARY:  
		    	 metaMap.put(rsmd.getColumnName(j), "LONGVARBINARY");     
		         break;
		     case java.sql.Types.NULL:            
		    	 metaMap.put(rsmd.getColumnName(j), "NULL");     
		         break;
		     case java.sql.Types.OTHER:           
		    	 metaMap.put(rsmd.getColumnName(j), "OTHER");     
		         break;
		     case java.sql.Types.JAVA_OBJECT:     
		    	 metaMap.put(rsmd.getColumnName(j), "JAVA_OBJECT");     
		         break;
		     case java.sql.Types.DISTINCT:         
		    	 metaMap.put(rsmd.getColumnName(j), "DISTINCT");     
		         break;
		     case java.sql.Types.STRUCT:  		  	
		    	 metaMap.put(rsmd.getColumnName(j), "STRUCT");     
		         break;
		     case java.sql.Types.ARRAY:  		  	
		    	 metaMap.put(rsmd.getColumnName(j), "ARRAY");     
		         break;
		     case java.sql.Types.BLOB:  		  	
		    	 metaMap.put(rsmd.getColumnName(j), "BLOB");     
		         break;
		     case java.sql.Types.CLOB:  		  	
		    	 metaMap.put(rsmd.getColumnName(j), "CLOB");     
		         break;
		     case java.sql.Types.REF:  			  
		    	 metaMap.put(rsmd.getColumnName(j), "REF");     
		         break;
		  }
	}
	
	
	/**
	 * @param metaMap
	 * @param rsmd
	 * @param j
	 * @throws SQLException
	 */
	public static boolean getRealDataType(String  unKnownDataType) {
	
	    boolean isString = true;
		switch(unKnownDataType)  
		  {
		    case "BIGINT": 	 
		    case "FLOAT":
		    case "REAL":  			 
		    case "DOUBLE": 		
		    case "NUMERIC":  		 	
		    case "DECIMAL":
		    case "BINARY" :
		    case "BLOB" :
		    case "CLOB" :
		    case "NULL" :	
		    				isString = false;
		    				break;
		    case "CHAR":
		    case "VARCHAR":
		    case "LONGVARCHAR":
		    case "DATE": 
		    case "TIME":
		    case "TIMESTAMP":
		    				isString = true;
		    				break;
		    default :break;
		  }
		return isString;
	}
	
	
	public static HashMap<String,String> getRangeDetails(String query) throws SQLException, IOException
	
	{
		HashMap<String,String> map = new HashMap<String,String>();
		
		Connection connection = null;
		Statement stmt =null;
		ResultSet rs = null;
		
		long startTime = 0;
			try {
				//conn = jdbcInstance.getConnection("jdbc:mysql://localhost:3306/", "mercerdb","root", "");
				
				startTime  = System.currentTimeMillis();
				
				connection = MercerDBCP.getDataSource().getConnection();
				stmt = connection.createStatement();
				
				rs = stmt.executeQuery(query);	
				while(rs.next())
				{
					map.put(Constants.ERROR_MIN, rs.getString(Constants.ERROR_MIN));
					map.put(Constants.ERROR_MAX, rs.getString(Constants.ERROR_MAX));
					map.put(Constants.ALERT_MIN, rs.getString(Constants.ALERT_MIN));
					map.put(Constants.ALERT_MAX, rs.getString(Constants.ALERT_MAX));
				}
				
				logger.debug("Time taken for creating connection and getting result statement for getRangeDetails() "
						+ "in DBUtiils..", (System.currentTimeMillis() - startTime));
			} catch ( SQLException  ex ) {
				System.out.println(query);
				map.put("SQLException", ex.getMessage());
				logger.error("Exception occured in getRangeDetails.."+ex.getMessage());
				throw ex;
			}
			finally
			{
				if(rs!=null)
				{
					rs.close();
				}
				if( null != stmt)
				{
					stmt.close();
				}
				if(stmt!=null)
				{
					stmt.close();
				}
				if (connection != null) {
	              try {
	                connection.close();
	              } catch(Exception e) {
	                logger.error("Error while closing connection {}",e);
	              }
	            }
			}
		return map;
	}

	/**
	 * @param tableName
	 * @return Map with values from two columns from refrence table.
	 * @throws SQLException 
	 * @throws IOException 
	 */
	public static Map<String, String> getRefTableData(String tableName) throws SQLException, IOException {
		logger.info("Entering Inside  getRefTableData method");

		Map<String, String> dataMap = new HashMap<>();

		Connection connection = null;
		Statement stmt =null;
		ResultSet rs = null;
		
		final String query = " SELECT * FROM " + tableName;
		
		long startTime = 0;
		
		logger.info("query is :"+query);
		try {
			startTime  = System.currentTimeMillis();
			
			connection = MercerDBCP.getDataSource().getConnection();
			stmt = connection.createStatement();
			
			
			rs = stmt.executeQuery(query);
			logger.info("rs is :"+rs.getMetaData());
			// Assuming table contains 2 columns only.
			while (rs.next()) {
				dataMap.put(rs.getString("CTRY_CODE"), rs.getString("CTRY_NAME"));
			}
			
			logger.debug("Time taken for creating connection and getting result statement for getRefTableData() "
					+ "in DBUtiils..", (System.currentTimeMillis() - startTime));
			logger.info("dataMap is :"+dataMap);
		} catch (  SQLException ex) {
              logger.error("Exception occured in getRefTableData.."+ex.getMessage());
              throw ex;
		} finally {
			
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (connection != null) {
	              try {
	                connection.close();
	              } catch(Exception e) {
	                logger.error("Error while closing connection {}",e);
	              }
	            }
		}

		logger.info("Existing from  getRefTableData method");
		return dataMap;
	}
	
	public static HashMap<String, ArrayList<HashMap<String, Object>>> loadDataBaseInMemory()
			throws SQLException, IOException {

		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		long startTime = 0;

		HashMap<String, ArrayList<HashMap<String, Object>>> recordsMap = null;
		ArrayList<String> tableNames = null;
		String sql = null;
		ResultSetMetaData rsMd = null;
		int columnCount = 0;
		Object recrodObj = null;
		boolean isRecordExist = false;
		try {

			// Define linked hash map to store tables and its corr. columns.

			tableNames = new ArrayList<String>(50);
			recordsMap = new HashMap<String, ArrayList<HashMap<String, Object>>> ();
			startTime = System.currentTimeMillis();
			logger.info(" DB Operation start time is " + startTime);
			connection = MercerDBCP.getDataSource().getConnection();
			// --- LISTING DATABASE TABLE NAMES ---
			tableNames = getAllTables();

			// --- LISTING DATABASE COLUMN NAMES AND RECORDS FROM EACH TABLE---
			stmt = connection.createStatement();
			for (String table : tableNames) {
				
				ArrayList<HashMap<String,Object>> tempArrayList = null;
				 tempArrayList = new ArrayList<HashMap<String,Object>>();
				 
				sql = " SELECT * FROM  " +  table ;
				resultSet = stmt.executeQuery(sql);
				rsMd = resultSet.getMetaData();
				columnCount = rsMd.getColumnCount();
				tempArrayList.clear();
				while (resultSet.next()) {
					HashMap<String, Object> tempMap = null;
					tempMap = new HashMap<String, Object> ();
					
					isRecordExist = true;
					for (int i = 1; i <= columnCount; i++) {
						recrodObj = resultSet.getObject(i);
						tempMap.put(rsMd.getColumnName(i), recrodObj);
					}
					tempArrayList.add(tempMap);
				}
				if( isRecordExist ) {
					recordsMap.put(table, tempArrayList);
				}	
			}
			logger.info(" DB Operation end time is "+  (System.currentTimeMillis() -  startTime));
			
		} catch (SQLException ex) {
			logger.error("Exception occured in getRefTableData.." + ex.getMessage());
			throw ex;

		} finally {

			if (resultSet != null) {
				resultSet.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (connection != null) {
              try {
                connection.close();
              } catch(Exception e) {
                logger.error("Error while closing connection {}",e);
              }
            }
		}
		//Files.write(Paths.get("C:\\Users\\10640514\\Desktop\\ABC.txt"), recordsMap.toString().getBytes());
		return recordsMap;
	}

	 public static ArrayList<String> getAllTables() throws SQLException{
		 
		 DatabaseMetaData dbMd = null;
		 ResultSet resultSet = null;
		 Connection connection = null;
		 ArrayList<String> tableNames = new ArrayList<String>();
		// --- LISTING DATABASE TABLE NAMES ---
		 try {
			 // Take connection from the DB Connection pool.
			 connection = MercerDBCP.getDataSource().getConnection();
			 // Using sql connection get Meta Data for all tables in Database
			dbMd = connection.getMetaData();
			//Get tables listing in the resultset.
			resultSet = dbMd.getTables(null, null, "%", null);
			
			while (resultSet.next()) {
				resultSet.getString(2);
				tableNames.add(resultSet.getString(3));
			}
			logger.info("Table Name = " + tableNames);
			
		} catch (Exception ex) {
			logger.error("Exception occured in getTables.." + ex.getMessage());
			ex.printStackTrace();
		} finally {
			dbMd = null;
			if (resultSet != null) {
				resultSet.close();
			}
			if (connection != null) {
			  try {
			    connection.close();
			  } catch(Exception e) {
			    logger.error("Error while closing connection {}",e);
			  }
            }
		}
		 return tableNames;
	 }
	 
	/* public static void main(String[] args) {
	 * 
	 * CacheLoadListener.loadCache();
		try {
			getRefTableMetaAndData("validlti");
			getRefTableMetaAndData("jobinversion");
			getRefTableMetaAndData("country");
			getRefTableMetaAndData("currency");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 try {
			System.out.println(getAllTables());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }*/
}
