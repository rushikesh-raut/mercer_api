package in.lnt.utility.general;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import in.lnt.validations.evaluator.APIExpressionEvaluator;

/**
 * 
 * @author Sarang Gandhi
 * @version 0.1
 * @since 28 Aug 2017.
 *
 */
public class DataUtility {

	public static boolean isStringNotNullAndBlank(String str) {
		if (str != null && !str.equals("")) {
			return true;
		}
		return false;
	}

	
	

	/**
	 * 
	 * @return timestampfor storing in DB
	 */
	public static Timestamp currentTimestamp() {
		Date today = new java.util.Date(); // TODO Create Util Function for this
		return new java.sql.Timestamp(today.getTime());
	}

	/**
	 * 
	 * @param string
	 * @return boolean
	 */
	public static boolean isDecimal(String string) {
		if (!StringUtils.isEmpty(string)) {
			return string.matches("(\\+|-)?([0-9]*+(\\.[0-9]+)?)"); // For 2 decimals ("^\\d+\\.\\d{2}$")
			// For loosely typed use regex:^[\d.]+$
		} else {
			return false;
		}
	}

	/**
	 * 2,50,000.20   ,	10,000,00.00 ,  4,50,0000.00 , 1,000.00 , 23,000.00 
	 * 55,00,000.45 ,10000.56 ,  1,100,000.00  , 60,00,000.23 , 60,00,000.23 
     * 1,20,000,00.45 , 10,00,00,000.23 
	 * @param string
	 * @return boolean
	 */
	public static boolean isDecimal(String string, ArrayNode decimalArray) {

		// For 2 decimals ("^\\d+\\.\\d{2}$")
		// For loosely typed use regex:^[\d.]+$
		String aDecimalFormatPattern = "###,###.###";
		String thousandsSeparator = ",";
		String decimalSeperator = ".";

		DecimalFormatSymbols symbols = null;

		DecimalFormat format = null;
		ParsePosition parsePosition = null;
		Object object = null;

		boolean flag = false;
		if (!StringUtils.isEmpty(string)) {

			if (decimalArray.size() > 0) {

				for (JsonNode jsNode : decimalArray) {

					if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get("pattern"))
							&& !StringUtils.isEmpty(jsNode.get("pattern").asText())) {
						aDecimalFormatPattern = jsNode.get("pattern").asText();
					}

					if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get("thousandsSeparator"))
							&& !StringUtils.isEmpty(jsNode.get("thousandsSeparator").asText())) {
						thousandsSeparator = jsNode.get("thousandsSeparator").asText();
					}

					if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get("decimalSeparator"))
							&& !StringUtils.isEmpty(jsNode.get("decimalSeparator").asText())) {
						decimalSeperator = jsNode.get("decimalSeparator").asText();
					}
				}
				try {
					symbols = new DecimalFormatSymbols();
					symbols.setGroupingSeparator(thousandsSeparator.charAt(0));
					symbols.setDecimalSeparator(decimalSeperator.charAt(0));

					format = new DecimalFormat(aDecimalFormatPattern, symbols);
					parsePosition = new ParsePosition(0);
					object = format.parse(string, parsePosition);

					if (object == null || parsePosition.getIndex() < string.length()) {
					} else {
						flag = true;
					}
				} catch (Exception ex) {
					return  false;
				}
			} else {
					flag = string.matches("(\\+|-)?([0-9]*+(\\.[0-9]+)?)");
			}
		}
		return flag;
	}

	/**
	 * 
	 * @param string
	 * @param decimalArray
	 * @return String
	 */
	public static String toDecimal(String string, ArrayNode decimalArray) {

		// For 2 decimals ("^\\d+\\.\\d{2}$")
		// For loosely typed use regex:^[\d.]+$
		String aDecimalFormatPattern = "###,###.###";
		String thousandsSeparator = ",";
		String decimalSeperator = ".";

		DecimalFormatSymbols symbols = null;

		DecimalFormat format = null;
		ParsePosition parsePosition = null;
		Object object = null;

		String resultDecimal = null;
		if (!StringUtils.isEmpty(string)) {

			if (decimalArray.size() > 0) {

				try {
					symbols = new DecimalFormatSymbols();
					symbols.setGroupingSeparator(thousandsSeparator.charAt(0));
					symbols.setDecimalSeparator(decimalSeperator.charAt(0));

					format = new DecimalFormat(aDecimalFormatPattern, symbols);
					parsePosition = new ParsePosition(0);
					object = format.parse(string, parsePosition);

					if (object == null || parsePosition.getIndex() < string.length()) {
					} else {
						resultDecimal = new BigDecimal(object.toString()).toString();
					}
				} catch (Exception ex) {
					return  resultDecimal;
				}
			} 
		}
		return resultDecimal;
	}

	/**
	 * 
	 * @param string
	 * @return boolean
	 */
	public static boolean isNumeric(String string) {
		if (!StringUtils.isEmpty(string)) {
			return string.matches("(\\+|-)?([0-9]+)");
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param string
	 * @return boolean
	 */
	public static boolean isNumeric(String string, ArrayNode numberArray) {

		// For 2 decimals ("^\\d+\\.\\d{2}$")
		// For loosely typed use regex:^[\d.]+$
		String aDecimalFormatPattern = "###,###.###";
		String thousandsSeparator = ",";
		String decimalSeperator = ".";

		DecimalFormatSymbols symbols = null;

		DecimalFormat format = null;
		ParsePosition parsePosition = null;
		Object object = null;

		boolean flag = false;
		if (!StringUtils.isEmpty(string)) {

			if (numberArray.size() > 0) {

				for (JsonNode jsNode : numberArray) {

					if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get("pattern"))
							&& !StringUtils.isEmpty(jsNode.get("pattern").asText())) {
						aDecimalFormatPattern = jsNode.get("pattern").asText();
					}

					if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get("thousandsSeparator"))
							&& !StringUtils.isEmpty(jsNode.get("thousandsSeparator").asText())) {
						thousandsSeparator = jsNode.get("thousandsSeparator").asText();
					}

					if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get("decimalSeparator"))
							&& !StringUtils.isEmpty(jsNode.get("decimalSeparator").asText())) {
						decimalSeperator = jsNode.get("decimalSeparator").asText();
					}
				}
				try {
					symbols = new DecimalFormatSymbols();
					symbols.setGroupingSeparator(thousandsSeparator.charAt(0));
					symbols.setDecimalSeparator(decimalSeperator.charAt(0));

					format = new DecimalFormat(aDecimalFormatPattern, symbols);
					parsePosition = new ParsePosition(0);
					object = format.parse(string, parsePosition);

					if (object == null || parsePosition.getIndex() < string.length()) {
					} else {
						object = new Integer(object.toString());
						if (object.toString().matches("(\\+|-)?([0-9]+)"))
							flag = true;
					}
				} catch (Exception ex) {
					return  false;
				}
			} else {
					flag = string.matches("(\\+|-)?([0-9]+)");
			}
		}
		return flag;
	}

	/**
	 * @param String
	 * @return String
	 */
	public static String toNumber(String string, ArrayNode numberArray) {

		// For 2 decimals ("^\\d+\\.\\d{2}$")
		// For loosely typed use regex:^[\d.]+$
		String aDecimalFormatPattern = "###,###.###";
		String thousandsSeparator = ",";
		String decimalSeperator = ".";

		DecimalFormatSymbols symbols = null;

		DecimalFormat format = null;
		ParsePosition parsePosition = null;
		Object object = null;

		String  numberString = null;
		if (!StringUtils.isEmpty(string)) {

			if (numberArray.size() > 0) {

				try {
					symbols = new DecimalFormatSymbols();
					symbols.setGroupingSeparator(thousandsSeparator.charAt(0));
					symbols.setDecimalSeparator(decimalSeperator.charAt(0));

					format = new DecimalFormat(aDecimalFormatPattern, symbols);
					parsePosition = new ParsePosition(0);
					object = format.parse(string, parsePosition);

					if (object == null || parsePosition.getIndex() < string.length()) {
					} else {
						numberString = new BigDecimal(object.toString()).toString();
					}
				} catch (Exception ex) {
					return  numberString;
				}
			} 
		}
		return numberString;
	}
	
	
	/**
	 * 
	 * @param inDate
	 * @return boolean
	 */
	public static boolean isValidDate(String inDate) {
		//YYYY-MM-DDThh:mm:ss.sTZD  if requires we need to add support for timestamp too.
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}

	/**
	 * PE - 7257
	 * 
	 * @param inDate
	 * @return boolean
	 */
	public static boolean isValidDate(String inputDate, ArrayNode inputDateArray) {
		boolean returnVal = false;
		if( StringUtils.isEmpty(inputDate)) {
			return returnVal;
		}
		/*
		 * Loop through array of formats and validate using checkDate function. 
		 */
		
		for (int i = 0; i < inputDateArray.size(); i++) {
			returnVal = checkDate(inputDate, inputDateArray.get(i).asText());
			if(returnVal) {
				break;
			}
		}
		return returnVal;
	}

	
  /**
   * 
   * @param inDate
   * @param format
   * @return boolean
   * This method will check the inDate is matching with the format.
   */
	public static boolean checkDate(String inDate, String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format.replaceAll("\"", "").trim());
		dateFormat.setLenient(false);
		//2 digit year is not allowed , issue has been raised in internal test scenarios.
		// Calendar is added to fetch year and check it is 4 digit.
		Calendar cal = Calendar.getInstance();
		int year ;
		try {
			 Date date = dateFormat.parse(inDate.trim());
			 cal.setTime(date);
			 year = cal.get(Calendar.YEAR);
		} catch (ParseException pe) {
			return false;
		}
		if (year < 999) {
			return false;
		}else {
			return true;
		}	
	}
	
	/**
	 * 
	 * @param dateToBeParse
	 * @param inputDateArray
	 * @return String with yyyy-MM-dd format
	 */
	public static String customDateConvertor(String dateToBeParse, ArrayNode inputDateArray) {
		DateFormat defaultDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat dateFormatNeeded = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		String convertedDate = null;
		try {
			for (JsonNode dtFormatEle : inputDateArray) {
				if (!JsonUtils.isNullOrBlankOrNullNode(dtFormatEle) &&
						checkDate(dateToBeParse, dtFormatEle.asText())) {
					defaultDateFormat = new SimpleDateFormat(dtFormatEle.asText());
					date = defaultDateFormat.parse(dateToBeParse);
					convertedDate = dateFormatNeeded.format(date);
					break;
				} 
			}

		} catch (ParseException e) {

		}
		return convertedDate;
	}
	

	/**
	 * PE - 7056
	 * 
	 * @author Sarang
	 * @since 9 Dec 2017
	 * 
	 * @param apiExpressionEvaluator
	 * @return metaDataColumnsOnly (List of Strings)
	 */

	public static List<String> extractColumnsFromMetaData(APIExpressionEvaluator apiExpressionEvaluator) {
		List<String> metaDataColumnsOnly = null;
		metaDataColumnsOnly = new ArrayList<>();
		if (null != apiExpressionEvaluator && null != apiExpressionEvaluator.getColumnDataTypeMapping()) {
			metaDataColumnsOnly = apiExpressionEvaluator.getColumnDataTypeMapping().keySet().stream()
					.collect(Collectors.toList());
		}
		return metaDataColumnsOnly;
	}

}
