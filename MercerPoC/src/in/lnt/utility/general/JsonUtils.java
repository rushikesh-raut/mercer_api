package in.lnt.utility.general;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.ErrorCacheConstant;

public class JsonUtils {

	/**
	 * @param jsonLikeObject
	 * @return message.
	 */
	public static String isJSONValid(String jsonLikeObject) {

		String returnValue = "true";
		try {
			new JSONObject(jsonLikeObject);
		} catch (JSONException ex) {
			try {
				new JSONArray(jsonLikeObject);
			} catch (JSONException ex1) {
				returnValue = ex.getMessage();
			}
		}
		return returnValue;
	}
	
	public static boolean isNullOrBlankOrNullNode(JsonNode node) {

		if(node==null || node.isNull()){
			return true;
		}
		return false;
	}
    
	public static boolean isNullNode(JsonNode node) {

		if(node!=null && NullNode.class.equals(node.getClass())){
			return true;
		}
		return false;
	}
	
    /**
     * Ref : https://www.javatips.net/api/pluggable-master/framework/src/main/java/org/json/CDL.java
     * Produce a comma delimited text from a JSONArray of JSONObjects using a
     * provided list of names. The list of names is not included in the output.
     * 
     * @param names
     *            A JSONArray of strings.
     * @param ja
     *            A JSONArray of JSONObjects.
     * @return A comma delimited text.
     * @throws JSONException
     */
	public static  String toString(JSONArray names, JSONArray ja) throws JSONException {
        if (names == null || names.length() == 0) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ja.length(); i += 1) {
            JSONObject jo = ja.optJSONObject(i);
            if (jo != null) {
                sb.append(rowToString(jo.toJSONArray(names)));
            }
        }
        return sb.toString();
    }
    
    /**
     * Ref : https://www.javatips.net/api/pluggable-master/framework/src/main/java/org/json/CDL.java
     * Produce a comma delimited text row from a JSONArray. Values containing
     * the comma character will be quoted. Troublesome characters may be
     * removed.
     * 
     * @param ja
     *            A JSONArray of strings.
     * @return A string ending in NEWLINE.
     */
	public static  String rowToString(JSONArray ja) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ja.length(); i += 1) {
            if (i > 0) {
                sb.append(',');
            }
            Object object = ja.opt(i);
            if (object != null && !ja.isNull(i)) {
                String string = object.toString();
                if (string.length() > 0 && (string.indexOf(',') >= 0 || string.indexOf('\n') >= 0 || string.indexOf('\r') >= 0 || string.indexOf(0) >= 0 || string.charAt(0) == '"')) {
                    sb.append('"');
                    int length = string.length();
                    for (int j = 0; j < length; j += 1) {
                        char c = string.charAt(j);
                        if (c >= ' ' && c != '"') {
                            sb.append(c);
                        }
                    }
                    sb.append('"');
                } else {
                    sb.append(string);
                }
            }
        }
        sb.append('\n');
        return sb.toString();
    }
	
	public static JsonNode getErrorParams(String params,List<String> paramValueList)  {
		// TODO Auto-generated method stub
		JsonNode paramNode = null;
		ObjectMapper mapper = new ObjectMapper();
		 List<String> paramsList = Arrays.asList(params.split(":"));
		 paramNode = mapper.createObjectNode();
		for(int i=0;i<paramsList.size();i++)
		{
			((ObjectNode)paramNode).put(paramsList.get(i), paramValueList.get(i));
		}
		return paramNode;
	}

	public static ObjectNode updateErrorNode(CustomStatusException cse, ObjectNode errNode)  {
		// TODO Auto-generated method stub
		errNode.put(Constants.MESSAGE_KEY, cse.getMessage());
		if(null != cse.getParams()){
			errNode.set(Constants.MESSAGE_PARAMS,getErrorParams(cse.getParams(),cse.getParamValues()));
		}
		return errNode;
	}
	
	public static ObjectNode updateErrorNodeForGenericException(ObjectNode errNode,Exception e) {
		ArrayList<String> paramValueList=null;
		CustomStatusException customStatusException = new CustomStatusException();
		paramValueList =  new ArrayList<String>();
		paramValueList.add(""+e.getMessage());
		//customStatusException = new CustomStatusException();
		customStatusException.setMessage(Cache.getPropertyFromError(ErrorCacheConstant.ERR_040));
		customStatusException.setParamValues(paramValueList);
		customStatusException.setParams(Cache.getPropertyFromError(ErrorCacheConstant.ERR_040+Constants.PARAMS));
		return JsonUtils.updateErrorNode(customStatusException,errNode);
		
	}
	public static boolean isStringExist(String value)
	{
		if(value!=null && !value.equals("") && !value.equals("null"))
			return true;
		else
			return false;
	}
	public static boolean isBlankTextNode(JsonNode node) {

        if( (node!=null && TextNode.class.equals(node.getClass())) && node.asText().equals("")){
               return true;
        }
        return false;
 }

	
	
}
