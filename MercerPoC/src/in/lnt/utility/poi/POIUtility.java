package in.lnt.utility.poi;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;

/**
 * 
 * @author Sarang Gandhi
 * 
 *         This is POI utility class.
 */
public class POIUtility {

	private static final Logger _logger = LoggerFactory.getLogger(POIUtility.class);

	public static InputStream convertXlsxToCSV(FileItem item) {

		_logger.info("Entering into POIUtility.convertXlsxToCSV method ");

		FileItem input_document = null;
		InputStream inpustStream = null;
		// For storing data into CSV files
		StringBuffer cellValue;
		XSSFWorkbook wb;
		XSSFSheet sheet;
		Iterator<Row> rowIterator;
		// Declare rows and cell from poi ss model
		Row row;
		Cell cell;
		Iterator<Cell> cellIterator;
		DataFormatter dataFormatter;
		try {
			if (null != item) {
				input_document = item;
			} else {// Throw exception over here
				return null;
			}
			_logger.info("Inisde try block");
			cellValue = new StringBuffer();

			_logger.info("xlsx reading process started" + cellValue);
			// Get the workbook instance for XLSX file
			wb = new XSSFWorkbook(input_document.getInputStream());

			// Get first sheet from the workbook
			sheet = wb.getSheetAt(0);
			_logger.info(" Read the first sheet of the xlsx book" + wb.getSheetAt(0));

			// Iterate through each rows from first sheet
			rowIterator = sheet.iterator();

			// This will append line name of no following new line.
			/*
			 * cellValue.append("The name or no of sheet is"
			 * +wb.getSheetName(0)); cellValue.append(System.lineSeparator());
			 */

			dataFormatter = new DataFormatter();

			while (rowIterator.hasNext()) {
				_logger.info("Inside outer while for the row iterator");
				row = rowIterator.next();

				// For each row, iterate through each columns
				cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {

					_logger.info("Inside inner while for the column iterator");
					cell = cellIterator.next();

					// Switch to the data types.
					switch (cell.getCellType()) {

					case Cell.CELL_TYPE_BOOLEAN:
						cellValue.append(cell.getBooleanCellValue() + ","); // Boolean
						break;

					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) { // Handles
																	// Date type
																	// here
							SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
							cellValue.append(dateFormat.format(cell.getDateCellValue()));
						} else {
							// cellValue.append(cell.getNumericCellValue()+",");
							// //Numeric
							cellValue.append(dataFormatter.formatCellValue(cell) + ",");
						}
						break;

					case Cell.CELL_TYPE_STRING:
						cellValue.append(cell.getStringCellValue() + ","); // String
						break;

					case Cell.CELL_TYPE_FORMULA:
						cellValue.append(cell.getCellFormula().toString() + ","); // Formula
						break;

					case Cell.CELL_TYPE_BLANK:
						cellValue.append("" + ","); // Blank
						break;

					/*
					 * default : cellValue.append(cell);
					 */
					}
					_logger.info("Exiting from switch case");
				}
				if (',' == cellValue.charAt(cellValue.length() - 1)) {
					cellValue.deleteCharAt(cellValue.length() - 1);
				}
				cellValue.append(System.lineSeparator());
			}
			_logger.info("Creating inputstream for the csv data ");
			inpustStream = new ByteArrayInputStream(cellValue.toString().getBytes());
		} catch (IOException ex) {
			_logger.info("IOException occures" + ex.getCause());
		} finally {
		}
		return inpustStream;
	}

	/**
	 * @author Jwala Pradhan
	 * @param File
	 *            item
	 * @return hashmap
	 */

	public static HashMap<String, Object> prseXslxFile(FileItem item) throws Exception{

		_logger.info(Constants.INTIATE + Constants.LOG_PARSEXSLXFILE);
		InputStream inpustStream = null;
		XSSFWorkbook workbook = null;
		HashMap<String, Object> map1 = null;
		HashMap<String, Object> workBookMap = new HashMap<String, Object>();
		StringBuffer cellValue = null;
		DataFormatter dataFormatter = new DataFormatter();
		
		// Ref : http://www.codejava.net/coding/working-with-formula-cells-in-excel-using-apache-poi
		FormulaEvaluator formulaEvaluator = null;
		try {
			inpustStream = item.getInputStream();
			workbook = new XSSFWorkbook(inpustStream);
			formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
			if (workbook != null) {
				for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
					Sheet sheet = workbook.getSheetAt(i);
					if (i == 0)
						workBookMap.put("FirstSheetName", workbook.getSheetName(i));
					List<String> header = new ArrayList<String>();
					List<ArrayList<String>> rows = new ArrayList<ArrayList<String>>();
					map1 = new HashMap<String, Object>();
					Iterator<Row> iterator = sheet.iterator();
					int j = 0;
					int ignoreRowCount = 0;
					while (iterator.hasNext()) {
						Row nextRow = iterator.next();

						Iterator<Cell> cellIterator = nextRow.cellIterator();
						if (j == 0) {											// HEADER : ROW(0)
							while (cellIterator.hasNext()) {
								Cell cell = cellIterator.next();
								header.add(cell.getStringCellValue());
							}

						} else if (j == 1) {									// IGNORE : ROW(1)
							j++;
							continue;
							
						} else {												// For ROW(>1)
							ArrayList<String> tempRow = new ArrayList<String>();
							
							// PE-6713
							// "ignoreRowCount" variable is used to count non-empty cells in a row.
							
							//PE-8575: Reject the file in case of Header is missing for one or multiple columns
							if(nextRow.getLastCellNum()>header.size())
							{
								throw new CustomStatusException(Constants.HTTPSTATUS_400,
										Cache.getPropertyFromError(ErrorCacheConstant.ERR_064));
							}
							ignoreRowCount = 0;
							
							for (int k = 0; k < header.size(); k++) {
								
								
								Cell cell = nextRow.getCell(k);
								
								// First : check if cell is null 
								if (cell == null) {
									tempRow.add(null);//PE -6874
									continue;
								}
								
								// Second : check the datatype and fetch data
								switch (cell.getCellType()) {
								case Cell.CELL_TYPE_BLANK:
									tempRow.add("  ");
									break;
								case Cell.CELL_TYPE_STRING:
									tempRow.add(cell.getStringCellValue());
									ignoreRowCount++;
									break;
								case Cell.CELL_TYPE_BOOLEAN:
									tempRow.add("" + cell.getBooleanCellValue());
									ignoreRowCount++;
									break;
								case Cell.CELL_TYPE_NUMERIC:
									if (DateUtil.isCellDateFormatted(cell)) { 		// Handles Date type here
										if(null != cellValue){
											cellValue.delete(0, cellValue.length());
											
											// Get date in Excel format
											cellValue.append(dataFormatter.formatCellValue(cell));
										}
								
									} else {
										cellValue = new StringBuffer();
										// //Numeric
										cellValue.append(dataFormatter.formatCellValue(cell));
									}
									tempRow.add(cellValue + "");
									ignoreRowCount++;
									break;
								
								case Cell.CELL_TYPE_FORMULA:
							        switch(cell.getCachedFormulaResultType()) {

									case Cell.CELL_TYPE_NUMERIC:
										if(null != cellValue){
											cellValue.delete(0, cellValue.length());
										}

										// Date
										if (DateUtil.isCellDateFormatted(cell)) {
											if(null != cellValue){
												cellValue.append(dataFormatter.formatCellValue(cell, formulaEvaluator));
											}
											// Numeric
										} else {
											cellValue = new StringBuffer();
											cellValue.append(dataFormatter.formatCellValue(cell, formulaEvaluator));
										}
										tempRow.add(cellValue + "");
										break;

									case Cell.CELL_TYPE_STRING:
										tempRow.add(cell.getStringCellValue());
										break;
									}
									break;
									case Cell.CELL_TYPE_ERROR:
										//byte errorValue = cell.getErrorCellValue();
										tempRow.add(null);
										break;
							}
							}
							if(ignoreRowCount != 0) {
								rows.add(tempRow);
							}
						}
						j++;
					}
					map1.put("header", header);
					map1.put("data", rows);
					workBookMap.put(sheet.getSheetName(), map1);
				}
			}
		}

		 finally {
			workbook = null;
			try {
				inpustStream.close();
			} catch (IOException e) {
				_logger.info("Exception :: "+ e.getMessage());
			}
		}
		_logger.info(Constants.EXIT + Constants.LOG_PARSEXSLXFILE);
		return workBookMap;
	}
}
