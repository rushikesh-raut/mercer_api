package in.lnt.validations;

import java.util.Iterator;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.databind.JsonNode;
import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.JsonUtils;

/**
 * @author rushikesh
 *
 */
public class PerFormL3BValidations {

  private static final Logger logger = LoggerFactory.getLogger(PerFormL3BValidations.class);

  public static JsonNode performL3B(String validationData, Boolean isMDARequest) throws Exception {
    logger.info(" Entering into crossSectionChecks method for json body ");

    String entitiesJson = "";
    String key = "";
    String entitiesKey = null;
    JSONObject wholeDataObject = null;
    Iterator<String> it = null;
    String validatedSyntax = "false";

    try {
      validatedSyntax = JsonUtils.isJSONValid(validationData); // Json Syntax checker..
      if (validatedSyntax.equalsIgnoreCase("true")) {
        wholeDataObject = new JSONObject(validationData);
        it = wholeDataObject.keys();
      } else {
        throw new CustomStatusException(Constants.HTTPSTATUS_400,
            Cache.getPropertyFromError(ErrorCacheConstant.ERR_004));
      }

      while (it != null && it.hasNext()) {
        key = it.next();
        if (key.equalsIgnoreCase("entities")) {
          entitiesKey = key;
        }
      }

      if (entitiesKey != null && wholeDataObject.get(entitiesKey) != null) {
        // logger.info(String.format("StartTime For process : %s",
        // wholeDataObject.get(entitiesKey) ));
        entitiesJson = wholeDataObject.get(entitiesKey).toString();
      } else { // errorResponse.put("RESPONSE", "ERR 3: Invalid input JSON
               // ");
        throw new CustomStatusException(Constants.HTTPSTATUS_400,
            Cache.getPropertyFromError(ErrorCacheConstant.ERR_004));
      }

      // boolean isMDARequest = false;
      // isMDARequest =
      // Boolean.valueOf(String.valueOf(request.getAttribute(Constants.ISMDAREQUEST)));
      // request.setAttribute(Constants.ISMDAREQUEST, isMDARequest);
      JsonNode data = validateForm(entitiesJson, isMDARequest);
      return data;
    } catch (Exception e) {
      throw new Exception(e.getMessage());
    }
  }

  /*
   * TODO Move to FormValidator.java
   */
  public static @ResponseBody JsonNode validateForm(String entitiesJson, Boolean isMDARequest)
      throws CustomStatusException, Exception {
    // logger.info(String.format("%s %s", Constants.INTIATE ,Constants.LOG_METHOD_VALIDATEFORM));
    JsonNode data = null;
    FormValidator_CSC formValidator = new FormValidator_CSC();
    // logger.info(request.getAttribute(Constants.ISMDAREQUEST).toString());
    return formValidator.parseAndValidate_CrossSectionCheck(entitiesJson, isMDARequest);

    // logger.info(String.format("%s %s", Constants.EXIT ,Constants.LOG_METHOD_VALIDATEFORM));
  }

}
