package in.lnt.validations.evaluator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lti.mosaic.function.def.CustomFunctions;
import com.lti.mosaic.parser.driver.ExpressionEvalutionDriver;
import com.lti.mosaic.parser.exception.ExpressionEvaluatorException;
import com.lti.mosaic.parser.exception.FieldNotValidException;

import in.lnt.constants.Constants;
import in.lnt.enums.DataTypes;
import in.lnt.enums.ValidationTypes;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.service.db.DBUtils;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.DataUtility;
import in.lnt.utility.general.JsonUtils;
import net.sf.ehcache.CacheManager;

public class APIExpressionEvaluator {
	private static final Logger logger = LoggerFactory.getLogger(APIExpressionEvaluator.class);
	private ExpressionEvalutionDriver expressionEvalutionDriver = new ExpressionEvalutionDriver();
	private static Pattern pattern = null;
	ObjectMapper mapper = new ObjectMapper();
	private Map<String, String> columnDataTypeMapping = null;
	public static final DecimalFormat df = new DecimalFormat("#");

	public ExpressionEvalutionDriver getExpressionEvaluatorDriver() {
		return expressionEvalutionDriver;
	}

	public void setExpressionEvaluatorDriver(ExpressionEvalutionDriver expressionEvalutionDriver) {
		this.expressionEvalutionDriver = expressionEvalutionDriver;
	}

	public Map<String, String> getColumnDataTypeMapping() {
		return columnDataTypeMapping;
	}

	public void setColumnDataTypeMapping(Map<String, String> columnDataTypeMapping) {
		this.columnDataTypeMapping = columnDataTypeMapping;
	}


	@SuppressWarnings("deprecation")
	public ObjectNode expressionEvaluator(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, JsonNode dataNode, boolean isAggregateRequest,	HashMap<String, String> columnMappingMap,
			ArrayList<JsonNode> columnNodeList , List<String> metaDataColumns) throws Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_EXPRESSIONEVALUATOR));
		String expr = "";
		String result = "";
		ObjectNode obj = null;
		boolean checkBox = false;
		boolean isHiddenQue=false;
		String errorType = "";
		// Start PE- 7056
		ArrayList<String>  metaDataColumnsOnly= (ArrayList<String>) metaDataColumns;
		// End PE- 7056
		if (!JsonUtils.isNullOrBlankOrNullNode(columnNode) && !JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.QUESTION_TYPE))
				&& columnNode.get(Constants.QUESTION_TYPE).asText().equalsIgnoreCase("checkboxes")) {
			checkBox = true;
			pattern = Pattern.compile(Constants.EXP_PATTERN_CHECKBOX);
		} else {
			pattern = Pattern.compile(Constants.EXP_PATTERN);
		}
		if (!JsonUtils.isNullOrBlankOrNullNode(columnNode) && !JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.HIDDEN)) 
				&& columnNode.get(Constants.HIDDEN).asText().equalsIgnoreCase("true")) {
			isHiddenQue = true;
		} 
		
		StringBuffer warnMessage = new StringBuffer("Row id is : ");
		String warnVar = dataMap.get("contextData.uniqueIdColumnCode").asText();
		
		Map<String, Object> paramValues = new HashMap<>();
		Object exprObj = null;
		if (validationOBJ != null && validationOBJ.get(Constants.EXPRESSION) != null) {

			paramValues = getParamValues(dataMap, validationOBJ, checkBox, columnMappingMap, columnNodeList, metaDataColumnsOnly,isHiddenQue);
			if (paramValues.containsKey(Constants.EXPRESSION_STRING)) {	// NK
				exprObj = paramValues.get(Constants.EXPRESSION_STRING);
			}
			if (exprObj instanceof String && !StringUtils.isEmpty(exprObj.toString()) && !paramValues.isEmpty()) {	// NK
				expr = exprObj.toString();

			try {

				if (!JsonUtils.isNullOrBlankOrNullNode(columnNode) && !JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.CODE)) 
						&& columnNode.get(Constants.CODE).asText().equalsIgnoreCase(Constants.EMPLOYEE_EEID) 
						&& JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.MAPPED_COLUMN_NAME))) {

					if (dataMap != null && (dataMap.get("YOUR_EEID") == null || dataMap.get("YOUR_EEID").asText().equals(""))) {

						return null;
					}
				}
				else if (!JsonUtils.isNullOrBlankOrNullNode(columnNode) && !JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.CODE))
						&& columnNode.get(Constants.CODE).asText().equalsIgnoreCase(Constants.EMPLOYEE_EEID)
						&& columnNode.get(Constants.MAPPED_COLUMN_NAME) != null) {

					if (dataMap != null
							&& (dataMap.get(columnNode.get(Constants.MAPPED_COLUMN_NAME).asText()) == null
							|| dataMap.get(columnNode.get(Constants.MAPPED_COLUMN_NAME).asText()).equals(""))) {

						return null; 
					}
				}
				result = expressionEvalutionDriver.expressionEvaluatorWithObject(paramValues, expr, false);
				//PE-8712 If null is the result for expression then do not show valdation object
				// And log the error.
				if( null == result ) {
					 logger.warn( warnMessage.append(null != dataMap.get(warnVar) ? dataMap.get(warnVar).asText():"")
							 .append(", Validation object id is :").append(null != validationOBJ.get("_id")?
									 validationOBJ.get("_id").asText():"")
							 .append(", Erroneous expression is: ").append(expr)
							 .append(", Question code is: ").append(null !=columnNode.get(Constants.CODE)?
							  columnNode.get(Constants.CODE).asText():"").toString());
					 warnMessage.delete(0, warnMessage.length()-1);
					 return null;
				}
			} catch (FieldNotValidException e) {
				logger.error("Exception occured in expressionEvaluator .." + e.getMessage());
				result = e.getMessage();
				result = getMessageForAnErrorType(validationOBJ, ValidationTypes.expressionError, expr, result);
				obj = prepareOutputJson(validationOBJ, columnNode, expr + result, ValidationTypes.expressionError,
						columnNode.get(Constants.CODE).asText(), expr, null, null);
				// throw new JSONException(e.getMessage()+"error in
				// exp..."+expr);
				return obj;
			} catch (ExpressionEvaluatorException e) {
				logger.warn( warnMessage.append(null != dataMap.get(warnVar) ? dataMap.get(warnVar).asText():"")
						 .append(", Validation object id is :").append(null != validationOBJ.get("_id")?
								 validationOBJ.get("_id").asText():"")
						 .append(", Erroneous expression is: ").append(expr)
						 .append(", Question code is: ").append(null !=columnNode.get(Constants.CODE)?
						  columnNode.get(Constants.CODE).asText():"")
						 .append(", Reason : ").append(e.getMessage()).toString());
				 warnMessage.delete(0, warnMessage.length()-1);
				 
				return null;
			} catch (Exception e) {
				logger.error("Exception occured in expressionEvaluator .." + e.getMessage());
				result = e.getMessage();
				obj = prepareOutputJson(validationOBJ, columnNode, expr + " " + "ATTENTION: " + result,
						ValidationTypes.expressionError, columnNode.get(Constants.CODE).asText(), expr, null, null);
				// throw new Exception(e.getMessage()+"error in exp..."+expr);
				return obj;
			}
			// logger.debug(String.format("result..%s", result));

			if (isAggregateRequest) {
				obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.expression, null, null, null,
						null);
				if (StringUtils.isBlank(result) || result.length() < 1) {
					result = "[]";
					// logger.info("Result found blank. So, replacing it with
					// [].");
				}
				obj.put("data", mapper.readTree(com.lti.mosaic.parser.utils.ExpUtils.cleanSingleQuoteStartAndEnd(result)));
				return obj;
			}

			if (validationOBJ.get(Constants.ERROR_TYPE) != null)
				errorType = (String) validationOBJ.get(Constants.ERROR_TYPE).asText();

			if ((errorType.compareToIgnoreCase(Constants.ERROR) == 0)
					|| (errorType.compareToIgnoreCase(Constants.ALERT) == 0)
					|| (errorType.compareToIgnoreCase(Constants.ENHANCEMENT) == 0)) {
				if (result != null && result.equals("true")) {
					if (columnNode.has(Constants.DIMENSIONS) && !columnNode.get(Constants.DIMENSIONS).isNull()
							&& (dataNode.hasNonNull(columnNode.get(Constants.CODE).asText())
									&& dataNode.get(columnNode.get(Constants.CODE).asText())
											.getNodeType() == JsonNodeType.OBJECT)) {
						obj = setValidationsForDimensions(columnNode, dataNode, validationOBJ, result, Constants.ALERT);
					} else
						obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.expression, null, null,
								null, null); // PE-3711
				} else if (result != null && result.equals("false")) {
				}
			} else if (errorType.compareToIgnoreCase("AUTOCORRECT") == 0) {

				// AdHoc solution for 7241
				if (columnNode.has(Constants.DIMENSIONS) && !columnNode.get(Constants.DIMENSIONS).isNull()
						&& (dataNode.hasNonNull(columnNode.get(Constants.CODE).asText()) && dataNode
								.get(columnNode.get(Constants.CODE).asText()).getNodeType() == JsonNodeType.OBJECT)) {
					obj = setValidationsForDimensions(columnNode, dataNode, validationOBJ, result,
							Constants.AUTOCORRECT);
					} else if (columnNode.has(Constants.CODE) &&
							//&& null != dataMap.get(columnNode.get(Constants.CODE).asText())
							//&& !dataMap.get(columnNode.get(Constants.CODE).asText()).asText().equals(result)
							(JsonUtils.isNullOrBlankOrNullNode(dataMap.get(columnNode.get(Constants.CODE).asText()))
									|| (!JsonUtils.isNullOrBlankOrNullNode(
											dataMap.get(columnNode.get(Constants.CODE).asText()))
											&& !dataMap.get(columnNode.get(Constants.CODE).asText()).asText()
													.equals(result)))) {
					((ObjectNode) validationOBJ).set("OriginalValue",
							dataMap.get(columnNode.get(Constants.CODE).asText()));
					((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), result);
					// Start PE-7071 Following code added for the new
					// requirement
					if (!JsonUtils.isNullOrBlankOrNullNode(dataNode.get(columnNode.get(Constants.CODE).asText()))) {
						dataMap.put(columnNode.get(Constants.CODE).asText(),
								dataNode.get(columnNode.get(Constants.CODE).asText()));
					}
					// End PE-7071
					obj = prepareOutputJson(validationOBJ, columnNode, result, ValidationTypes.expression, null, null,
							null, null);
				}
			}
		}
	}		
		if (null == obj && (null == result || !result.equals("false"))
				&& !errorType.toUpperCase().equals("AUTOCORRECT") && !paramValues.isEmpty()) {
			result = getMessageForAnErrorType(validationOBJ, ValidationTypes.expressionError, expr, result);
			obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.expression, null, null, null,
					null);
		}
		return obj;
	}

	public ObjectNode checkMandatoryFields(HashMap<String, JsonNode> dataMap, String key, String dataType,
			String mappedColumn) throws Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_CHECKMANDATORYFIELDS));
		if ((dataMap.get(key) == null || dataMap.get(key).asText().isEmpty())
				&& (dataMap.get(mappedColumn) == null || dataMap.get(mappedColumn).asText().isEmpty())) {
			return prepareOutputJson(null, null, null, ValidationTypes.required, key, null, null, null);
		}
		// logger.info(String.format("%s %s", Constants.EXIT
		// ,Constants.LOG_CHECKMANDATORYFIELDS));
		return checkDataType(dataMap, key, dataType, mappedColumn);
	}

	/*
	 * For Cross Section
	 */
	public ObjectNode expressionEvaluator(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, JsonNode dataNode, Map<String, Integer> cscDuplicateChkMap,
			ArrayList<JsonNode> columnNodeList, List<String> metaDataColumns, int endPoint) throws CustomStatusException, Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_EXPRESSIONEVALUATOR));
		ArrayList<String> paramValueList = null;
		String expr = "";
		String result = "";
		ObjectNode obj = null;
		boolean checkBox = false;
		String fieldArr[] = null;
		String prefix = "this.";
		String patternString = "";
		ArrayList<String> dimList = null;
		JsonNode dimNode = null;
		Pattern pattern = Pattern.compile(Constants.EXP_PATTERN);
		boolean isHiddenQue=false;
		// Start PE- 7056
		ArrayList<String>  metaDataColumnsOnly= (ArrayList<String>) metaDataColumns;
		boolean isExpressionFlag = true;
		boolean thisAndContextFlag = false;
		// End PE- 7056
		
		if (columnNode != null && columnNode.get(Constants.QUESTION_TYPE) != null
				&& columnNode.get(Constants.QUESTION_TYPE).asText().equalsIgnoreCase("checkboxes")) {
			checkBox = true;
			pattern = Pattern.compile(Constants.EXP_PATTERN_CHECKBOX);
		} else {
			pattern = Pattern.compile(Constants.EXP_PATTERN);
		}
		if (!JsonUtils.isNullOrBlankOrNullNode(columnNode) &&
				!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.HIDDEN)) 
				&& columnNode.get(Constants.HIDDEN).asText().equalsIgnoreCase("true")) {
			isHiddenQue = true;
		} 
		Map<String, Object> paramValues = new HashMap<>();
		if (validationOBJ != null && validationOBJ.get(Constants.EXPRESSION) != null
				&& !validationOBJ.get(Constants.EXPRESSION).asText().equals(Constants.BLANK)) {// PE7044
																								// Blank
																								// expression
																								// clause
																								// is
																								// added
			expr = validationOBJ.get(Constants.EXPRESSION).asText().replaceAll(Constants.CURLY_BRACES, "");
			patternString = expr;
			if (expr != null && expr.contains("bool_in_lookup")) {
				patternString = expr.replaceAll(Constants.REPLACE_FROM, Constants.REPLACE_TO);
			}
			Matcher matcher = pattern.matcher(patternString);
			List<String> fields = new ArrayList<String>();
			while (matcher.find()) {
				fields.add(matcher.group().replace("this.", "").trim());
			}
			
			if( !fields.isEmpty() ) {  	
				thisAndContextFlag = true;
			}
			if ( thisAndContextFlag ) {
					if ( (!metaDataColumnsOnly.isEmpty() &&  chekElementExist(metaDataColumnsOnly,fields)) || isHiddenQue) { 
						isExpressionFlag = true;
					}else{
						isExpressionFlag = false;
					}
			 }
			
			if ( isExpressionFlag ) {  //PE- 7056
				// logger.debug(String.format("fields..%s", fields));
				for (String field : fields) {
					if (field != null && field.trim().contains("contextData")) {
						prefix = prefix.replace("this.", "");
					}
					if (checkBox) {
						fieldArr = extractFields(field.trim());
						if (fieldArr != null) {
							paramValues.put(prefix + field.trim(), (dataMap.get(fieldArr[0]) == null)
									? (dataMap.get("otherSectionsData." + fieldArr[0].trim()) == null ? "null"
											: findInArray(dataMap.get("otherSectionsData." + fieldArr[0]), fieldArr[1]))
									: findInArray(dataMap.get(fieldArr[0]), fieldArr[1]));
						} else {
							paramValues.put(prefix + field.trim(), (dataMap.get(field.trim()) == null)
									? (dataMap.get("otherSectionsData." + field.trim()) == null ? "null"
											: getContextDataValues(dataMap.get("otherSectionsData." + field.trim()).asText()
													.replace(",", "")))
									: getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(field.trim(), "dataType"),
											dataMap.get(field.trim()).asText().replace(",", "")));
						}
					} else {
						String tmp = null;
						 if (!dataMap.containsKey(field.trim()) && !isHiddenQue) {												// PE-7506 						
							if (dataMap.get("otherSectionsData." + field.trim()) == null) {
								if (cscDuplicateChkMap != null && cscDuplicateChkMap.containsKey(field.trim())) {
	
									paramValueList = new ArrayList<String>();
									paramValueList.add("" + cscDuplicateChkMap.get(field.trim()));
									paramValueList.add("" + field.trim());
									paramValueList.add("" + expr);
									throw new CustomStatusException(Constants.HTTPSTATUS_400,
											Cache.getPropertyFromError(ErrorCacheConstant.ERR_048),
											Cache.getPropertyFromError(ErrorCacheConstant.ERR_048 + Constants.PARAMS),
											paramValueList);
								} else {
									paramValueList = new ArrayList<String>();
									paramValueList.add(field.trim());
									paramValueList.add(expr);
									throw new CustomStatusException(Constants.HTTPSTATUS_400,
											Cache.getPropertyFromError(ErrorCacheConstant.ERR_007),
											Cache.getPropertyFromError(ErrorCacheConstant.ERR_007 + Constants.PARAMS),
											paramValueList);
								}
							} else {
								tmp = getContextDataValues(
										dataMap.get("otherSectionsData." + field.trim()).asText().replace(",", ""));
							}
						} else {
							String val = !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(field.trim()))?dataMap.get(field.trim()).asText():"";
							tmp = getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(field.trim(), "dataType"), val.replace(",", ""));
							val=null;
						}
						paramValues.put(prefix + field.trim(), tmp);
					}
					prefix = "this.";
				}
				try {
					result = expressionEvalutionDriver.expressionEvaluatorWithObject(paramValues, expr, false);
	
				} catch (ExpressionEvaluatorException e) {
					logger.warn("Erroneous expression is: "+ expr +", Reason " +e.getMessage() );
					return null;
				}catch (Exception e) {
					logger.error("Excepion occured in expressionEvaluator.." + e.getMessage());
					paramValueList = new ArrayList<String>();
					paramValueList.add(e.getMessage());
					if ( e.getCause().getMessage().indexOf("org.antlr.v4.runtime") == -1 ) {
						obj = null;
					}else {
						throw new CustomStatusException(Constants.HTTPSTATUS_400,
							in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_040),
							in.lnt.utility.general.Cache
									.getPropertyFromError(ErrorCacheConstant.ERR_040 + Constants.PARAMS),
							paramValueList);
					}
				}
				// logger.debug(String.format("result..%s", result));
				String errorType = "";
				if (validationOBJ.get(Constants.ERROR_TYPE) != null)
					errorType = (String) validationOBJ.get(Constants.ERROR_TYPE).asText();
				if ((errorType.compareToIgnoreCase(Constants.ERROR) == 0)
						|| (errorType.compareToIgnoreCase(Constants.ALERT) == 0)
						|| (errorType.compareToIgnoreCase(Constants.ENHANCEMENT) == 0)) {
					if (result != null && result.equals("true")) {
	
						// Implementation start PE : 5414
	
						// PE-8269 Prasad Mail : MDA not applicable for crossectionchecks
						if (endPoint==Constants.ENDPOINT_PERFORM_DEFAULT_ACTIONS // PE-8397
								&& validationOBJ.get(Constants.MDA) != null
								&& !validationOBJ.get(Constants.MDA).asText().equals("")
								&& validationOBJ.get(Constants.ERROR_TYPE).asText().equals(Constants.ERROR)) {
							if (validationOBJ.get(Constants.MDA).asText().equalsIgnoreCase(Constants.EXECLUE_ROW)) {
								// logger.info(String.format("MDA value in
								// request..%s", Constants.EXECLUE_ROW));
								//((ObjectNode) (dataNode)).put(Constants.EXCLUDE_FLAG, "Y");
								List<String> columnNodeList_Section = new ArrayList<>();
								for(int index = 0; index < columnNodeList.size(); index++){
									columnNodeList_Section.add(columnNodeList.get(index).get(Constants.CODE).asText());
								}
								if(columnNodeList_Section != null && columnNodeList_Section.contains(Constants.EXCLUDE_FLAG)){//PE-7771
									((ObjectNode) (dataNode)).put(Constants.EXCLUDE_FLAG, Constants.ANSWER_YES);
									return obj;
								}
							} else if (validationOBJ.get(Constants.MDA).asText().equalsIgnoreCase(Constants.CLEAR_VALUE)) {
								// logger.info(String.format("MDA value in
								// request..%s", Constants.CLEAR_VALUE));
								((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), "");
								return obj;
							} else {// 7044 condition added
								// 7044 condition added
								String responseData = expressionEvaluatorForMDA(dataMap, validationOBJ, columnNode,
										cscDuplicateChkMap, metaDataColumnsOnly);
								if(null != responseData && !responseData.equals(Constants.INVALID_EXP)) {
									if (columnNode.get(Constants.DIMENSIONS) != null) {
										dimList = APIExpressionEvaluator
												.extractDimensions(columnNode.get(Constants.DIMENSIONS));
										dimNode = mapper.createObjectNode();
										for (String dim : dimList) {
											((ObjectNode) dimNode).put(dim, responseData);
										}
										((ObjectNode) (dataNode)).set(columnNode.get(Constants.CODE).asText(), dimNode);
									}
		
									else {
										((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), responseData);
									}
		
									return obj;
								}
								//PE-8609
								if( null == responseData ) {
									return null;
								}
							}
						}
	
					
						// Implementation end PE : 5414
	
						obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.expression, null, null,
								null, null); // PE-3711
					} else if (result != null && result.equals("false")) {
					} else {
						throw new CustomStatusException(Constants.HTTPSTATUS_400,
								in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_005));
					}
				} else if (errorType.compareToIgnoreCase("AUTOCORRECT") == 0) {
					if (columnNode.has(Constants.CODE) &&
							//&& null != dataMap.get(columnNode.get(Constants.CODE).asText())
							//&& !dataMap.get(columnNode.get(Constants.CODE).asText()).asText().equals(result)
							(JsonUtils.isNullOrBlankOrNullNode(dataMap.get(columnNode.get(Constants.CODE).asText()))
									|| (!JsonUtils.isNullOrBlankOrNullNode(
											dataMap.get(columnNode.get(Constants.CODE).asText()))
											&& !dataMap.get(columnNode.get(Constants.CODE).asText()).asText()
													.equals(result)))) {
						((ObjectNode) validationOBJ).set("OriginalValue",
								dataMap.get(columnNode.get(Constants.CODE).asText()));
						((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), result);
						obj = prepareOutputJson(validationOBJ, columnNode, result, ValidationTypes.expression, null, null,
								null, null);
					}
				}
	
			}
		}		
		// logger.info(String.format("%s %s", Constants.EXIT
		// ,Constants.LOG_EXPRESSIONEVALUATOR));
		
		
		return obj;
	}

	public ObjectNode checkMandatoryFields(HashMap<String, JsonNode> dataMap, String key, String dataType)
			throws Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_CHECKMANDATORYFIELDS));
		if (dataMap.get(key) == null || dataMap.get(key).asText().isEmpty()) {
			return prepareOutputJson(null, null, null, ValidationTypes.required, key, null, null, null);
		}
		// logger.info(String.format("%s %s", Constants.EXIT
		// ,Constants.LOG_CHECKMANDATORYFIELDS));
		return checkDataType(dataMap, key, dataType);
	}

	public ObjectNode checkDataType(HashMap<String, JsonNode> dataMap, String key, String dataType) throws Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_CHECKDATATYPE));
		Pattern INT_PATTERN = Pattern.compile(Constants.INT_PATTERN);
		Pattern DOUBLE_PATTERN = Pattern.compile(Constants.DOUBLE_PATTERN);
		Pattern LONG_PATTERN = Pattern.compile(Constants.LONG_PATTERN);
		ObjectNode obj = null;
		if (EnumUtils.isValidEnum(DataTypes.class, String.valueOf(dataType).toUpperCase())) {
			String value = String.valueOf(dataMap.get(key).asText()).replace(",", "");
			switch (DataTypes.valueOf(String.valueOf(dataType.toUpperCase()))) {
			case STRING:
				break;
			case INTEGER:
			case INT:
				if (((StringUtils.isEmpty(value)) || (value == "null")) || (INT_PATTERN.matcher(value).matches())) {
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
				}
				break;
			case DOUBLE:
				if (((StringUtils.isEmpty(value)) || (value == "null")) || (DOUBLE_PATTERN.matcher(value).matches())) {
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
				}
				break;
			case DATE:
				break;
			case BOOLEAN:
				break;
			case LONG:
				if (((StringUtils.isEmpty(value)) || (value == "null")) || (LONG_PATTERN.matcher(value).matches())) {
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
				}
				break;
			}
		}
		// logger.info(String.format("%s %s", Constants.EXIT
		// ,Constants.LOG_CHECKDATATYPE));
		return obj;
	}

	public ObjectNode checkValidationTypeOneOf(HashMap<String, JsonNode> dataMap, String key, JsonNode node,
			JsonNode validationNode) throws Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_CHECKVALIDATIONTYPEONEOF));
		ObjectNode obj = null;
		Pattern INT_PATTERN = Pattern.compile("^([+-]?[0-9]\\d*|0)$");
		Pattern DOUBLE_PATTERN = Pattern.compile("\\d+\\.\\d+");
		Pattern LONG_PATTERN = Pattern.compile("^-?\\d{1,19}$");
		String value = dataMap.get((node.get("code").asText())).asText();
		JsonNode valuesNode = validationNode.get("values");
		ArrayList<Object> valueList = new ArrayList<>();
		String values = "";

		for (int i = 0; i < valuesNode.size(); i++) {
			if (valuesNode != null) {
				valueList.add(valuesNode.get(i));
				if (i == 0)
					values = values + valuesNode.get(i).asText();
				else
					values = values + "," + valuesNode.get(i).asText();
			}

		}
		String modifiedValue = String.valueOf(value).replace(",", "");
		switch (validationNode.get("values").get(0).getNodeType()) {
		case NUMBER:
			if (DOUBLE_PATTERN.matcher(modifiedValue).matches()) {
				if (!valueList.contains(Double.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}
			} else if (INT_PATTERN.matcher(modifiedValue).matches()) {
				if (!valueList.contains(Integer.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}
			} else if (LONG_PATTERN.matcher(modifiedValue).matches()) {
				if (!valueList.contains(Integer.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}

			} else {

				obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
			}

			break;
		case STRING:
			if (!StringUtils.isBlank(value)) {
				if (!valueList.contains(String.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}
			} else {
				obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
			}
			break;

		}
		// logger.info(String.format("%s %s", Constants.EXIT
		// ,Constants.LOG_CHECKVALIDATIONTYPEONEOF));
		return obj;
	}
	/*
	 * For CSC end
	 */

	public ObjectNode checkDataType(HashMap<String, JsonNode> dataMap, String key, String dataType, String mappedColumn)
			throws Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_CHECKDATATYPE));
		Pattern INT_PATTERN = Pattern.compile(Constants.INT_PATTERN);
		Pattern DOUBLE_PATTERN = Pattern.compile(Constants.DOUBLE_PATTERN);
		Pattern LONG_PATTERN = Pattern.compile(Constants.LONG_PATTERN);
		ObjectNode obj = null;
		String value = "";
		if (EnumUtils.isValidEnum(DataTypes.class, String.valueOf(dataType).toUpperCase())) {
			if (key != null && !key.equals("")) {
				value = String.valueOf(dataMap.get(key).asText()).replace(",", "");
			} else {
				value = String.valueOf(dataMap.get(mappedColumn).asText()).replace(",", "");
			}
			switch (DataTypes.valueOf(String.valueOf(dataType.toUpperCase()))) {
			case STRING:
				break;
			case INTEGER:
			case INT:
				if (((StringUtils.isEmpty(value)) || (value == "null")) || (INT_PATTERN.matcher(value).matches())) {
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
				}
				break;
			case DOUBLE:
				if (((StringUtils.isEmpty(value)) || (value == "null")) || (DOUBLE_PATTERN.matcher(value).matches())) {
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
				}
				break;
			case DATE:
				break;
			case BOOLEAN:
				break;
			case LONG:
				if (((StringUtils.isEmpty(value)) || (value == "null")) || (LONG_PATTERN.matcher(value).matches())) {
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DataType, key, value, dataType, null);
				}
				break;
			}
		}
		// logger.info(String.format("%s %s", Constants.EXIT
		// ,Constants.LOG_CHECKDATATYPE));
		return obj;
	}

	public ObjectNode checkValidationTypeOneOf(HashMap<String, JsonNode> dataMap, String key, JsonNode node,
			JsonNode validationNode, String mappedColumn) throws Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_CHECKVALIDATIONTYPEONEOF));
		ObjectNode obj = null;
		Pattern INT_PATTERN = Pattern.compile("^([+-]?[0-9]\\d*|0)$");
		Pattern DOUBLE_PATTERN = Pattern.compile("\\d+\\.\\d+");
		Pattern LONG_PATTERN = Pattern.compile("^-?\\d{1,19}$");
		String value = dataMap.get((node.get("code").asText())).asText();
		JsonNode valuesNode = validationNode.get("values");
		ArrayList<Object> valueList = new ArrayList();
		String values = "";

		for (int i = 0; i < valuesNode.size(); i++) {
			if (valuesNode != null) {
				valueList.add(valuesNode.get(i));
				if (i == 0)
					values = values + valuesNode.get(i).asText();
				else
					values = values + "," + valuesNode.get(i).asText();
			}

		}
		String modifiedValue = String.valueOf(value).replace(",", "");
		switch (validationNode.get("values").get(0).getNodeType()) {
		case NUMBER:
			if (DOUBLE_PATTERN.matcher(modifiedValue).matches()) {
				if (!valueList.contains(Double.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}
			} else if (INT_PATTERN.matcher(modifiedValue).matches()) {
				if (!valueList.contains(Integer.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}
			} else if (LONG_PATTERN.matcher(modifiedValue).matches()) {
				if (!valueList.contains(Integer.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}

			} else {

				obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
			}

			break;
		case STRING:
			if (!StringUtils.isBlank(value)) {
				if (!valueList.contains(String.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
				}
			} else {
				obj = prepareOutputJson(null, null, null, ValidationTypes.oneOf, key, value, null, values);
			}
			break;

		}
		// logger.info(String.format("%s %s", Constants.EXIT
		// ,Constants.LOG_CHECKVALIDATIONTYPEONEOF));
		return obj;
	}

	private String[] extractFields(String field) throws Exception {
		String arr[] = null;
		if (field != null && field.length() > 0 && StringUtils.contains(field, "['")
				&& StringUtils.contains(field, "']")) {
			// ArrayUtils
			arr = StringUtils.split(field, "['");

		}
		return arr;
	}

	private String findInArray(JsonNode node, String value) throws Exception {

		// logger.debug(String.format("value...%s", value));
		boolean returnValue = false;
		if (null != node && node.getNodeType() == JsonNodeType.ARRAY) {
			Iterator<JsonNode> it = node.iterator();
			while (it.hasNext()) {
				returnValue = it.next().asText().equalsIgnoreCase(value);
				if (returnValue)
					break;
			}
		} else {
			if (null != node) {
				String arr[] = node.asText().split(",");
				if (arr != null && arr.length > 0) {
					for (int i = 0; i < arr.length; i++) {
						if (arr[i].equalsIgnoreCase(value))
							returnValue = true;
					}
				}
			}

		}
		// logger.debug(String.format("returnValue...%s", returnValue));
		return returnValue + "";
	}

	/*
	 * NOTE : Use this method to : Generate message(or result) for the
	 * prepareOutputJson() method depending upon the "errorType"
	 */
	public static String getMessageForAnErrorType(JsonNode validationOBJ, ValidationTypes errorType, String expr,
			String result) throws Exception {
		String message = null;
		if (errorType != null) {
			switch (errorType) {
			case expressionError:
				// PE-6660 : If "message" is provided by user in validationOBJ,
				// use it.
				// Else display default message.
				if (!JsonUtils.isNullOrBlankOrNullNode(validationOBJ.get("message"))) {
					if (StringUtils.isBlank(validationOBJ.get("message").asText())) {
						message = result;
					} else {
						message = validationOBJ.get("message").asText();
					}
				}
				break;

			case eeIdAutoCorrect:
				message = "Missing employee Id geneated:" + result;
				break;

			default:
				message = expr + " " + "ATTENTION: " + result;
				break;

			}
		} else {
			message = expr + " " + "ATTENTION: " + result;
		}
		return message;
	}

	public ObjectNode prepareOutputJson(JsonNode validationOBJ, JsonNode columnNode, String result,
			ValidationTypes errorType, String ky, String value, String datatype, String values) throws Exception {

		/*
		 * NOTE : In order to create result, use getMessageForAnErrorType()
		 * written above this method.
		 */

		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_PREPAREOUTPUTJSON));
		ObjectNode validation = mapper.createObjectNode();
		JsonNode paramNode = mapper.createObjectNode();
		String message = null;
		if (errorType != null) {
			switch (errorType) {
			case DataType:
				validation.put(Constants.FIELD, ky);
				
//				validation.put(Constants.MESSAGE_KEY, ky + " with value " +
//				value + " not matched with data type " + datatype);
				
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_045));
				((ObjectNode) paramNode).put("key", ky);
				((ObjectNode) paramNode).put("value", value);
				((ObjectNode) paramNode).put("datatype", datatype);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				break;
			case required:
				validation.put(Constants.FIELD, ky);
				// validation.put(Constants.MESSAGE_KEY, ky + " is required.");
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_041));
				((ObjectNode) paramNode).put("key", ky);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				break;
			case oneOf:
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.MESSAGE,
						value + " is not a valid value for this field.  Please choose from " + values);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_042));
				((ObjectNode) paramNode).put("value", value);
				((ObjectNode) paramNode).put("values", values);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				break;
			case expression:
				Iterator<String> it = validationOBJ.fieldNames();
				String key = "";
				while (it.hasNext()) {
					key = it.next();
					validation.put(key, validationOBJ.get(key).asText());
				}
				// PE-7071 : Blank CODE will be accepted.
				String field = JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.CODE))
						? (columnNode.get(Constants.CODE) == null ? null : "")
						: columnNode.get(Constants.CODE).asText();
				validation.put(Constants.FIELD, field);
				break;

			case dataTypeError:
				// logger.info(String.format("Check for the dataTypeError %s ",
				// validationOBJ));
				if (null != validationOBJ) {
					Iterator<String> itr = validationOBJ.fieldNames();
					// logger.info(String.format("Check for the dataTypeError %s
					// ", itr.hasNext()));
					String keyName = "";
					while (itr.hasNext()) {
						key = itr.next();
						validation.put(keyName, validationOBJ.get(keyName).asText());
					}
				}
				validation.put(Constants.FIELD, columnNode.get(Constants.CODE).asText());
				validation.put(Constants.VALIDATION_TYPE, "dataTypeCheck");
				validation.put(Constants.ERROR_TYPE, Constants.ERROR);
				// validation.put(Constants.MESSAGE, "Incorrect " + value + "
				// format");
				validation.put(Constants.ID, "dataType" + StringUtils.capitalize(datatype));
				/*validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_043));*/
				validation.put(Constants.MESSAGE_KEY,
						result);
				((ObjectNode) paramNode).put("value", value);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				// logger.info(String.format("validation object is..%s",
				// validation));
				break;

			case expressionError:
				Iterator<String> expIt = validationOBJ.fieldNames();
				String expKey = "";
				while (expIt.hasNext()) {
					expKey = expIt.next();
					validation.put(expKey, validationOBJ.get(expKey).asText());
				}
				validation.put(Constants.FIELD, ky);
				//validation.put(Constants.VALIDATION_TYPE, Constants.EXPRESSION);
				//validation.put(Constants.ERROR_TYPE, Constants.ERROR);
				if (!JsonUtils.isNullOrBlankOrNullNode(validationOBJ.get("message"))
						&& StringUtils.isBlank(validationOBJ.get("message").asText())) {
					validation.put(Constants.MESSAGE_KEY,
							in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_044));
					((ObjectNode) paramNode).put("result", result);
					validation.set(Constants.MESSAGE_PARAMS, paramNode);
				} else {
					validation.put(Constants.MESSAGE, result);
				}
				//validation.put("category", "input_data");
				break;
			case eeIdAutoCorrect:
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.VALIDATION_TYPE, Constants.EXPRESSION);
				validation.put(Constants.ERROR_TYPE, Constants.AUTOCORRECT);
				// validation.put(Constants.MESSAGE, result);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_039));
				((ObjectNode) paramNode).put("eeid", result);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				validation.put(Constants.ORIGINAL_VALUE, Constants.BLANK);
				break;

			case range:
				Iterator<String> it2 = validationOBJ.fieldNames();
				while (it2.hasNext()) {
					key = it2.next();
					if (validationOBJ.get(key).getNodeType() == JsonNodeType.ARRAY)
						validation.set(key, validationOBJ.get(key));
					else
						validation.put(key, validationOBJ.get(key).asText());
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.VALIDATION_TYPE, Constants.VALIDATION_RANGE);
				validation.put(Constants.ERROR_TYPE, value);
				validation.put(Constants.MESSAGE_KEY, result);
				validation.put("category", "input_data");
				if (!values.equals(Constants.BLANK))
					validation.put(Constants.DIMENSION, values);
				break;
			case rangeValidationRefTable:

				Iterator<String> it1 = validationOBJ.fieldNames();
				while (it1.hasNext()) {
					key = it1.next();
					if (validationOBJ.get(key).getNodeType() == JsonNodeType.ARRAY)
						validation.set(key, validationOBJ.get(key));
					else
						validation.put(key, validationOBJ.get(key).asText());
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.ERROR_TYPE, value);
				validation.put(Constants.MESSAGE_KEY, result);
				if (null != values && !values.equals(Constants.BLANK) )
					validation.put(Constants.DIMENSION, values);
				break;
			case eligibility:
				validation = (ObjectNode) validationOBJ;
				break;

			case dropDown:
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.ERROR_TYPE, Constants.ERROR);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_053));
				((ObjectNode) paramNode).put("values", values);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				break;

			case sequentialCheck:
				it = validationOBJ.fieldNames();
				key = "";
				while (it.hasNext()) {
					key = it.next();
					if (key.equalsIgnoreCase("dependentColumns")) {
						validation.put(key, validationOBJ.get(key));
					} else {
						validation.put(key, validationOBJ.get(key).asText());
					}
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.VALIDATION_TYPE, Constants.SEQUENTIAL_CHECK);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_054));
				((ObjectNode) paramNode).put("column", ky);
				((ObjectNode) paramNode).put("prevColumn", value);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				validation.put(Constants.ERROR_TYPE, Constants.ERROR);
				if (!values.equals(Constants.BLANK))
					validation.put(Constants.DIMENSION, values);
				break;
			case predefinedValidations:
				if (!JsonUtils.isNullOrBlankOrNullNode(validationOBJ)) {
					Iterator<String> predefinedIter = validationOBJ.fieldNames();
					String predefinedkey = "";
					while (predefinedIter.hasNext()) {
						predefinedkey = predefinedIter.next();
						validation.put(predefinedkey, validationOBJ.get(predefinedkey).asText());
					}
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_029));
				if (!JsonUtils.isNullOrBlankOrNullNode(columnNode)) {
					validation.put("dimension", columnNode.asText());
				}
				break;
			}
		} else {
			if (result != null && result.length() > 0) {
				validation.put(Constants.MESSAGE, result);
			}
		}

		// logger.debug(String.format("validation..%s", validation));
		// logger.info(String.format("%s %s", Constants.EXIT
		// ,Constants.LOG_PREPAREOUTPUTJSON));
		return validation;
	}

	public Map<String, Object> getParamValues(Map<String, JsonNode> dataMap, JsonNode validationOBJ,boolean checkBox,	// NK
			Map<String, String> columnMappingMap, List<JsonNode> columnNodeList, List<String> metaColnsOnly,boolean isHiddenQue)
			throws Exception {
		String patternString = "";
		String expr = "";
		String prefix = "this.";
		String fieldArr[] = null;
		boolean isExpressionFlag = true;
		Map<String, Object> paramValues = new HashMap<>();	// NK
		expr = validationOBJ.get(Constants.EXPRESSION).asText();// .replaceAll(Constants.CURLY_BRACES,
		boolean thisAndContextFlag = false;	 // PE - 7056													// "");

		patternString = expr;
		if (expr != null && expr.contains("bool_in_lookup")) {
			patternString = expr.replaceAll(Constants.REPLACE_FROM, Constants.REPLACE_TO);
		}
		Matcher matcher = pattern.matcher(patternString);
		List<String> fields = new ArrayList<>();
		while (matcher.find()) {
			fields.add(matcher.group().replace("this.", "").trim());
		}
		// PE - 7056	
		if( !fields.isEmpty() ) {  	
			thisAndContextFlag = true;
		}
		if ( thisAndContextFlag ) {
				if ( (!metaColnsOnly.isEmpty() &&  chekElementExist(metaColnsOnly,fields)) ||  isHiddenQue) { 
					isExpressionFlag = true;
				}else{
					isExpressionFlag = false;
				}
		 }
		if ( isExpressionFlag ) {
		// logger.debug(String.format("fields..%s", fields));
			String fieldtrim = "";
			JsonNode dataMap_field_node = null;
		for (String field : fields) {
			if (null != field) {
				fieldtrim = field.trim(); 
				if (fieldtrim.contains("contextData")) {
					prefix = prefix.replace("this.", "");
				}
				dataMap_field_node = dataMap.get(fieldtrim);
			}
			if (checkBox) {
				// logger.debug(String.format("checkBox..%s", fields));
				fieldArr = extractFields(fieldtrim);
				if (fieldArr != null) {
					paramValues.put(prefix + fieldtrim,
							(dataMap.get(fieldArr[0]) == null)
									? (dataMap.get(columnMappingMap.get(fieldArr[0].trim())) == null
											? (dataMap.get("otherSectionsData." + fieldArr[0].trim()) == null ? "null"
													: findInArray(dataMap.get("otherSectionsData." + fieldArr[0]),
															fieldArr[1]))
											: "null")
									: findInArray((dataMap.get(fieldArr[0].trim())), fieldArr[1]));
				} else {
					paramValues
							.put(prefix + fieldtrim,
									(JsonUtils.isNullOrBlankOrNullNode(dataMap_field_node))
											? (dataMap.get(columnMappingMap.get(fieldtrim)) == null
													? (dataMap
															.get("otherSectionsData." + fieldtrim) == null ? "null" : // To
																															// make
																															// it
																															// data
																															// type
																															// dependent
																	getContextDataValues(
																			getContextDataValues(dataMap
																					.get("otherSectionsData."
																							+ fieldtrim)
																					.asText().replace(",", ""))))
													: getDataTypeBasedValue(
															columnDataTypeMapping.getOrDefault(
																	columnMappingMap.get(fieldtrim), "dataType"),
															dataMap.get(columnMappingMap.get(fieldtrim)).asText()))
											: getDataTypeValues(fieldtrim, dataMap_field_node)//PE-7440 Method Extracted 

					);
				}
			} else {
				paramValues
						.put(prefix + fieldtrim,
								(JsonUtils
										.isNullOrBlankOrNullNode(
												dataMap_field_node))
														? (dataMap.get(columnMappingMap.get(fieldtrim)) == null
																? (dataMap.get(
																		"otherSectionsData." + fieldtrim) == null
																				? "null"
																				: dataMap
																						.get("otherSectionsData."
																								+ fieldtrim)
																						.asText().replace(",", ""))
																: getDataTypeBasedValue(
																		columnDataTypeMapping.getOrDefault(
																				columnMappingMap.get(fieldtrim),
																				"dataType"),
																		dataMap.get(columnMappingMap.get(fieldtrim))
																				.asText()))
														: getDataTypeValues(fieldtrim, dataMap_field_node));//PE-7440 Method Extracted 
			}
			prefix = "this.";
		}
		// Add expression
				paramValues.put(Constants.EXPRESSION_STRING, expr);
		}else {	
			paramValues.clear();
		}
		
		return paramValues;
	}

	/**
	 * @param field
	 * @param dataMapField
	 * @return
	 */
	//PE-7440
	protected Object getDataTypeValues(String field, JsonNode dataMapField) {
		
		List <String> dataMapList = new ArrayList<>();
		if( dataMapField.isArray()) {
			for (int i=0; i < dataMapField.size(); i++) {
				dataMapList.add("\""+dataMapField.get(i).asText()+"\"");	// PE-7795 (FIND_IN_SET fails for variable array)	// NK
			}	
			return dataMapList;
		}else {
			return getDataTypeBasedValue(
				columnDataTypeMapping.getOrDefault(field.trim(), "dataType"),
				dataMapField.asText().replace(",", ""));
		}
	}

	public String getDataTypeBasedValue(String type, String value) {
		try {
			if (!StringUtils.isBlank(value)) {
				switch (type) {
				case "int":
					return value;
				case "double":
					return df.format(Double.valueOf(value));
				case "text":
				case "string":
					return "\"" + value + "\"";
				case "boolean":
					return value;
				case "date":
					return "\"" + value + "\"";
				case "dataType":
					return getContextDataValues(value);
				}
			} else {
				return "null";
			}
		} catch (Exception e) {
			return "null";
		}
		return value;
	}

	public String getContextDataValues(String value) {
		try {
			if (!StringUtils.isBlank(value)) {

				// Commenting for PE-7773
//				if (NumberUtils.isNumber(value)) {
//					try {
//						Double d = Double.valueOf(value);
//						if ((d % 1) == 0) {
//							return value;
//						} else {
//							return df.format(Double.valueOf(value));
//						}
//					} catch (Exception e) {
//
//					}
//				} else {
					value = "\"" + value + "\""; // Modified for PE: 7254
					return value;
//				}
			} else {
				return "null";
			}
		} catch (Exception e) {
			return "null";
		}
//		return value;
	}

	@SuppressWarnings("unlikely-arg-type")
	public ArrayList<ObjectNode> prepareRangeObject(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, JsonNode dataNode) throws Exception {

		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_EXPRESSIONEVALUATOR));
		ObjectNode obj = null;
		String paramValue = "";
		String warning = null;
		String warningMsg = null;
		ArrayList<String> dimList = null;
		JsonNode code = null;
		StringBuffer key = null;
		ArrayList<ObjectNode> objList = new ArrayList<ObjectNode>();
		String value = "";
		HashMap<String, String> paramValueMap = null;
		// logger.info("Getting min,max for Alert,Error..");
		String minError = validationOBJ.has(Constants.MINIMUM_ERROR)
				? validationOBJ.get(Constants.MINIMUM_ERROR).asText() : null;
		String maxError = validationOBJ.has(Constants.MAXIMUM_ERROR)
				? validationOBJ.get(Constants.MAXIMUM_ERROR).asText() : null;
		String minAlert = validationOBJ.has(Constants.MINIMIUM_ALERT)
				? validationOBJ.get(Constants.MINIMIUM_ALERT).asText() : null;
		String maxAlert = validationOBJ.has(Constants.MAXIMUM_ALERT)
				? validationOBJ.get(Constants.MAXIMUM_ALERT).asText() : null;
		code = columnNode.get(Constants.CODE);

		if (code != null && columnNode.get(Constants.DIMENSIONS) != null) {
			paramValueMap = prepareParamValuesMap(code, columnNode.get(Constants.DIMENSIONS), dataMap);
		} else if (code != null) {
			paramValueMap = new HashMap<String, String>();
			if (null != dataMap && !JsonUtils.isNullOrBlankOrNullNode(code)
					&& !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(code.asText()))) {
				paramValue = dataMap.get(code.asText()).asText();
				paramValueMap.put(Constants.BLANK, paramValue);
			} else if (!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.MAPPED_COLUMN_NAME)) && !JsonUtils
					.isNullOrBlankOrNullNode(dataMap.get(columnNode.get(Constants.MAPPED_COLUMN_NAME).asText()))) {
				paramValue = dataMap.get(columnNode.get(Constants.MAPPED_COLUMN_NAME).asText()).asText();
				paramValueMap.put(Constants.BLANK, paramValue);

			}
		}

		if (paramValueMap != null && paramValueMap.size() > 0) {
			for (Map.Entry<String, String> entry : paramValueMap.entrySet()) {
				warning=null;
				warningMsg = null;
				value = entry.getValue();
				if (JsonUtils.isStringExist(value)) {
				if (DataUtility.isStringNotNullAndBlank(minError) && !StringUtils.isEmpty(value)
						&& Double.parseDouble(value) < Double.parseDouble(minError)) {
					warning = Constants.ERROR;
					// warningMsg = Constants.ERROR_MIN_MSG; //"minError";
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_024); // "minError";
				}
				if (null == warningMsg && DataUtility.isStringNotNullAndBlank(maxError)
						&& Double.parseDouble(value) > Double.parseDouble(maxError)) {
					warning = Constants.ERROR;
					// warningMsg = Constants.ERROR_MAX_MSG; // "maxError";
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_027); // "minError";

				}
				if (null == warningMsg && DataUtility.isStringNotNullAndBlank(minAlert)
						&& Double.parseDouble(value) < Double.parseDouble(minAlert)) {
					warning = Constants.ALERT;
					// warningMsg = Constants.ALERT_MIN_MSG; // "minAlert";
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_025); // "minError";
				}
				if (null == warningMsg && DataUtility.isStringNotNullAndBlank(maxAlert)
						&& Double.parseDouble(value) > Double.parseDouble(maxAlert)) {
					warning = Constants.ALERT;
					// warningMsg = Constants.ALERT_MAX_MSG; // "maxAlert";
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_026); // "minError";
				}
				}	
				// logger.info(String.format("warning :: %s warningMsg :: %s",
				// warning,warningMsg));
				if (warning != null) {
					obj = prepareOutputJson(validationOBJ, columnNode, warningMsg, ValidationTypes.range, code.asText(),
							warning, null, entry.getKey());
					objList.add(obj);
				}

			}
		}
		return objList;
	}

	public ObjectNode setValidationsForDimensions(JsonNode columnNode, JsonNode dataNode, JsonNode validationOBJ,
			String result, String type) {
		ObjectNode obj = null;

		JsonNode dimensionNode = dataNode.path(columnNode.get(Constants.CODE).asText()).deepCopy();
		for (JsonNode jsonNode : columnNode.get(Constants.DIMENSIONS)) {
			// Matching dimension Code within Expression (As no column/sequence
			// is present for dimensional Expression execution)
			try {
				if (validationOBJ.get(Constants.EXPRESSION).asText().contains(jsonNode.asText())) {
					switch (type) {
					case Constants.ERROR:
					case Constants.ENHANCEMENT:
					case Constants.ALERT:
						((ObjectNode) validationOBJ).put("dimension", jsonNode.asText());
						obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.expression, null, null,
								null, null);
						break;
					case Constants.AUTOCORRECT:
						if (!dimensionNode.get(jsonNode.asText()).asText().equals(result)) {
							((ObjectNode) validationOBJ).set("OriginalValue", dimensionNode.get(jsonNode.asText()));
							((ObjectNode) (dimensionNode)).put(jsonNode.asText(), result);
							((ObjectNode) validationOBJ).put("dimension", jsonNode.asText());
							obj = prepareOutputJson(validationOBJ, columnNode, result, ValidationTypes.expression, null,
									null, null, null);
						}
						break;
					}
				}
			} catch (Exception e) {

			}
		}
		((ObjectNode) (dataNode)).set(columnNode.get(Constants.CODE).asText(), dimensionNode);
		return obj;
	}

	// PE - 5568
	public ArrayNode checkValidity(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ, JsonNode columnNode,
			JsonNode dataNode, boolean isMDARequest,List<String> metaDataColumnsOnly) throws Exception {
		ArrayNode arr = mapper.createArrayNode();
		ObjectNode validation = mapper.createObjectNode();
		JsonNode paramNode = mapper.createObjectNode();
		if (validationOBJ != null && validationOBJ.has(Constants.DEPENDENT_COLUMNS)
				&& validationOBJ.get(Constants.DEPENDENT_COLUMNS) != null) {
			JsonNode depedentColumns = validationOBJ.get(Constants.DEPENDENT_COLUMNS);
			boolean performMDAAction = false;
			
			if (!JsonUtils.isNullOrBlankOrNullNode(dataNode.get(columnNode.get(Constants.CODE).asText())) && 
					(dataNode.get(columnNode.get(Constants.CODE).asText()).asText()).equals(Constants.ANSWER_YES)) {
				for (JsonNode depedentColumn : depedentColumns) {
					if ((JsonUtils.isNullOrBlankOrNullNode(dataNode.get(depedentColumn.asText()))
							|| (StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), "null")
									|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), ""))) 
							&& metaDataColumnsOnly.contains(depedentColumn.asText())) {
						if (!isMDARequest) {
							ObjectNode errorObject = mapper.createObjectNode();
							errorObject.put(Constants.VALIDATION_TYPE,
									validationOBJ.get(Constants.VALIDATION_TYPE).asText());
							errorObject.put(Constants.ERROR_TYPE, Constants.ALERT);
							errorObject.put(Constants.FIELD, depedentColumn.asText());
							
//							errorObject.put(Constants.MESSAGE_KEY,
//									"This incubent is not eligible for " + columnNode.get(Constants.CODE).asText()
//											+ "'s  " + columnNode.get(Constants.DISPLAYLABEL).asText()
//											+ ". Please indicate eligibility.");
							
							errorObject.put(Constants.MESSAGE_KEY,
									in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_058));
							//((ObjectNode) paramNode).put("code", columnNode.get(Constants.CODE).asText());
							// ((ObjectNode)paramNode).put("displayLabel",columnNode.get(Constants.DISPLAYLABEL).asText());
							((ObjectNode) paramNode).put("displayLabel",
									JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL)) ? ""
											: columnNode.get(Constants.DISPLAYLABEL).asText());
							errorObject.set(Constants.MESSAGE_PARAMS, paramNode);
							errorObject = prepareOutputJson(errorObject, null, null, ValidationTypes.eligibility, null,
									null, null, null);
							arr.add(errorObject);
						}
						// MDA ACTION
						performMDAAction = true;
					} else {
						continue;
					}
				}
				if (performMDAAction) {
					if (isMDARequest) {
						// Do Nothing
					}
				}

			} else if (!JsonUtils.isNullOrBlankOrNullNode(dataNode.get(columnNode.get(Constants.CODE).asText())) && 
					(dataNode.get(columnNode.get(Constants.CODE).asText()).asText()).equals(Constants.ANSWER_NO)) {
				// Hierarchy : No Answer, 0, Some Answer

				for (JsonNode depedentColumn : depedentColumns) {
					// Blank / null
					if (JsonUtils.isNullOrBlankOrNullNode(dataNode.get(depedentColumn.asText()))
							|| (StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), "null")
									|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), ""))) {
						continue;
					} 
					// zero
					else if (null != dataNode.get(depedentColumn.asText())
							&& dataNode.get(depedentColumn.asText()).asText().equals("0")) {
						((ObjectNode) (dataNode)).put(depedentColumn.asText(), "");
					} else {
						if (!isMDARequest) {
							ObjectNode errorObject = mapper.createObjectNode();

							errorObject.put(Constants.ERROR_TYPE, Constants.ERROR);
							errorObject.put(Constants.VALIDATION_TYPE,
									validationOBJ.get(Constants.VALIDATION_TYPE).asText());
							errorObject.put(Constants.FIELD, depedentColumn.asText());
						
//							errorObject.put(Constants.MESSAGE,
//									"This incubent is not eligible for " + columnNode.get(Constants.CODE).asText()
//											+ "'s  " + columnNode.get(Constants.DISPLAYLABEL).asText()
//											+ ". Please indicate eligibility.");
						
							errorObject.put(Constants.MESSAGE_KEY,
									in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_057));
							((ObjectNode) paramNode).put("baseQuestionLabel", columnNode.get(Constants.CODE).asText());
							((ObjectNode) paramNode).put("displayLabel",
									JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL)) ? ""
											: columnNode.get(Constants.DISPLAYLABEL).asText());
							errorObject.set(Constants.MESSAGE_PARAMS, paramNode);
							errorObject = prepareOutputJson(errorObject, null, null, ValidationTypes.eligibility, null,
									null, null, null);
							arr.add(errorObject);
						}
						// MDA ACTION
						performMDAAction = true;

					}
				}
				if (performMDAAction) {
					if (isMDARequest) {
						((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), Constants.ANSWER_YES);
					}
				}

			} else {
				boolean someAns=false;
				for (JsonNode depedentColumn : depedentColumns) {
					if (JsonUtils.isNullOrBlankOrNullNode(dataNode.get(depedentColumn.asText()))
							|| (StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), "null")
									|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), ""))) {
						performMDAAction = true;

					} else {
						someAns=true;
						break;
					}
				}
				
				if(someAns)
				{
					((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), Constants.ANSWER_YES);
					for (JsonNode depedentColumn : depedentColumns)
					{
					/*if (JsonUtils.isNullOrBlankOrNullNode(dataNode.get(depedentColumn.asText())) 
							&& !JsonUtils.isStringExist(dataNode.get(depedentColumn.asText()).asText()))*/
						if ((JsonUtils.isNullOrBlankOrNullNode(dataNode.get(depedentColumn.asText()))
								|| (StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), "null")
										|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), ""))) 
								&& metaDataColumnsOnly.contains(depedentColumn.asText()))
					{
					ObjectNode errorObject = mapper.createObjectNode();
					errorObject.put(Constants.ERROR_TYPE, Constants.ALERT);
					errorObject.put(Constants.VALIDATION_TYPE, validationOBJ.get(Constants.VALIDATION_TYPE).asText());
					errorObject.put(Constants.FIELD, depedentColumn.asText());
					errorObject.put(Constants.MESSAGE_KEY,
							in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_058));
					//((ObjectNode) paramNode).put("code", columnNode.get(Constants.CODE).asText());
					((ObjectNode) paramNode).put("displayLabel",
							JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL)) ? ""
									: columnNode.get(Constants.DISPLAYLABEL).asText());
					errorObject.set(Constants.MESSAGE_PARAMS, paramNode);
					errorObject = prepareOutputJson(errorObject, null, null, ValidationTypes.eligibility, null, null,
							null, null);
					arr.add(errorObject);
					}
				}
				}
				else
				{
					ObjectNode errorObject = mapper.createObjectNode();
					errorObject.put(Constants.ERROR_TYPE, Constants.ALERT);
					errorObject.put(Constants.VALIDATION_TYPE, validationOBJ.get(Constants.VALIDATION_TYPE).asText());
					errorObject.put(Constants.FIELD, columnNode.get(Constants.CODE).asText());
					errorObject.put(Constants.MESSAGE_KEY,
							in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_050));
					((ObjectNode) paramNode).put("code", columnNode.get(Constants.CODE).asText());
					((ObjectNode) paramNode).put("displayLabel",
							JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL)) ? ""
									: columnNode.get(Constants.DISPLAYLABEL).asText());
					errorObject.set(Constants.MESSAGE_PARAMS, paramNode);
					arr.add(errorObject);
				}

				if (!someAns) {
					if (isMDARequest) {
						((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), Constants.ANSWER_NO);
					}
				}
			}
		}
		return arr;
	}

	public ArrayList<ObjectNode> prepareRefernceRangeObject(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, JsonNode dataNode) throws Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_PREPAREREFENCERANGEOBJECT));
		StringBuffer query = new StringBuffer();
		// ColumnList
		List<String> columnList = null;
		// ColumnMap
		Map<String, String> colMap = null;
		long startTime = 0;
		HashMap<String, String> rangeMap = null;
		String paramValue = "";
		JsonNode code = null;
		JsonNode mappedColName = null;
		HashMap<String, String> paramValueMap = null;
		String tableName = null;
		ArrayList<String> paramValueList = null;
		code = columnNode.get(Constants.CODE);
		mappedColName = columnNode.get(Constants.MAPPED_COLUMN_NAME);
		String metaDatatableName = null;
		ArrayList<ObjectNode> objList = new ArrayList<ObjectNode>();
		List<Object> list = null;
		ObjectNode objNode;
		List<String> tableList = new ArrayList<>();
		// ******************Start Cache Logic For Testing then Development
		// Purpose******************************** PE-8609(500 Error)

		CacheManager cm = CacheManager.getInstance();
		net.sf.ehcache.Cache cache = cm.getCache("cacheStore");
		Object cacheObj = cache.get("allTableList").getObjectValue();
		if (null != cacheObj) {
			list = Arrays.asList(cacheObj);
			for (Object ob : list) {
				tableList.add(ob.toString());
			}
		}

		// ******************End Cache Logic For Testing then Development
		// Purpose********************************
		metaDatatableName = validationOBJ.get(Constants.REFERENCE_TABLE_NAME).asText();

		if (!StringUtils.isEmpty(metaDatatableName)) {
			tableName = metaDatatableName;
		} else {
			throw new CustomStatusException(Constants.HTTPSTATUS_400,
					in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_051));
		}
		// PE-8609(500 Error)
		if (!CollectionUtils.isEmpty(list) && !tableList.get(0).contains(metaDatatableName.toUpperCase())) {
			objNode = prepareOutputJson(validationOBJ, columnNode,
					in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_051),
					ValidationTypes.rangeValidationRefTable, columnNode.get(Constants.CODE).asText(), "ERROR", null,
					null);

			// Logging error for ELK Stack to separate error file expression_error.log
			logger.warn(objNode.toString());
		} else {
			if (code != null && columnNode.get(Constants.DIMENSIONS) != null) {
				paramValueMap = prepareParamValuesMap(code, columnNode.get(Constants.DIMENSIONS), dataMap);
			} else if (code != null) {
				paramValueMap = new HashMap<String, String>();
				paramValue = getParamValuesRange(code, dataMap, mappedColName);
				paramValueMap.put(Constants.BLANK, paramValue);
			}
			query.append(" SELECT ERROR_MIN,ERROR_MAX,ALERT_MIN,ALERT_MAX FROM " + tableName + " WHERE ");
			startTime =  System.currentTimeMillis();
			columnList = DBUtils.getRefTableMetaData(tableName);
			// colMap
			colMap = DBUtils.getTableSchemaData(tableName);
			
		/*	for (Map.Entry<String, String> entry : colMap.entrySet()) {
					if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText().equals("")
							&& entry.getKey().equalsIgnoreCase("CTX_CTRY_CODE")) {
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(entry.getKey() + "='"
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText() + "'");
						} else {
							query.append(entry.getKey() + "="
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText() + "");
						}
				   }
			}*/
/*			// PE-7689 Start
			int j = 0;
			for (int i = 0; i < columnList.size(); i++) {
				if (j == 0) {
					if (columnList.get(i).equalsIgnoreCase("CTX_CTRY_CODE")
							&& !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText().equals("")) {
						j++;
						query.append(columnList.get(i) + "='"
								+ (dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText() + "'");
					}

					else if (columnList.get(i).equalsIgnoreCase("CTX_SUPER_SECTOR")
							&& !JsonUtils.isNullOrBlankOrNullNode(
									dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector")).asText().equals("")) {
						j++;
						query.append(columnList.get(i) + "='"
								+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector")).asText() + "'");
					} else if (columnList.get(i).equalsIgnoreCase("CTX_SECTOR")
							&& !JsonUtils
									.isNullOrBlankOrNullNode(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector")).asText().equals("")) {
						j++;
						query.append(columnList.get(i) + "='"
								+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector")).asText() + "'");
					} else if (columnList.get(i).equalsIgnoreCase("CTX_SUBSECTOR")
							&& !JsonUtils.isNullOrBlankOrNullNode(
									dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector")).asText().equals("")) {
						j++;
						query.append(columnList.get(i) + "='"
								+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector")).asText() + "'");
					} else if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(columnList.get(i)))
							&& !dataMap.get(columnList.get(i)).asText().equals("")) {
						j++;
						query.append(columnList.get(i) + "='" + (dataMap.get(columnList.get(i)).asText() + "'"));
					}
				} else {

					if (columnList.get(i).equalsIgnoreCase("CTX_CTRY_CODE")
							&& !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText().equals("")) {
						j++;
						query.append(" and " + columnList.get(i) + "='"
								+ (dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText() + "'");
					}

					else if (columnList.get(i).equalsIgnoreCase("CTX_SUPER_SECTOR")
							&& !JsonUtils.isNullOrBlankOrNullNode(
									dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector")).asText().equals("")) {
						j++;
						query.append(" and " + columnList.get(i) + "='"
								+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector")).asText() + "'");
					} else if (columnList.get(i).equalsIgnoreCase("CTX_SECTOR")
							&& !JsonUtils
									.isNullOrBlankOrNullNode(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector")).asText().equals("")) {
						j++;
						query.append(" and " + columnList.get(i) + "='"
								+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector")).asText() + "'");
					} else if (columnList.get(i).equalsIgnoreCase("CTX_SUBSECTOR")
							&& !JsonUtils.isNullOrBlankOrNullNode(
									dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector")).asText().equals("")) {
						j++;
						query.append(" and " + columnList.get(i) + "='"
								+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector")).asText() + "'");
					} else if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(columnList.get(i)))
							&& !dataMap.get(columnList.get(i)).asText().equals("")) {
						j++;
						query.append(
								" and " + columnList.get(i) + "='" + (dataMap.get(columnList.get(i)).asText() + "'"));
					}

				}
			}
*/
			//Looping over column and column data type
			
			// PE-7689 Start
			int j = 0;
			for (Map.Entry<String, String> entry : colMap.entrySet()) {
				if (j == 0) {
					if (entry.getKey().equalsIgnoreCase("CTX_CTRY_CODE")
							&& !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText().equals("")) {
						j++;
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(entry.getKey() + "='"
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText() + "'");
						} else {
							query.append(entry.getKey() + "="
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText());
						}
					}

					else if (entry.getKey().equalsIgnoreCase("CTX_SUPER_SECTOR")
							&& !JsonUtils.isNullOrBlankOrNullNode(
									dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector")).asText().equals("")) {
						j++;
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(entry.getKey() + "='"
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector")).asText() + "'");
						} else {
							query.append(entry.getKey() + "="
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector")).asText() );
						}
						
					} else if (entry.getKey().equalsIgnoreCase("CTX_SECTOR")
							&& !JsonUtils
									.isNullOrBlankOrNullNode(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector")).asText().equals("")) {
						j++;
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(entry.getKey() + "='"
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector")).asText() + "'");
						} else {
							query.append(entry.getKey() + "="
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector")).asText() );
						}
					} else if (entry.getKey().equalsIgnoreCase("CTX_SUBSECTOR")
							&& !JsonUtils.isNullOrBlankOrNullNode(
									dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector")).asText().equals("")) {
						j++;
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(entry.getKey() + "='"
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector")).asText() + "'");
						} else {
							query.append(entry.getKey() + "="
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector")).asText() );
						}
					} else if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(entry.getKey()))
							&& !dataMap.get(entry.getKey()).asText().equals("")) {
						j++;
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(entry.getKey() + "='" + (dataMap.get(entry.getKey()).asText() + "'"));
						} else {
							query.append(entry.getKey() + "=" + (dataMap.get(entry.getKey()).asText()));
						}
						
					}
				} else {

					if (entry.getKey().equalsIgnoreCase("CTX_CTRY_CODE")
							&& !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText().equals("")) {
						j++;
						
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(" and " + entry.getKey() + "='"
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText() + "'");
						} else {
							query.append(" and " + entry.getKey() + "="
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".ctryCode")).asText() );
						}
					
					}

					else if (entry.getKey().equalsIgnoreCase("CTX_SUPER_SECTOR")
							&& !JsonUtils.isNullOrBlankOrNullNode(
									dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector")).asText().equals("")) {
						j++;
						
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(" and " + entry.getKey() + "='"
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector")).asText() + "'");
						} else {
							query.append(" and " + entry.getKey() + "="
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.superSector")).asText() );
						}
						
					} else if (entry.getKey().equalsIgnoreCase("CTX_SECTOR")
							&& !JsonUtils
									.isNullOrBlankOrNullNode(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector")).asText().equals("")) {
						j++;
						
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(" and " + entry.getKey() + "='"
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector")).asText() + "'");
						} else {
							query.append(" and " + entry.getKey() + "="
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.sector")).asText() );
						}
						
					} else if (entry.getKey().equalsIgnoreCase("CTX_SUBSECTOR")
							&& !JsonUtils.isNullOrBlankOrNullNode(
									dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector"))
							&& !(dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector")).asText().equals("")) {
						j++;
						
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(" and " + entry.getKey() + "='"
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector")).asText() + "'");
						} else {
							query.append(" and " + entry.getKey() + "="
									+ (dataMap.get(Constants.CONTEXTDATAKEY + ".industry.subSector")).asText() );
						}
						
					} else if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(entry.getKey()))
							&& !dataMap.get(entry.getKey()).asText().equals("")) {
						j++;
						if (DBUtils.getRealDataType(entry.getValue())) {
							query.append(" and " + entry.getKey() + "='" + (dataMap.get(entry.getKey()).asText() + "'"));

						} else {
							query.append(" and " + entry.getKey() + "=" + (dataMap.get(entry.getKey()).asText()));

						}
					}

				}
			}

			query.append(CustomFunctions.FIRST_ROW);

			// PE-7689 End
			// logger.info(String.format("query..%s", query));
			startTime = System.currentTimeMillis();
			rangeMap = DBUtils.getRangeDetails(query.toString());
			// logger.debug(String.format("Time taken for getRangeDetails() in
			// prepareRefernceRangeObject..%s", (System.currentTimeMillis() -
			// startTime)));
			if (null != rangeMap) {
				if (null != rangeMap.get("SQLException")) {
					return null;
				}
				objList = getRangeResult(paramValueMap, validationOBJ, columnNode, paramValue,
						rangeMap.get("ERROR_MIN"), rangeMap.get("ALERT_MIN"), rangeMap.get("ERROR_MAX"),
						rangeMap.get("ALERT_MAX"));
			}
		}
		return objList;

	}

	// Reference table range logic check PE : 6738, PE 6027 start
	public static String getParamValuesRange(JsonNode code, HashMap<String, JsonNode> dataMap, JsonNode mappedColName)
			throws Exception {
		String paramValue = "";
		if (code != null && !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(code.asText()))) {
			if (dataMap.get(code.asText()).asText() != null
					&& !dataMap.get(code.asText()).asText().equals(Constants.BLANK)) {
				paramValue = dataMap.get(code.asText()).asText();
			} else if (mappedColName != null && mappedColName.asText() != null) {
				if (dataMap.get(mappedColName.asText()).asText() != null
						&& !dataMap.get(mappedColName.asText()).asText().equals(Constants.BLANK)) {
					paramValue = dataMap.get(mappedColName.asText()).asText();
				}
			}
		}
		return paramValue;
	}
	// Reference table range logic check PE : 6738

	public static ArrayList<ObjectNode> getRangeResult(HashMap<String, String> paramValueMap, JsonNode validationOBJ,
			JsonNode columnNode, String paramValue, String minError, String minAlert, String maxError, String maxAlert)
			throws Exception {

		String warning = "";
		String warningMsg = null;
		String value = "";
		ObjectNode obj = null;
		ArrayList<ObjectNode> objList = new ArrayList<ObjectNode>();

		if (paramValueMap != null && paramValueMap.size() > 0) {
			for (Map.Entry<String, String> entry : paramValueMap.entrySet()) {
				warningMsg = null;
				warning="";
				value = entry.getValue();
				if (JsonUtils.isStringExist(value)) {
					if (DataUtility.isStringNotNullAndBlank(minError) && !StringUtils.isEmpty(value)
							&& Double.parseDouble(value) < Double.parseDouble(minError)) {
						warning = Constants.ERROR;
						// warningMsg = Constants.ERROR_MIN_MSG; //"minError";
						warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_024);
					}
					if (null == warningMsg && DataUtility.isStringNotNullAndBlank(maxError)
							&& Double.parseDouble(value) > Double.parseDouble(maxError)) {
						warning = Constants.ERROR;
						// warningMsg = Constants.ERROR_MAX_MSG; // "maxError";
						warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_027);
					}
					if (null == warningMsg && DataUtility.isStringNotNullAndBlank(minAlert)
							&& Double.parseDouble(value) < Double.parseDouble(minAlert)) {
						warning = Constants.ALERT;
						// warningMsg = Constants.ALERT_MIN_MSG; // "minAlert";
						warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_025);
					}
					if (null == warningMsg && DataUtility.isStringNotNullAndBlank(maxAlert)
							&& Double.parseDouble(value) > Double.parseDouble(maxAlert)) {
						warning = Constants.ALERT;
						// warningMsg = Constants.ALERT_MAX_MSG; // "maxAlert";
						warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_026);
					}
				}
				// logger.info(String.format("warning :: %s warningMsg :: %s",
				// warning,warningMsg));
				if (JsonUtils.isStringExist(warningMsg) && JsonUtils.isStringExist(warning)) {
					if (warning.equals(Constants.ERROR))
						obj = new APIExpressionEvaluator().prepareOutputJson(validationOBJ, columnNode, warningMsg,
								ValidationTypes.rangeValidationRefTable, columnNode.get(Constants.CODE).asText(),
								"ERROR", null, entry.getKey()); // PE-3711
					else if (warning.equals(Constants.ALERT))
						obj = new APIExpressionEvaluator().prepareOutputJson(validationOBJ, columnNode, warningMsg,
								ValidationTypes.rangeValidationRefTable, columnNode.get(Constants.CODE).asText(),
								"ALERT", null, entry.getKey());
					objList.add(obj);
				}

			}
		}
		return objList;
	}
	// Reference table range logic check PE : 6738, PE 6027 end

	public static HashMap<String, String> prepareParamValuesMap(JsonNode code, JsonNode dimension,
			HashMap<String, JsonNode> dataMap) throws Exception {

		ArrayList<String> dimList = null;
		StringBuffer key = null;
		key = new StringBuffer();
		HashMap<String, String> paramValueMap = new HashMap<String, String>();
		dimList = extractDimensions(dimension);

		for (int i = 0; i < dimList.size(); i++) {
			key.delete(0, key.length());
			key.append(code.asText()).append(".").append(dimList.get(i));

			if (dataMap.get(key.toString()) != null) {
				paramValueMap.put(dimList.get(i), dataMap.get(key.toString()).asText());
			}
		}
		return paramValueMap;

	}

	public static ArrayList<String> extractDimensions(JsonNode node) throws Exception {
		ArrayList<String> list = new ArrayList<String>();
		if (node != null && node.getNodeType() == JsonNodeType.ARRAY) {
			Iterator<JsonNode> it = node.iterator();
			while (it.hasNext()) {
				list.add(it.next().asText());
			}
		}
		return list;
	}

	// PE7044 This method will execute the MDA Set Default expression and return
	// the result of the expression
	public String expressionEvaluatorForMDA(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, Map<String, Integer> cscDuplicateChkMap, List<String> metaDataColumns) throws Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_EXPRESSIONEVALUATOR));
		String expr = "";
		String result = "";
		boolean checkBox = false;
		String fieldArr[] = null;
		String prefix = "this.";
		String patternString = "";
		ArrayList<String> paramValueList = null;
		Pattern pattern = Pattern.compile(Constants.EXP_PATTERN);
		ObjectNode obj = null;
		boolean isHiddenQue=false;
		// Start PE- 7056
		ArrayList<String>  metaDataColumnsOnly= (ArrayList<String>) metaDataColumns;
		boolean isExpressionFlag = true;
		boolean thisAndContextFlag = false; 
		// End PE- 7056
		
		if (columnNode != null && columnNode.get(Constants.QUESTION_TYPE) != null
				&& columnNode.get(Constants.QUESTION_TYPE).asText().equalsIgnoreCase("checkboxes")) {
			checkBox = true;
			pattern = Pattern.compile(Constants.EXP_PATTERN_CHECKBOX);
		} else {
			pattern = Pattern.compile(Constants.EXP_PATTERN);
		}
		if (!JsonUtils.isNullOrBlankOrNullNode(columnNode) && !JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.HIDDEN)) 
				&& columnNode.get(Constants.HIDDEN).asText().equalsIgnoreCase("true")) {
			isHiddenQue = true;
		} 
		Map<String, Object> paramValues = new HashMap<>();
		if (validationOBJ != null && validationOBJ.get(Constants.MDA) != null) {
			expr = validationOBJ.get(Constants.MDA).asText().replaceAll(Constants.CURLY_BRACES, "");
			patternString = expr;
			if (expr != null && expr.contains("bool_in_lookup")) {
				patternString = expr.replaceAll(Constants.REPLACE_FROM, Constants.REPLACE_TO);
			}
			Matcher matcher = pattern.matcher(patternString);
			List<String> fields = new ArrayList<String>();
			while (matcher.find()) {
				fields.add(matcher.group().replace("this.", "").trim());
			}
			
			 // PE - 7056	
			if( !fields.isEmpty() ) {  	
				thisAndContextFlag = true;
			}
			if ( thisAndContextFlag ) {
					if ( (!metaDataColumnsOnly.isEmpty() &&  chekElementExist(metaDataColumnsOnly,fields)) || isHiddenQue) { 
						isExpressionFlag = true;
					}else{
						isExpressionFlag = false;
					}
			 }
			if ( isExpressionFlag ) {   // PE - 7056
				String fieldtrim = "";
			for (String field : fields) {
				if (null != field) {
					fieldtrim = field.trim();
					if (fieldtrim.contains("contextData")) {
						prefix = prefix.replace("this.", "");
					}
				}
				if (checkBox) {
					fieldArr = extractFields(fieldtrim);
					if (fieldArr != null) {
						paramValues.put(prefix + fieldtrim, (dataMap.get(fieldArr[0]) == null)
								? (dataMap.get("otherSectionsData." + fieldArr[0].trim()) == null ? "null"
										: findInArray(dataMap.get("otherSectionsData." + fieldArr[0]), fieldArr[1]))
								: findInArray(dataMap.get(fieldArr[0]), fieldArr[1]));
					} else {
						paramValues.put(prefix + fieldtrim, (dataMap.get(fieldtrim) == null)
								? (dataMap.get("otherSectionsData." + fieldtrim) == null ? "null"
										: getContextDataValues(dataMap.get("otherSectionsData." + fieldtrim).asText()
												.replace(",", "")))
								: getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(fieldtrim, "dataType"),
										dataMap.get(fieldtrim).asText().replace(",", "")));
					}
				} else {
					String tmp = null;
					if (dataMap.get(fieldtrim) == null && !isHiddenQue) {
						if (dataMap.get("otherSectionsData." + fieldtrim) == null) {
							if (cscDuplicateChkMap != null && cscDuplicateChkMap.containsKey(fieldtrim)) {
								paramValueList = new ArrayList<String>();
								paramValueList.add("" + cscDuplicateChkMap.get(fieldtrim));
								paramValueList.add("" + fieldtrim);
								paramValueList.add("" + expr);
								throw new CustomStatusException(Constants.HTTPSTATUS_400,
										in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_048),
										in.lnt.utility.general.Cache.getPropertyFromError(
												ErrorCacheConstant.ERR_048 + Constants.PARAMS),
										paramValueList);// Cannot find variable
														// {1} in expression {2}
							} else {
								// TODO Change JSONException to CustomStatusException below
								throw new JSONException(String.format(Constants.ERROR_CANNOT_FIND_VARIABLE_IN_EXPR,
										fieldtrim, expr));
							}
						} else {
							tmp = getContextDataValues(
									dataMap.get("otherSectionsData." + fieldtrim).asText().replace(",", ""));
						}
					} else {
						tmp = getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(fieldtrim, "dataType"),
								dataMap.get(fieldtrim).asText().replace(",", ""));
					}
					paramValues.put(prefix + fieldtrim, tmp);
				}
				prefix = "this.";
			}
			expr = expr.replace("null", "\'null\'");
			try {
				result = expressionEvalutionDriver.expressionEvaluatorWithObject(paramValues, expr, false);

			} catch (ExpressionEvaluatorException e) {
				logger.warn("Erroneous expression is: "+ expr +", Reason " +e.getMessage() );
				return null;
			}catch (Exception e) {
				logger.error("Excepion occured in expressionEvaluator.." + e.getMessage());
				paramValueList = new ArrayList<String>();
				paramValueList.add(e.getMessage());
				result = Constants.INVALID_EXP;//PE-8032
			  }
		   }else {//PE-8032
				result = Constants.INVALID_EXP;
			}
		}
		return result;
	}
	
	/**
	 * @author Akhileshwar
	 * @throws Exception
	 * PE-7045 List of validation objects for the node on the basis of sequential validation
	 */
	
	public List<ObjectNode> performSequentialCheck(Map<String, JsonNode> dataMap, JsonNode validationNode,
			JsonNode node, JsonNode dataNode, Map<String, JsonNode> dimMap, Map<String, String> columnDataTypeMap,
			boolean uploadMultipleFile, List<String> metaDataColumnsOnly)
			throws Exception {
		ObjectNode obj = null;
		List<ObjectNode> objList = new ArrayList<>();
		String dataType = "";
		String currentColumn = "";
		String nextColumn = "";
		JsonNode dimNode = null;
		List<String> dimList;
		List<String> nextDimList;
		String result = "";

		boolean flag = true;
		boolean nodeDimension = false;
		boolean nextNodeDimension = false;
		String dimensionExtension = "";
		Map<String, List<String>> colDimMap = new HashMap<String, List<String>>();
		StringBuffer colBuffer = new StringBuffer();
		Pattern INT_PATTERN = Pattern.compile(Constants.INT_PATTERN);
		Pattern DOUBLE_PATTERN = Pattern.compile(Constants.DOUBLE_PATTERN);
		Pattern LONG_PATTERN = Pattern.compile(Constants.LONG_PATTERN);

		String currentVal = "";
		String nextVal = "";
		JsonNode valuesNode = validationNode.get("dependentColumns");
		ArrayList<String> columnList = new ArrayList<>();
		ArrayList<String> columnListTemp = new ArrayList<>();

		for (int i = 0; i < valuesNode.size(); i++) {
		 if(metaDataColumnsOnly.contains(valuesNode.get(i).asText())){//PE 7989 if condition added
			dataType = columnDataTypeMap.get(valuesNode.get(i).asText());
			if (dataType.equalsIgnoreCase(Constants.INT) || dataType.equalsIgnoreCase(Constants.INTEGER)
					|| dataType.equalsIgnoreCase(Constants.BIGINT) || dataType.equalsIgnoreCase(Constants.FLOAT)
					|| dataType.equalsIgnoreCase(Constants.DOUBLE)) {

				columnListTemp.add(valuesNode.get(i).asText());
			}
			}
		}
		if (columnListTemp.size() > 1) {
			for (String col : columnListTemp) {
				flag = false;
				dimNode = dimMap.get(col);
				if (dimNode != null) {
					dimList = APIExpressionEvaluator.extractDimensions(dimNode);
					for (String dimension : dimList) {
						colBuffer.delete(0, colBuffer.length());
						colBuffer.append(col).append(".").append(dimension);
						if (dataMap != null && !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(colBuffer.toString()))
								&& !dataMap.get(colBuffer.toString()).asText().isEmpty()) {
							flag = true;
							colDimMap.put(col, dimList);
							break;
						}
					}

				} else {
					if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(col)) && !dataMap.get(col).asText().isEmpty()
							&& !"null".equalsIgnoreCase(dataMap.get(col).asText().trim())) {
						flag = true;
					}
				}
				if (flag) {
					columnList.add(col);
				}
			}
		}

		if (columnList.size() > 1) {
			for (int i = 0; i < columnList.size() - 1; i++) {
				String thisColumnDefault = columnList.get(i);
				String nextColumnDefault = columnList.get(i + 1);

				dimList = null;
				nextDimList = null;
				nodeDimension = false;
				nextNodeDimension = false;
				dimList = colDimMap.get(thisColumnDefault);
				nextDimList = colDimMap.get(nextColumnDefault);

				if (dimList != null && nextDimList != null) {
					dimList.retainAll(nextDimList);
					nodeDimension = true;
					nextNodeDimension = true;
				} else if (dimList == null && nextDimList != null) {
					dimList = nextDimList;
					nextNodeDimension = true;
				} else if (dimList != null && nextDimList == null) {
					nodeDimension = true;
				}

				currentColumn = thisColumnDefault;
				nextColumn = nextColumnDefault;

				if (nodeDimension == true || nextNodeDimension == true) {
					if (dimList != null) {
						for (String dim : dimList) {
							dimensionExtension = "." + dim;
							currentColumn = thisColumnDefault;
							nextColumn = nextColumnDefault;
							if (nodeDimension) {
								currentColumn += dimensionExtension;
							}

							if (nextNodeDimension) {
								nextColumn += dimensionExtension;
							}
							if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(currentColumn))) { // MAY
																									// FAIL
								currentVal = dataMap.get(currentColumn).asText();
							} else {
								currentVal = null;
							}

							if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(nextColumn))) { // MAY
																								// FAIL
								nextVal = dataMap.get(nextColumn).asText();
							} else {
								nextVal = null;
							}

							if (currentVal != null && !currentVal.isEmpty() && nextVal != null && !nextVal.isEmpty()) {
								if (((INT_PATTERN.matcher(currentVal).matches())
										|| (DOUBLE_PATTERN.matcher(currentVal).matches())
										|| (LONG_PATTERN.matcher(currentVal).matches()))
										&& ((INT_PATTERN.matcher(nextVal).matches())
												|| (DOUBLE_PATTERN.matcher(nextVal).matches())
												|| (LONG_PATTERN.matcher(nextVal).matches()))) {
									if (dataMap.get(currentColumn).asDouble() >= dataMap.get(nextColumn).asDouble()) {
										currentColumn = thisColumnDefault;
										nextColumn = nextColumnDefault;
										dimNode = mapper.createObjectNode();
										for (String dim1 : dimList) {
											if (dim1.equals(dim) && !uploadMultipleFile) {
												((ObjectNode) dimNode).put(dim1, "");
											} else {
												String val = null;
												if (!JsonUtils.isNullOrBlankOrNullNode(
														dataMap.get(currentColumn + "." + dim1))) {
													val = dataMap.get(currentColumn + "." + dim1).asText();
												}
												((ObjectNode) dimNode).put(dim1, val);
											}
										}
										((ObjectNode) (dataNode)).set(currentColumn, dimNode);

										dimNode = mapper.createObjectNode();
										for (String dim1 : dimList) {
											if (dim1.equals(dim) && !uploadMultipleFile) {
												((ObjectNode) dimNode).put(dim1, "");
											} else {
												// CHANGE HERE
												if (!JsonUtils.isNullOrBlankOrNullNode(
														dataMap.get(nextColumn + "." + dim1))) {
													((ObjectNode) dimNode).put(dim1,
															dataMap.get(nextColumn + "." + dim1).asText());
												}
											}
										}
										((ObjectNode) (dataNode)).set(nextColumn, dimNode);
										obj = prepareOutputJson(validationNode, node, result,
												ValidationTypes.sequentialCheck, nextColumn, currentColumn, null, dim);
										objList.add(obj);
									}
								}
							}
						}
					}
				} else {
					if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(currentColumn))) {
						currentVal = dataMap.get(currentColumn).asText(); // HANDLE
																			// THIS
					} else {
						currentVal = null;
					}
					if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(nextColumn))) {
						nextVal = dataMap.get(nextColumn).asText(); // HANDLE
																	// THIS
					}

					if (currentVal != null && !currentVal.isEmpty() && nextVal != null && !nextVal.isEmpty()) {
						if (((INT_PATTERN.matcher(currentVal).matches())
								|| (DOUBLE_PATTERN.matcher(currentVal).matches())
								|| (LONG_PATTERN.matcher(currentVal).matches()))
								&& ((INT_PATTERN.matcher(nextVal).matches())
										|| (DOUBLE_PATTERN.matcher(nextVal).matches())
										|| (LONG_PATTERN.matcher(nextVal).matches()))) {
							if (dataMap.get(currentColumn).asDouble() >= dataMap.get(nextColumn).asDouble()) {
								currentColumn = thisColumnDefault;
								nextColumn = nextColumnDefault;
								if(!uploadMultipleFile){
								((ObjectNode) (dataNode)).put(currentColumn, "");
								((ObjectNode) (dataNode)).put(nextColumn, "");
								}
								obj = prepareOutputJson(validationNode, node, result, ValidationTypes.sequentialCheck,
										nextColumn, currentColumn, null, "");
								objList.add(obj);
							}

						}
					}

				}
			}
		}
		return objList;
	}
	
	
	public static boolean chekElementExist(List<String> superSet, List<String> subset) {
		
		return superSet.containsAll(subset);
	}
	
	/**
	 * PE-7241
	 * @param inputValue
	 * @param key
	 * @param dataType
	 * @return
	 * @throws Exception
	 */
	public ObjectNode checkRequiredField(String inputValue, String key, String dataType) throws Exception{
		ObjectNode obj = mapper.createObjectNode();
		if (StringUtils.isBlank(inputValue)) {
			return obj = prepareOutputJson(null, null, null, ValidationTypes.required, key, inputValue, dataType, null);
		}
		return obj;
	}
	
}
