package in.lti.mosaic.api.base.bso;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.message.producer.MessageProducer;
import in.lti.mosaic.api.base.message.producer.MessageTypes;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;

/**
 * @author rushikesh
 *
 */
public class AsyncValidatorBSO {

  private static final Logger logger = LoggerFactory.getLogger(AsyncValidatorBSO.class);

  /**
   * @param requestData
   */
  @SuppressWarnings("unchecked")
  public static void processRequest(String requestData) {

    logger.debug(LoggerConstants.LOG_MAXIQAPI + ">> performL0");


    Map<String, String> requestMap =
        (Map<String, String>) ObjectSerializationHandler.toObject(requestData, Map.class);

    StatusBSO.insertStatusForRequestId(requestMap.get(Constants.Request.RequestId),
        requestMap.get(Constants.Request.ValidationType), Constants.RequestStatus.RequestReceived,
        null, requestMap.get(Constants.Request.EnvironmentName),
        requestMap.get(Constants.Request.DocumentId), null);

    Integer priorityOfRequest = 1;
    try {
      priorityOfRequest = Integer.valueOf(requestMap.get(Constants.Request.Priority));

    } catch (Exception e) {
      logger.error("Error while parsing priority {}", e);
      priorityOfRequest = 1;
    }

    try {
      MessageProducer.rabbitMessageSendWithPriority(MessageTypes.PERFORM_VALIDATION.toString(),
          requestData, (priorityOfRequest != null) ? priorityOfRequest : 1);

      StatusBSO.insertStatusForRequestId(requestMap.get(Constants.Request.RequestId),
          requestMap.get(Constants.Request.ValidationType), Constants.RequestStatus.AddedToQueue,
          null, requestMap.get(Constants.Request.EnvironmentName),
          requestMap.get(Constants.Request.DocumentId), null);

    } catch (Exception e) {
      logger.error("Error while sending msg {}", e);

      StatusBSO.insertStatusForRequestId(requestMap.get(Constants.Request.RequestId),
          requestMap.get(Constants.Request.ValidationType), Constants.RequestStatus.ERROR, null,
          requestMap.get(Constants.Request.EnvironmentName),
          requestMap.get(Constants.Request.DocumentId), e.getMessage());
    }

    logger.debug(LoggerConstants.LOG_MAXIQAPI + "<< performL0");
  }

}
