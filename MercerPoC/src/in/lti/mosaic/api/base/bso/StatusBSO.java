package in.lti.mosaic.api.base.bso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.beans.Status;
import in.lti.mosaic.api.base.dao.StatusDAO;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.mysql.MysqlOperations;
import in.lti.mosaic.api.base.mysql.MysqlUtility;

/**
 * @author rushi
 *
 */
public class StatusBSO {

  private static final Logger logger = LoggerFactory.getLogger(StatusBSO.class);

  /**
   * @param requestId
   * @param environmentName
   * @throws SystemException
   */
  public static String getDocumentIdFromRequestIdAndEnvironmentName(String requestId,
      String environmentName) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQAPI + ">> getDocumentIdFromRequestIdAndEnvironmentName {} {}",
        requestId, environmentName);

    String documentId = null;
    Status status = StatusDAO.fetchStatusFromDbBasedOnRequestIdAndEnvironmentName(requestId,
        environmentName);
    if (null != status) {
      documentId = status.getDocument_id();
    }

    logger.debug(LoggerConstants.LOG_MAXIQAPI + "<< getDocumentIdFromRequestIdAndEnvironmentName");

    return documentId;
  }

  /**
   * 
   * @param requestId
   * @param environmentName
   * @return
   * @throws SystemException
   */
  public static Status getStatusFromRequestIdAndEnvironmentName(String requestId,
      String environmentName) throws SystemException {

    logger.debug(LoggerConstants.LOG_MAXIQAPI + ">> getStatusFromRequestIdAndEnvironmentName {} {}",
        requestId, environmentName);

    Status status =
        StatusDAO.fetchStatusFromDbBasedOnRequestIdAndEnvironmentName(requestId, environmentName);

    logger.debug(LoggerConstants.LOG_MAXIQAPI + "<< getStatusFromRequestIdAndEnvironmentName");

    return status;
  }

  /**
   * 
   * @param requestId
   * @param requestType
   * @param status
   * @param processing_status
   * @param environmentName
   * @param documentId
   * @param errorMsg
   */
  public static void insertStatusForRequestId(String requestId, String requestType, String status,
      Integer processing_status, String environmentName, String documentId, String errorMsg) {

    logger.debug(LoggerConstants.LOG_MAXIQAPI + ">> insetStatusFroRequestId {} {} {} {} {} {} {}",
        requestId, requestType, status, processing_status, environmentName, documentId, errorMsg);

    Status statusObject = new Status();

    statusObject.setRequest_id(requestId);
    statusObject.setValidation_type(requestType);
    statusObject.setDocument_id(documentId);
    statusObject.setStatus(status);
    statusObject.setProcessing_status(processing_status);
    statusObject.setEnvironment_name(environmentName);
    statusObject.setCurrent_time_stamp(System.currentTimeMillis());
    statusObject.setRemarks(errorMsg);

    try {
      MysqlOperations.insert(statusObject, MysqlUtility.getIdAnnotationForClass(Status.class));
    } catch (Exception e) {
      e.printStackTrace();
      logger.error("Error while inserting status {}", e);
    }

    logger.debug(LoggerConstants.LOG_MAXIQAPI + ">> insetStatusFroRequestId ");

  }

}
