package in.lti.mosaic.api.base.configs;

/**
 * This is a constants definition class.
 * 
 * @author Gireesh Puthumana
 * @version 1.0
 * @since 2017-12-26
 *
 */
public final class CacheConstants {

  /**
   * Environment variable name which will give the base path for property files
   */
  public static final String BASE_PATH = "API_HOME";
  public static final String RABBITMQ_ADRESSES = "RABBITMQ_ADRESSES";
  public static final String RABBITMQ_USER_NAME = "RABBITMQ_USER_NAME";
  public static final String RABBITMQ_PASSWORD = "RABBITMQ_PASSWORD";
  public static final String EXCHANGE_NAME = "EXCHANGE_NAME";
  public static final String MAX_CAPACITY_PER_WORKER = "MAX_CAPACITY_PER_WORKER";

  public static final String JDBC_DRIVER = "JDBC_DRIVER";
  public static final String DB_ADDRESS = "DB_ADDRESS";
  public static final String DB_NAME = "DB_NAME";
  public static final String USER_NAME = "USER_NAME";
  public static final String PASSWORD = "PASSWORD";
  public static final String BUCKET_ID = "BUCKET_ID";
  public static final String API_KEY = "API_KEY";
  public static final String DOCUMENT_STORE_URL = "DOCUMENT_STORE_URL";
  public static final String MAX_RETRY_CALL_BACK = "MAX_RETRY_CALL_BACK";
  public static final String WAIT_TIME_BETN_CALL_BACK = "WAIT_TIME_BETN_CALL_BACK";
  public static final String MAX_CAPACITY_OF_MONGO_THREAD = "MAX_CAPACITY_OF_MONGO_THREAD";
  public static final String EVENT_API_URL = "EVENT_API_URL";
  public static final String EVENT_TYPE = "EVENT_TYPE";
public static final String EVENT_API_KEY = "EVENT_API_KEY";
}
