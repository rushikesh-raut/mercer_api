package in.lti.mosaic.api.base.constants;

/**
 * 
 * @author rushi
 *
 */
public class Constants {

  public static class ValidationTypes {
    public static final String L1 = "L1";
    //public static final String L2 = "L2";
    public static final String L3A = "L3a";
    public static final String L3B = "L3b";
  }
  
  public static class RequestStatus{
    public static final String RequestReceived = "1-Request-received";
    public static final String AddedToQueue = "2-Added-to-queue";
    public static final String PickedUpFroProcessing = "3-Picked-up-for-processing";
    public static final String InputDocumentDownloaded = "4-Input-document-downloaded";
    public static final String ProcessingFinished = "5-Processing-finished";
    public static final String OutputDocumentCreated = "6-Output-document-created";
    public static final String DeliveredSuccessFully = "7-Delivered-successfully";
    public static final String DeadLetter = "7-Dead-letter";
    
    public static final String ERROR = "ERROR";
  }
  
  public static class Status {
    public static final String Id = "id";
    public static final String RequestId = "request_id";
    public static final String DocumentId = "document_id";
    public static final String Status = "status";
    public static final String EnvironmentName = "environment_name";
    public static final String Remarks = "remarks";
  }
  
  public static class Request {
    public static final String RequestId = "requestId";
    public static final String Priority = "priority";
    public static final String ValidationType = "validationType";
    public static final String DocumentId = "documentId";
    public static final String EnvironmentName = "environmentName";
    public static final String Status = "status";
  }
  
  public static class ResponseStatus {
    public static final Integer HTTPSTATUS_200 = 200;
    public static final Integer HTTPSTATUS_400 = 400;
    public static final Integer HTTPSTATUS_500 = 500;
  }

}
