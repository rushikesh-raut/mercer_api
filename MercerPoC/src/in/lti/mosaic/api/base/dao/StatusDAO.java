package in.lti.mosaic.api.base.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.mysql.MysqlOperations;
import in.lti.mosaic.api.base.beans.Status;

/**
 * @author rushi
 *
 */
public class StatusDAO {

  /**
   * @param requestId
   * @param environmentName
   * @return
   * @throws SystemException 
   */
  public static Status fetchDocumentIdFromDbBasedOnRequestIdAndEnvironmentName(String requestId,
      String environmentName) throws SystemException {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put(Constants.Status.RequestId, requestId);
    params.put(Constants.Status.Status, Constants.RequestStatus.DeadLetter);
    params.put(Constants.Status.EnvironmentName, environmentName);
    
    Status status = MysqlOperations.scanOneForQuery(Status.class, params);
    
    return status;
  }
  
  /**
   * @param requestId
   * @param environmentName
   * @return
   * @throws SystemException 
   */
  public static Status fetchStatusFromDbBasedOnRequestIdAndEnvironmentName(String requestId,
      String environmentName) throws SystemException {
    
    Status statusToReturn = new Status();
    
    Map<String, Object> params = new HashMap<String, Object>();
    params.put(Constants.Status.RequestId, requestId);
    params.put(Constants.Status.EnvironmentName, environmentName);
    
    Long maxId = 0l;

    List<Status> listOfStatus = MysqlOperations.scanForQuery(Status.class, params);
    
    
    
    if (null != listOfStatus) {
      for(Status status : listOfStatus) {
        if(status.getId() > maxId) {
          maxId = status.getId();
          statusToReturn = status;
        }
      }
    } 
    
    return statusToReturn;
  }

}
