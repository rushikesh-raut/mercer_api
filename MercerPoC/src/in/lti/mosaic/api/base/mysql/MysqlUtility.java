package in.lti.mosaic.api.base.mysql;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.exceptions.ExceptionsMessanger;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.loggers.ParamUtils;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;

/**
 * @author rushi
 *
 */
public class MysqlUtility {

  private static final Logger logger = LoggerFactory.getLogger(MysqlUtility.class);

  /**
   * Read the table name annotation and return the valid table name for the object
   * 
   * @param object
   * @return
   * @throws SystemException
   */
  public static String getTableNameForObject(Object object) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : >> getTableNameForObject()"
        + ParamUtils.getString(object));
    Class<?> entityClass = object.getClass();

    String tableNameForClass = getTableNameForClass(entityClass);

    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : << getTableNameForObject()"
        + ParamUtils.getString(tableNameForClass));
    return tableNameForClass;
  }

  /**
   * Return table name for class
   * 
   * @param entityClass
   * @return
   * @throws SystemException
   */
  public static String getTableNameForClass(Class<?> entityClass) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : >> getTableNameForClass()"
        + ParamUtils.getString(entityClass));

    TableName tableName = entityClass.getAnnotation(TableName.class);

    if (null != tableName) {
      logger.debug(LoggerConstants.LOG_MAXIQAPI + " : << getTableNameForClass()"
          + ParamUtils.getString(entityClass));
      return tableName.tableName();
    }
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : << getTableNameForClass()"
        + ParamUtils.getString(entityClass));

    ExceptionsMessanger.throwException(new SystemException(), "ERR_001", null);
    return null;
  }

  /**
   * @param object
   * @param tableName
   * @return
   */
  public static String generateInsertQuery(Object object, String tableName) {
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : >> generateInsertQuery()"
        + ParamUtils.getString(object, tableName));

    Map<String, Object> map = null;

    map = getMapForBean(object);

    Set<String> set = map.keySet();

    String columns = "";
    String values = "";
    int i = 0;
    for (String key : set) {

      Object obj = map.get(key);
      String value = null;
      if (obj instanceof String) {
        value = obj + "";
      } else if (obj instanceof Enum) {
        value = String.valueOf(obj);
      } else {
        value = ObjectSerializationHandler.toString(map.get(key));
      }
      boolean skipBraces = false;
      if (obj instanceof Boolean) {
        skipBraces = true;
      } else {
        skipBraces = false;
      }
      value = StringUtils.replace(value, "'", "''");

      if (i == 0) {
        columns += key;
        if (skipBraces)
          values += "?";
        else
          values += "?";
      } else {
        columns += "," + key;
        if (skipBraces)
          values += ",?"/* + value */;
        else
          values += ",?" /* + value + "'" */;
      }
      i++;
    }

    String insertStatement =
        "insert into " + tableName + " (" + columns + ") values (" + values + ")";

    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : << generateInsertQuery()"
        + ParamUtils.getString(insertStatement));

    return insertStatement;
  }

  /**
   * @param object
   * @return
   */
  public static Map<String, Object> getMapForBean(Object object) {

    logger.debug(
        LoggerConstants.LOG_MAXIQAPI + " : >> : getMapForBean()" + ParamUtils.getString(object));

    Map<String, Object> hashmap = new HashMap<>();
    ObjectMapper mapper = new ObjectMapper();
    try {
      hashmap = mapper.readValue(ObjectSerializationHandler.toString(object),
          new TypeReference<Map<String, Object>>() {});
    } catch (JsonGenerationException e) {
      logger.error(LoggerConstants.LOG_MAXIQAPI + e);
    } catch (JsonMappingException e) {
      logger.error(LoggerConstants.LOG_MAXIQAPI + e);
    } catch (IOException e) {
      logger.error(LoggerConstants.LOG_MAXIQAPI + e);
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQAPI + " : << : getMapForBean()" + ParamUtils.getString(hashmap));

    return hashmap;

  }

  /**
   * @param stmt
   * @param object
   * @return
   */
  public static PreparedStatement generateInsertQueryData(PreparedStatement statement,
      Object object) throws SystemException, SQLException {
    int counter = 1;

    Map<String, Object> map = null;

    map = getMapForBean(object);

    Set<String> set = map.keySet();

    for (String key : set) {

      Object obj = map.get(key);
      statement.setObject(counter, (obj instanceof String) ? obj + ""
          : (obj instanceof Enum) ? String.valueOf(obj) : ObjectSerializationHandler.toString(obj));
      counter++;
    }

    return statement;

  }

  /**
   * @param entityClass
   * @param query
   * @return
   */
  public static String generateQuery(Class<?> entityClass, Map<String, Object> query)
      throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : >>  generateQuery()"
        + ParamUtils.getString(entityClass, query));

    String tableName = getTableNameForClass(entityClass);

    String sql = "select * from " + tableName;
    if (null != query) {
      int counter = 0;
      for (Entry<String, Object> entry : query.entrySet()) {
        if (counter == 0) {
          sql += " where " + entry.getKey() + "=?";
        } else {
          sql += " and " + entry.getKey() + "=?";
        }
        counter++;
      }
    }

    logger
        .debug(LoggerConstants.LOG_MAXIQAPI + " : <<  generateQuery()" + ParamUtils.getString(sql));
    return sql;

  }

  /**
   * @param prepareStatement
   * @param query
   * @return
   */
  public static PreparedStatement generateSqlStatmentQueryData(PreparedStatement statement,
      Map<String, Object> query) throws SystemException, SQLException {

    if (null != query && query.size() > 0) {
      int counter = 1;
      for (Entry<String, Object> entry : query.entrySet()) {
        statement.setObject(counter,
            (entry.getValue() instanceof String) ? entry.getValue() + ""
                : (entry.getValue() instanceof Enum) ? String.valueOf(entry.getValue())
                    : ObjectSerializationHandler.toString(entry.getValue()));
        counter++;
      }
    }
    return statement;
  }

  /**
   * @param entityClass
   * @param resultSet
   * @return
   */
  @SuppressWarnings("unchecked")
  public static <T> T buildObject(Class<T> entityClass, ResultSet result)
      throws SQLException, ClassNotFoundException, IOException {
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : >> buildObject()"
        + ParamUtils.getString(entityClass, result));

    Map<String, Object> map = new HashMap<String, Object>();

    Field[] declaredFields = entityClass.getDeclaredFields();

    for (Field field : declaredFields) {

      String value = result.getString(field.getName());

      if (field.getType().equals(String.class)) {
        map.put(field.getName(), value);
      } else {
        Object val = deserialize(value, field.getType());

        map.put(field.getName(), val);
      }

    }

    Object bean = getObjectFromMap(map, entityClass);

    logger.debug(
        LoggerConstants.LOG_MAXIQAPI + " : << buildObject()" + ParamUtils.getString((T) bean));

    return (T) bean;

  }

  /**
   * Get Bean for Object Map
   * 
   * @param map
   * @param entityClass
   * @return
   * @throws ClassNotFoundException
   * @throws IOException
   */
  @SuppressWarnings("unchecked")
  private static <T> T getObjectFromMap(Map<String, Object> map, Class<T> entityClass)
          throws ClassNotFoundException, IOException {
      logger.debug(
              LoggerConstants.LOG_MAXIQAPI + " : >> getObjectFromMap()" + ParamUtils.getString(map, entityClass));

      String json = ObjectSerializationHandler.toString(map);

      return (T) ObjectSerializationHandler.toObject(json, entityClass);
  }

  /**
   * Builds the object of the Class type given with the Byte array
   * 
   * @param bytes
   * @param classType
   * @return
   * 
   */
  public static Object deserialize(String bytes, Class<?> classType) {
      logger.debug(LoggerConstants.LOG_MAXIQAPI + " : >> deserialize()" + ParamUtils.getString(bytes, classType));

      if (null != bytes && !StringUtils.equals("null", bytes)) {
          Object object = ObjectSerializationHandler.toObject(bytes, classType);
          logger.debug(LoggerConstants.LOG_MAXIQAPI + " : << deserialize() : [return : {}]", object);

          return object;
      }
      logger.debug(LoggerConstants.LOG_MAXIQAPI + " : << deserialize()" + ParamUtils.getString(null));
      return null;
  }
  
  /**
   * Get Id annotations for the class
   * 
   * @param cls
   * @return
   * @throws SystemException
   */
  public static String getIdAnnotationForClass(Class<?> cls) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : >> getIdAnnotationForClass()"
        + ParamUtils.getString(cls));

    for (Field field : cls.getDeclaredFields()) {

      String name = field.getName();
      Annotation[] annotations = field.getDeclaredAnnotations();

      if (null != annotations) {
        for (Annotation annotation : annotations) {
          if (annotation instanceof Id) {
            logger.debug(LoggerConstants.LOG_MAXIQAPI + " : << getIdAnnotationForClass()"
                + ParamUtils.getString(name));
            return name;
          }
        }
      }
    }

    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : << getIdAnnotationForClass()"
        + ParamUtils.getString(null));

    ExceptionsMessanger.throwException(new SystemException(), "ERR_004", null);
    return null;
  }
  
  public static String generateInsertQueryForAutoIncrementedPrimaryKey(
      String primaryKeyAutoIncrement, String tableName, Map<String, Object> map) {

    logger.debug(
        LoggerConstants.LOG_MAXIQAPI + " : << generateInsertQueryForAutoIncrementedPrimaryKey()"
            + ParamUtils.getString(primaryKeyAutoIncrement, tableName, map));

    Set<String> set = map.keySet();

    String columns = "";
    String values = "";
    int i = 0;
    for (String key : set) {

      if (!StringUtils.equals(key, primaryKeyAutoIncrement)) {
        
        if(null == map.get(key)) {
          continue;
        }

        String value = "?";

        if (i == 0) {
          columns += key;
          values += value;
        } else {
          columns += "," + key;
          values += "," + value;
        }
        i++;
      }
    }
    String insertStatement =
        "insert into " + tableName + " (" + columns + ") values (" + values + ")";

    logger.debug(
        LoggerConstants.LOG_MAXIQAPI + " : << generateInsertQueryForAutoIncrementedPrimaryKey()"
            + ParamUtils.getString(insertStatement));

    return insertStatement;
  }
  
  public static PreparedStatement generateInsertQueryDataWithPK(String primaryKeyAutoIncrement,
      PreparedStatement statement, Object object) throws SystemException, SQLException {
    int counter = 1;

    Map<String, Object> map = null;

    map = getMapForBean(object);

    Set<String> set = map.keySet();

    for (String key : set) {
      if (!StringUtils.equals(key, primaryKeyAutoIncrement)) {
        Object obj = map.get(key);
        if(null == obj) {
          continue;
        }
        statement.setObject(counter,
            (obj instanceof String) ? obj + ""
                : (obj instanceof Enum) ? String.valueOf(obj)
                    : ObjectSerializationHandler.toString(obj));
        counter++;
      }
    }
    return statement;

  }

}
