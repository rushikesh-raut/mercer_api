package in.lnt.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lti.mosaic.derby.MercerDBCP;
import in.lnt.utility.constants.LoggerConstants;
import in.lnt.utility.general.JsonUtils;
import in.lnt.validations.FormValidator;
import in.lti.mosaic.api.base.beans.Status;
import in.lti.mosaic.api.base.bso.AsyncValidatorBSO;
import in.lti.mosaic.api.base.bso.StatusBSO;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;

/**
 * @author rushikesh
 *
 */
@Controller
public class AsyncController {
  
  private static final Logger logger = LoggerFactory.getLogger(AsyncController.class);
  
  @RequestMapping(value = "/healthCheck", method = RequestMethod.GET)
  public @ResponseBody String healthCheck(HttpServletRequest request,
      HttpServletResponse response) {
    return "OK";
  }
  
  //This is a temp endpoint for dbcp pool
  // can be removed after successfull testing of dbcp
  @RequestMapping(value = "/getDbcp", method = RequestMethod.GET)
  public @ResponseBody String getDbcpStatus(
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    Map<String , String> map = MercerDBCP.getDataSource().printStatus();
    
    return ObjectSerializationHandler.toString(map);
  }
  
  //This is a temp endpoint
  //Should be remoced after testing of derby is completed
  @RequestMapping(value = "/getDerbyCount", method = RequestMethod.GET)
  public @ResponseBody String getDerbyCount(@RequestParam String tableName,HttpServletRequest request,
      HttpServletResponse response) throws SQLException, IOException {
    
    Connection connection = MercerDBCP.getDataSource().getConnection();
    
    PreparedStatement countPreparedStatement = connection.prepareStatement(" select count(*) as rowcount from " + tableName);
    ResultSet rs = countPreparedStatement.executeQuery();
    rs.next();
    int count = rs.getInt("rowcount");
    rs.close();
    connection.close();
    
    return count+"";
  }
  
  //This is a temp endpoint
  //Should be remoced after testing of derby is completed
  @RequestMapping(value = "/runDerby", method = RequestMethod.GET)
  public @ResponseBody String runDerbyQuery(@RequestParam String query,HttpServletRequest request,
      HttpServletResponse response) throws SQLException, IOException {
    
    Connection connection = MercerDBCP.getDataSource().getConnection();
    
    PreparedStatement countPreparedStatement = connection.prepareStatement(query);
    ResultSet rs = countPreparedStatement.executeQuery();
    
    rs.close();
    connection.close();
    
    return "Query is fine";
    
  }
  
  @RequestMapping(value = "/performL0", method = RequestMethod.POST)
  public @ResponseBody void performL0Validations(HttpServletRequest request, HttpServletResponse response) {

    logger.debug(LoggerConstants.LOG_MAXIQAPI + ">> performL0");
    
    FileUploadController fileUploadController = new FileUploadController();

    Map<String, Object> _L0_output = new HashMap<String, Object>();

    try {
      Map<String, Object> parsedData = fileUploadController.parseCsvRequest(request, response);

      List<InputStream> inputStreams = (List<InputStream>) parsedData.get("inputStreams");
      String entitiesJson = (String) parsedData.get("entitiesJson");
      FileItem fileJSON = (FileItem) parsedData.get("fileJSON");

      _L0_output = fileUploadController.performL0ValidationForFileRequest(request, response,
          parsedData, inputStreams, entitiesJson, fileJSON);
      
      JSONObject validationDataObject = new JSONObject(fileJSON.getString());

      String entities = validationDataObject.get("entities").toString();

      HashMap<String, Object> compareFieldsMap =
          (HashMap<String, Object>) _L0_output.get("compareFieldsMap");
      
      FormValidator formvalidator = new FormValidator();

      JsonNode entitiesNodeResponse =
          formvalidator.preparingL1Input(entities, (String) _L0_output.get("csvJsonString"), false,
              (LinkedHashMap<String, String>) _L0_output.get("xslJsonMap"),
              (String) compareFieldsMap.get("sheetMatchingFlag"), true);

      FileUploadController.downLoadData(response, entitiesNodeResponse.toString().getBytes(),
          Constants.ResponseStatus.HTTPSTATUS_200);
      

    } catch (Exception e) {
      
      logger.error(" General exception caught {}",e);

      ObjectNode errNode = fileUploadController.mapper.createObjectNode();

      errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);

      FileUploadController.downLoadData(response, errNode.toString().getBytes(),
          Constants.ResponseStatus.HTTPSTATUS_500);
      
    }
    
    logger.debug(LoggerConstants.LOG_MAXIQAPI + "<< performL0");

  }
  
  @RequestMapping(value = "/validateAsync", method = RequestMethod.POST, consumes = "application/json")
  public void validateAsync(@RequestBody String validationData,
      HttpServletRequest request, HttpServletResponse response) {
    
    logger.debug(LoggerConstants.LOG_MAXIQAPI + ">> validateAsync");
    
    AsyncValidatorBSO.processRequest(validationData);
    
    logger.debug(LoggerConstants.LOG_MAXIQAPI + "<< validateAsync");
  }
  
  
  @RequestMapping(value = "/statusCheck", method = RequestMethod.POST, consumes = "application/json")
  public @ResponseBody String getStatus(@RequestBody String validationData,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQAPI + ">> getStatusOfRequest");
 
    Map<String, String> requestMap =
        (Map<String, String>) ObjectSerializationHandler.toObject(validationData, Map.class);
 
 
    try {
      Status status = StatusBSO.getStatusFromRequestIdAndEnvironmentName(
          requestMap.get(Constants.Request.RequestId),
          requestMap.get(Constants.Request.EnvironmentName));
      if(null != status) {
        return ObjectSerializationHandler.toString(status);
      }
    } catch (SystemException e) {
      e.printStackTrace();
      logger.error(LoggerConstants.LOG_MAXIQAPI + "<< getStatusOfRequest {}", e);
      throw new Exception(e);
    }
    throw new Exception("Request Id not found");
 
  }

}
