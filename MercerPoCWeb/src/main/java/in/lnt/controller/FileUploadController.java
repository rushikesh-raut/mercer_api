package in.lnt.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.parser.CSVParser;
import in.lnt.pojo.FileStorageInfo;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.JsonUtils;
import in.lnt.utility.poi.POIUtility;
import in.lnt.validations.FormValidator;

/**
 * Handles requests for the application file upload requests
 */
@Controller
public class FileUploadController {

	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

	// upload settings
	private static final int MEMORY_THRESHOLD = 1024 * 1024 * 3; // 3MB
	private static final int MAX_FILE_SIZE = 1024 * 1024 * 2000; // 2GB
	private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 2500; // 2.5GB
	ArrayList<String> paramValueList=null;
	public static ObjectMapper mapper = new ObjectMapper();

	/**
	 * For Input as Files : MetaData as JSON and data as CSV or XLSX Only.
	 * 
	 * @param request JSON and CSV or XLSX File
	 * @param response JSON
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST, produces = "application/json")
	public void uploadMultipleFileHandler(HttpServletRequest request, HttpServletResponse response) {
	
		logger.info(" Entering into uploadMultipleFileHandler method for json,xlsx and csv");
		long startTime = System.currentTimeMillis();
		
		ObjectNode errNode = null;
		FileItem fileJSON = null;
		List<InputStream> inputStreams = new ArrayList<InputStream>();
		String entitiesJson = "";
		HashMap<String, Object> compareFieldsMap = null;
		JSONObject validationDataObject = null;
		Map<String, Object> _L0_output = new HashMap<String, Object>();

		try {
			errNode = mapper.createObjectNode();
			Map<String, Object> parsedData = parseCsvRequest(request, response);
			inputStreams = (List<InputStream>) parsedData.get("inputStreams");
			entitiesJson = (String) parsedData.get("entitiesJson");
			fileJSON = (FileItem) parsedData.get("fileJSON");
			_L0_output = performL0ValidationForFileRequest(request, response, parsedData, inputStreams, entitiesJson,
					fileJSON);
			if(_L0_output!=null)
			{
			compareFieldsMap = (HashMap<String, Object>) _L0_output.get("compareFieldsMap");
			if (compareFieldsMap != null && compareFieldsMap.get("resultNode") != null) {
				if (compareFieldsMap.get("resultNode").equals(ErrorCacheConstant.ERR_055))
					throw new CustomStatusException(Constants.HTTPSTATUS_400,
							Cache.getPropertyFromError(ErrorCacheConstant.ERR_055));
				else {
					try {

						downLoadData(response, compareFieldsMap.get("resultNode").toString().getBytes(),
								Constants.HTTPSTATUS_422);
						return;
						// logger.debug("response sent successfully..");
					} catch (Exception e) {
						logger.error(
								"Exception occured in uploadMultipleFileHandler for downLoadData" + e.getMessage());
						// Exception need to be propagate on API where empty
						// JSON response will be written instead of blank.
						errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
						downLoadData(response, errNode.toString().getBytes(), Constants.HTTPSTATUS_500);
					}
				}
			} else {
				validationDataObject = new JSONObject(Streams.asString(fileJSON.getInputStream(), "UTF-8"));
				entitiesJson = validationDataObject.get("entities").toString();
				FormValidator formValidator = new FormValidator();

				// Validation framework will be called here.
				validateForm(entitiesJson, request, response, (String) _L0_output.get("csvJsonString"), false,
						(LinkedHashMap<String, String>) _L0_output.get("xslJsonMap"),
						(String) compareFieldsMap.get("sheetMatchingFlag"), true);
			}
			}

		}  catch (CustomStatusException cse) {

			errNode = JsonUtils.updateErrorNode(cse, errNode);
			downLoadData(response, errNode.toString().getBytes(), cse.getHttpStatus());
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured in uploadMultipleFileHandler..", e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			downLoadData(response, errNode.toString().getBytes(), Constants.HTTPSTATUS_500);
		}
		finally{
		for (InputStream inputStream : inputStreams) {
			try {
				inputStream.close();
			} catch (Exception e) {
				logger.error("Exception occured in uploadMultipleFileHandler for closing inputStream.."+e.getMessage());
				errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
				downLoadData(response, errNode.toString().getBytes(), Constants.HTTPSTATUS_500);
			}
		  }
		logger.info(" Existing from   uploadMultipleFileHandler method for json,xlsx and csv and time taken to execute this method is"
				+ (System.currentTimeMillis() - startTime));
		}
		
	}
	
	/**
	 * Upload multiple file using Spring Controller
	 ** 
	 * @param validationData
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException,JSONException
	 *             
	 */

	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody void uploadJSONRequestHandler(@RequestBody String validationData, HttpServletRequest request,
			HttpServletResponse response) {

		logger.info(" Entering into uploadJSONRequestHandler ");

		long startTime = System.currentTimeMillis();

		ObjectNode errNode = null;
		try {
			errNode = mapper.createObjectNode();
			String entitiesJson = parseJsonRequest(validationData);
			HashMap<String, String> empIdMap = new FormValidator().checkUniqueNessForEEID(entitiesJson);
			// logger.info(String.format("isUniqueEeid : %s", isUniqueEeid));
			if (empIdMap.get("isUnique").equals("true"))
				validateForm(entitiesJson, request, response, null, false, null, null, false);
			else if (empIdMap.get("isUnique").equals(Constants.DUPLICATE))
				throw new CustomStatusException(Constants.HTTPSTATUS_400,
						Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
			else if (empIdMap.get("isUnique").equals(Constants.MISSING))
				throw new CustomStatusException(Constants.HTTPSTATUS_400,
						Cache.getPropertyFromError(ErrorCacheConstant.ERR_039));
		} catch (CustomStatusException cse) {

			errNode = JsonUtils.updateErrorNode(cse, errNode);
			downLoadData(response, errNode.toString().getBytes(), cse.getHttpStatus());
		} catch (Exception e) {
			logger.error("Exception occured in uploadMultipleFileHandler..", e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			downLoadData(response, errNode.toString().getBytes(), Constants.HTTPSTATUS_500);
		} finally {
			logger.info(" Exiting from  uploadJSONRequestHandler and time taken to execute this method is "
					+ (System.currentTimeMillis() - startTime));
		}

	}

	public @ResponseBody void validateForm(String entitiesJson, HttpServletRequest request,
			HttpServletResponse response, String csvJsonString, boolean isAggregateRequest,
			HashMap<String, String> xslJsonMap, String firstSheetName, boolean isMultiPartRequest )throws CustomStatusException,Exception {
		
		logger.info(" Entering into validateForm method " );
		
		long startTime = System.currentTimeMillis();
		JsonNode data = null;
		ObjectNode errNode = null;
		try {
			errNode = mapper.createObjectNode();
			FormValidator formValidator = new FormValidator();
			data = formValidator.parseAndValidate(entitiesJson, csvJsonString, isAggregateRequest, xslJsonMap,
					firstSheetName, isMultiPartRequest); 
			if (data != null) {
				downLoadData(response, data.toString().getBytes("UTF-8"), 200);
			}
		} finally {	
			logger.info(" Exiting from validateForm method and time taken to execute this method is "+(System.currentTimeMillis()-startTime) );
		}	
	}

	public static void downLoadData(HttpServletResponse response, byte[] bs, int status) {
		InputStream is;
		ObjectNode errNode = null;
		logger.info(" Entering into downLoadData method " );
		long startTime = System.currentTimeMillis();
		try {
			
			errNode = mapper.createObjectNode();
			is = new ByteArrayInputStream(bs);
			response.setStatus(status);
			// MIME type of the file
			response.setContentType("application/json");
			// Response header
			response.setHeader("Content-Disposition", "attachment; filename=\"output.json\"");

			// Read from the file and write into the response
			OutputStream os = response.getOutputStream();
			byte[] buffer = new byte[1024];
			int len;
			while ((len = is.read(buffer)) != -1) {
				os.write(buffer, 0, len);
			}
			os.flush();
			os.close();
			is.close();
			
		} catch (Exception e) {
			logger.error("Exception occured in downloadData..,"+e.getMessage());
		// Code has been removed because in case of exception going into recursive which results to stackoverflow..	
		}finally {
			logger.info(" Existing  from downLoadData method and time taken to execute this method is " + (System.currentTimeMillis() - startTime));
		}
	}

	/**
	 * 
	 * @param jsonLikeObject
	 * @return message.
	 */
	public static String isJSONValid(String jsonLikeObject) {

		String returnValue = "true";
		try {
			new JSONObject(jsonLikeObject);
		} catch (JSONException ex) {
			try {
				new JSONArray(jsonLikeObject);
			} catch (JSONException ex1) {
				returnValue = ex.getMessage();
			}
		}
		return returnValue;
	}

	public Map<String, Object> parseCsvRequest(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info(" Entering into parseCsvRequest method " );
		long startTime = System.currentTimeMillis();
		String entitiesJson = "";
		Map<String, Object> parsedData = new HashMap<String, Object>();
		List<InputStream> inputStreams = new ArrayList<InputStream>();
		JSONObject errorResponse = new JSONObject();
		Map<String, Object> workBookMap = null;
		
		try {
			
		if (!ServletFileUpload.isMultipartContent(request)) {
			// if not, we stop here
			PrintWriter writer;
				writer = response.getWriter();
				errorResponse.put("RESPONSE", "Error: Form must has enctype=multipart/form-data.");
				writer.println(errorResponse);
				writer.flush();
			return null;
		}

		// configures upload settings
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// sets memory threshold - beyond which files are stored in disk
		factory.setSizeThreshold(MEMORY_THRESHOLD);
		// sets temporary location to store files
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		// sets maximum size of upload file
		upload.setFileSizeMax(MAX_FILE_SIZE);
		// sets maximum size of request (include file + form data)
		upload.setSizeMax(MAX_REQUEST_SIZE);
		FileItem fileJSON = null;
		FileItem fileDATA = null;
		String extn = "";
			// parses the request's content to extract file data
			List<FileItem> formItems = upload.parseRequest(request);
			if (formItems != null && formItems.size() > 0) {
				// byte[] bytes = file.getBytes();
				for (FileItem item : formItems) {

					// extn = item.getName().split("\\.")[1];
					extn = item.getName().substring(item.getName().lastIndexOf(".") + 1);
					if (extn != null && !extn.equals("") && (extn.toLowerCase().equals("csv")
							|| extn.toLowerCase().equals("json") || extn.toLowerCase().equals("xlsx"))) {
						if (item.getFieldName().equals("MetaData")) {
							fileJSON = item;
						} else if (item.getFieldName().equals("Data")) {
							fileDATA = item;
						}

						// Check Metadata and Data file size

						if (fileDATA != null && fileDATA.getSize() == 0) {
							throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_001));
						} else if (fileJSON != null && fileJSON.getSize() == 0) {
							/*errorResponse.put("RESPONSE", "ERR 4 : JSON FILE SIZE is 0");
							downLoadData(response, errorResponse.toString().getBytes(), 400);*/
							throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_002));
						}

						// Check if file is xlsx file then convert to csv else
						// skip
						if (item.getFieldName().equals("Data") && extn.toLowerCase().equals("xlsx")) {
							// inputStreams.add(POIUtility.convertXlsxToCSV(item));
							inputStreams.add(item.getInputStream());
							workBookMap = POIUtility.prseXslxFile(item);

						} else {
							inputStreams.add(item.getInputStream());
							parsedData.put("inputStreams", item);
						}

					} else {
						throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_003));
					}
				}
			}


		//logger.info(String.format("%d  %d", fileDATA.getSize(),fileJSON.getSize()));
		parsedData.put("inputStreams", inputStreams);
		parsedData.put("fileJSON", fileJSON);
		parsedData.put("fileDATA", fileDATA);
		parsedData.put("entitiesJson", entitiesJson);
		parsedData.put("workBookMap", workBookMap);
		} finally {
			logger.info(" Existing from parseCsvRequest method and time taken to execute this method is " + (System.currentTimeMillis() - startTime));
		}
		
		return parsedData;

	}

	public String parseJsonRequest(String validationData) throws Exception{
		
		logger.info(" Entering into parseJsonRequest method " );
		long startTime = System.currentTimeMillis();
		
		String entitiesJson = "";
		String key = "";
		String entitiesKey = null;
		JSONObject validationDataObject = null;
		Iterator<String> it = null;
		String validatedSyntax = "false";

		validatedSyntax = isJSONValid(validationData); // Syntax checker..
		if (validatedSyntax.equalsIgnoreCase("true")) {
			validationDataObject = new JSONObject(validationData);
			it = validationDataObject.keys();
		} else {
			throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_004));
		}
		try {
			while (it != null && it.hasNext()) {
				key = it.next();
				if (key.equalsIgnoreCase("entities")) {
					entitiesKey = key;
				}
			}

			if (entitiesKey != null && validationDataObject.get(entitiesKey) != null) {

				entitiesJson = validationDataObject.get(entitiesKey).toString();

			} else {
				// errorResponse.put("RESPONSE", "ERR 3: Invalid input JSON");
				throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_004));
			}
		} catch (Exception e) {
			throw e;
		}finally {
			logger.info(" Existing from parseJsonRequest method and time taken to execute this method is " + (System.currentTimeMillis() - startTime));
		}
		return entitiesJson;

	}

	@RequestMapping(value = "/validateAggregates", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody void performAggregation(@RequestBody String validationData, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ObjectNode errNode = null;
		
		logger.info(" Entering into performAggregation method " );
		long startTime = System.currentTimeMillis();
		
		try{
			String entitiesJson = parseJsonRequest(validationData);
			//PE : 2449 to check duplication of EEID start in case of entity data in request body
			HashMap <String,String> empIdMap=new FormValidator().checkUniqueNessForEEID(entitiesJson);
			//logger.info(String.format("isUniqueEeid : %s", isUniqueEeid));
			if(empIdMap.get("isUnique").equals("true"))
			validateForm(entitiesJson,request, response, null, true, null, null, false);
			else if(empIdMap.get("isUnique").equals(Constants.DUPLICATE))
			//throw new CustomStatusException(Constants.HTTPSTATUS_400,Constants.DUPLICATE_EEID);
			throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
			else if(empIdMap.get("isUnique").equals(Constants.MISSING))
			//throw new CustomStatusException(Constants.HTTPSTATUS_400,Constants.MISSING_EEID);	
			throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_039));
			}
		catch(CustomStatusException cse) {

			errNode = mapper.createObjectNode();			
			errNode = JsonUtils.updateErrorNode(cse,errNode);
			downLoadData(response, errNode.toString().getBytes("UTF-8"), cse.getHttpStatus());
		}
		catch(Exception e)
		{
			logger.error("Exception occured in performAggregation :: ", e.getMessage());	
			downLoadData(response, JsonUtils.updateErrorNodeForGenericException(errNode, e).toString().getBytes("UTF-8"), Constants.HTTPSTATUS_500);
		}finally {
			logger.info(" Existing from performAggregation method and time taken to execute this method is " + (System.currentTimeMillis() - startTime));
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/validateAggregates", method = RequestMethod.POST, produces = "application/json")
	public void performAggregation(HttpServletRequest request, HttpServletResponse response) {

		logger.info(" Entering into performAggregation method " );
		long startTime = System.currentTimeMillis();
		
		String entitiesJson = "";
		FileItem fileJSON = null;
		List<InputStream> inputStreams = new ArrayList<InputStream>();
		JSONObject validationDataObject = null;
		HashMap<String, Object> csvMap = null;
		FileStorageInfo fStoreInfo = null;
		String csvJsonString=null;
		ObjectNode errNode = null;
		try {
		Map<String, Object> parsedData = parseCsvRequest(request, response);
		inputStreams = (List<InputStream>) parsedData.get("inputStreams");
		entitiesJson = (String) parsedData.get("entitiesJson");
		fileJSON = (FileItem) parsedData.get("fileJSON");
		String reqLocale;
			if (inputStreams.size() == 2) {

				/**
				 * @author Nikhil Kshirsagar
				 * @since 25-09-2017
				 * Code to write input .json & .csv files to HDFS starts here 
				 */
				UUID uuid = UUID.randomUUID(); // Gets random Id
				
				fStoreInfo = new FileStorageInfo();
				
				if(null == request.getHeader("X-Request-ID")){
					fStoreInfo.setRequestId(uuid.toString());
				}else{
					fStoreInfo.setRequestId(request.getHeader("X-Request-ID"));
				}
				
				if(null == request.getRemoteAddr()){
					fStoreInfo.setRequestIp("");
				}else{
					fStoreInfo.setRequestIp(request.getRemoteAddr());					
				}
				
				reqLocale = request.getLocale().toString();
				fStoreInfo.setRequestLocale(reqLocale);
				
				if(null == request.getHeader("User-Agent")){
					fStoreInfo.setRequestAgent("");
				}else{
					fStoreInfo.setRequestAgent(request.getHeader("User-Agent"));
				}
				
				// TEMPORARY COMMENTING CODE
//				fileNames = Arrays.asList(fileJSON.getName(), ((FileItem) parsedData.get("fileDATA")).getName());
//				WriteToHDFileSystem.writeFilesToHdfs(fStoreInfo, fileNames, inputStreams);	// Improve calling by introducing map
				
				// Code to write HDFS ends here. 
				
				
				csvMap=CSVParser.parseFile(inputStreams.get(1));
				if(csvMap!=null && csvMap.get("isEeidDuplicate")==null)
				{
				HashMap<String, Object> map = new HashMap<String, Object>();
					map.put("data", csvMap.get("allRecords"));
				ObjectMapper mapper = new ObjectMapper();
					csvJsonString = mapper.writeValueAsString(map);
				validationDataObject = new JSONObject(Streams.asString(fileJSON.getInputStream(), "UTF-8"));
				entitiesJson = validationDataObject.get("entities").toString();
				validateForm(entitiesJson, request, response, csvJsonString, true, null, null, true);
				}
				else if(csvMap!=null && csvMap.get("isEeidDuplicate")!=null && csvMap.get("isEeidDuplicate").equals("true"))
				{
					throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
				}
			}
		} 
		catch(CustomStatusException cse) {

			errNode = mapper.createObjectNode();			
			errNode = JsonUtils.updateErrorNode(cse,errNode);
			try {
				downLoadData(response, errNode.toString().getBytes("UTF-8"), cse.getHttpStatus());
			} catch (UnsupportedEncodingException e) {
				logger.error("UnsupportedEncodingException : "+e.getMessage());	
			}
		}catch (Exception e) {
			logger.error("Exception occured in performAggregation"+e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			try {
				downLoadData(response, errNode.toString().getBytes("UTF-8"), Constants.HTTPSTATUS_500);
			} catch (UnsupportedEncodingException uee) {
				logger.error("UnsupportedEncodingException : "+uee.getMessage());	
			}
		}
		finally{
		for (InputStream inputStream : inputStreams) {
			try {
				inputStream.close();
			} catch (IOException e) {
				logger.error(" Exception occured in performAggregation while closing inputstream.."+e.getMessage());
				errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
				try {
					downLoadData(response, errNode.toString().getBytes("UTF-8"), Constants.HTTPSTATUS_500);
				} catch (UnsupportedEncodingException uee) {
					logger.error("UnsupportedEncodingException : "+uee.getMessage());	
				}
			}
		}
		logger.info(" Existing from performAggregation method and time taken to execute this method is " + (System.currentTimeMillis() - startTime));

		}
	}

	public Map<String, Object> performL0ValidationForFileRequest(HttpServletRequest request,
			HttpServletResponse response, Map<String, Object> parsedData, List<InputStream> inputStreams,
			String entitiesJson, FileItem fileJSON) {
		JSONObject validationDataObject = null;
		HashMap<String, Object> xlsxMap = null;
		HashMap<String, Object> csvMap = null;
		LinkedHashMap<String, String> xslJsonMap = null;
		String csvJsonString = null;
		HashMap<String, Object> compareFieldsMap = null;
		FormValidator formValidator = new FormValidator();

		FileStorageInfo fStoreInfo = null;
		String reqLocale;
		List<String> fileNames = null;

		ObjectMapper objMapper = new ObjectMapper();
		ArrayNode entitiesNode = null;

		JsonNode contextDataNode = null;
		StringBuffer coName_CtryCode = new StringBuffer();
		List<String> xlsSheetList = null;
		List<String> entitiesList = null;
		HashMap<String, String> empIdMap = null;
		ObjectNode errNode = null;
		String l0Message = "";
		long stopTime = 0;
		String reqDataInJson = "";
		JSONObject reqDataJsonObject = null;
		//Map<String, Object> _L0_output = new HashMap<String, Object>();
		Map<String, Object> _L0_output = null;
		Boolean eeidColFlag = false;
		List<String> headerList = new ArrayList<>();
		try {
			errNode = mapper.createObjectNode();
			if (inputStreams.size() == 2) {
				/**
				 * @author Nikhil Kshirsagar
				 * @since 25-09-2017 Code to write input .json & .csv files to
				 *        HDFS starts here
				 */
				UUID uuid = UUID.randomUUID(); // Gets random Id

				fStoreInfo = new FileStorageInfo();

				if (null == request.getHeader("X-Request-ID")) {
					fStoreInfo.setRequestId(uuid.toString());
				} else {
					fStoreInfo.setRequestId(request.getHeader("X-Request-ID"));
				}

				if (null == request.getRemoteAddr()) {
					fStoreInfo.setRequestIp("");
				} else {
					fStoreInfo.setRequestIp(request.getRemoteAddr());
				}

				reqLocale = request.getLocale().toString();
				fStoreInfo.setRequestLocale(reqLocale);

				if (null == request.getHeader("User-Agent")) {
					fStoreInfo.setRequestAgent("");
				} else {
					fStoreInfo.setRequestAgent(request.getHeader("User-Agent"));
				}

				FileItem fileItem_fileDATA = (FileItem) parsedData.get("fileDATA");
				fileNames = Arrays.asList(fileJSON.getName(), fileItem_fileDATA.getName());
				if (fileNames.get(1).substring(fileNames.get(1).lastIndexOf(".") + 1).equalsIgnoreCase("json")) {
					reqDataJsonObject = new JSONObject(Streams.asString(fileItem_fileDATA.getInputStream(), "UTF-8"));
					reqDataInJson = reqDataJsonObject.toString();
				}
				// WriteToHDFileSystem.writeFilesToHdfs(fStoreInfo,
				// fileNames,inputStreams); // Improve calling by introducing
				// map

				// Code to write HDFS ends here.
				validationDataObject = new JSONObject(Streams.asString(fileJSON.getInputStream(), "UTF-8"));
				entitiesJson = validationDataObject.get("entities").toString();
				// logger.debug(String.format(Constants.LOG_REQJSON ,
				// entitiesJson));
				// PE : 2449 to check duplication of EEID start in case of
				// entity data in request body
				empIdMap = new FormValidator().checkUniqueNessForEEID(entitiesJson);
				// logger.debug(String.format("isUniqueEeid..%s",isUniqueEeid));
				// if(isUniqueEeid.equalsIgnoreCase(Constants.DUPLICATE))
				if (empIdMap != null && empIdMap.get("isUnique") != null
						&& empIdMap.get("isUnique").equals(Constants.DUPLICATE)) {
					// throw new
					// CustomStatusException(Constants.HTTPSTATUS_400,Constants.DUPLICATE_EEID);
					throw new CustomStatusException(Constants.HTTPSTATUS_400,
							Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
				} // done
				else if (empIdMap != null && empIdMap.get("isUnique") != null
						&& empIdMap.get("isUnique").equals(Constants.MISSING)) {
					// throw new
					// CustomStatusException(Constants.HTTPSTATUS_400,Constants.MISSING_EEID);
					throw new CustomStatusException(Constants.HTTPSTATUS_400,
							Cache.getPropertyFromError(ErrorCacheConstant.ERR_038));
				}

				xlsxMap = formValidator.convertXlsxToMap((Map<String, Object>) parsedData.get("workBookMap"),
						empIdMap.get("empIdColumn"));
				if (!reqDataInJson.isEmpty()) {
					// validateForm(entitiesJson, request, response,
					// reqDataInJson, false, xslJsonMap, null, true);
					//	csvJsonString = reqDataInJson;
					//sheetMatchingFlag
					//PE_8395
					formValidator.performL0Validation(entitiesJson, reqDataInJson, false, null,
							null);
					_L0_output = new HashMap<String, Object>();
					compareFieldsMap =  new HashMap<String, Object>();
					compareFieldsMap.put("sheetMatchingFlag", null);
					_L0_output.put("csvJsonString", reqDataInJson);
					_L0_output.put("compareFieldsMap", compareFieldsMap);
					_L0_output.put("xslJsonMap", xslJsonMap);
					 return _L0_output;
					// csvJsonString = reqDataInJson;s
				} else if (xlsxMap == null) {
					csvMap = CSVParser.parseFile(fileItem_fileDATA.getInputStream(), true);
					
					if (csvMap != null && csvMap.get("isEeidDuplicate") == null) {
						//PE-8690
						
						HashMap<String, Object> map = new HashMap<String, Object>();
						map.put("data", csvMap.get("allRecords"));
						ObjectMapper mapper = new ObjectMapper();
						csvJsonString = mapper.writeValueAsString(map);
					
					} else if (csvMap != null && csvMap.get("isEeidDuplicate") != null
							&& csvMap.get("isEeidDuplicate").equals("true")) {
						throw new CustomStatusException(Constants.HTTPSTATUS_400,
								Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
					}
					
					compareFieldsMap = formValidator.compareFields(
							(List<HashMap<String, String>>) csvMap.get("allRecords"),
							(List<String>) csvMap.get("header"), entitiesJson, null,csvMap.get("eeidColFlag"));
					if (compareFieldsMap != null && compareFieldsMap.get("resultNode") != null
							&& !compareFieldsMap.get("resultNode").equals(ErrorCacheConstant.ERR_055)) {
						downLoadData(response, compareFieldsMap.get("resultNode").toString().getBytes("UTF-8"),
								Constants.HTTPSTATUS_422);
						return null;
					}
				}

				// PE : 2449 to check duplication of EEID in excel sheet
				else if (xlsxMap != null
						&& (xlsxMap.get("isEeidDuplicate") == null || xlsxMap.get("isEeidDuplicate").equals("false"))) {
					compareFieldsMap = formValidator.compareFields(null, null, entitiesJson, xlsxMap,true);
					xslJsonMap = formValidator.prepareJsonString(xlsxMap);
				}
				// PE : 2449 to check duplication of EEID in csv
				else if (xlsxMap != null && xlsxMap.get("isEeidDuplicate") != null
						&& xlsxMap.get("isEeidDuplicate").equals("true")) {
					throw new CustomStatusException(Constants.HTTPSTATUS_400,
							Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
				}

				entitiesNode = objMapper.readValue(entitiesJson, ArrayNode.class);

				// For CSV data
				if (csvJsonString != null && !csvJsonString.equals("")) {
					if (entitiesNode.size() > 1) {
						// throw new
						// CustomStatusException(Constants.HTTPSTATUS_400,
						// Constants.ERROR_1_ENTITY_SHOULD_BE_IN_CSV_FILE);
						// throw new
						// CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_032));
						downLoadData(response, csvJsonString.getBytes("UTF-8"), Constants.HTTPSTATUS_422);
						return null;
					}
					// For .xlsx data : Throw exception if (No. of entities) !=
					// (No. of sheets)
				} else if (xslJsonMap != null) {
					if (compareFieldsMap != null && compareFieldsMap.get("resultNode") != null
							&& !compareFieldsMap.get("resultNode").equals(ErrorCacheConstant.ERR_055)) {
						downLoadData(response, compareFieldsMap.get("resultNode").toString().getBytes("UTF-8"),
								Constants.HTTPSTATUS_422);
						return null;
					}
					// logger.debug("response sent successfully..");)
					// Throw exception if No. of entities do not match No. of
					// .xlsx sheets
					if (entitiesNode.size() != xslJsonMap.size()) {
						paramValueList = new ArrayList<String>();
						paramValueList.add("" + entitiesNode.size());
						paramValueList.add("" + xslJsonMap.size());
						throw new CustomStatusException(Constants.HTTPSTATUS_400,
								Cache.getPropertyFromError(ErrorCacheConstant.ERR_033),
								Cache.getPropertyFromError(ErrorCacheConstant.ERR_033 + Constants.PARAMS),
								paramValueList);

					} else if (entitiesNode.size() > 1 && xslJsonMap.size() > 1) {
						// C1_US,C2_PL ; X1_XX, X2_YY

						xlsSheetList = new ArrayList<String>(xslJsonMap.keySet());

						entitiesList = new ArrayList<String>();
						for (int i = 0; i < entitiesNode.size(); i++) {
							contextDataNode = entitiesNode.get(i).get(Constants.CONTEXTDATAKEY);

							// coName_CtryCode =
							// contextDataNode.get("companyName").asText() + "_"
							// + contextDataNode.get("ctryCode").asText();

							coName_CtryCode.delete(0, coName_CtryCode.length());
							coName_CtryCode.append(contextDataNode.get("companyName").asText());
							coName_CtryCode.append("_");
							coName_CtryCode.append(contextDataNode.get("ctryCode").asText());
							entitiesList.add(coName_CtryCode.toString());
							// coName_CtryCode = null;
						}
						if (!xlsSheetList.containsAll(entitiesList)) {
							throw new CustomStatusException(Constants.HTTPSTATUS_400,
									Cache.getPropertyFromError(ErrorCacheConstant.ERR_037));
						}

					}
				}
				formValidator.performL0Validation(entitiesJson, csvJsonString, false, xslJsonMap,
						(String) compareFieldsMap.get("sheetMatchingFlag"));
				/*l0Message = formValidator.performL0Validation(entitiesJson, csvJsonString, false, xslJsonMap,
						(String) compareFieldsMap.get("sheetMatchingFlag"));*/
				/*if (!l0Message.equals("")) {
					throw new CustomStatusException(Constants.HTTPSTATUS_400, l0Message);
				}*/
			};
			_L0_output = new HashMap<String, Object>();
			_L0_output.put("compareFieldsMap", compareFieldsMap);
			_L0_output.put("csvJsonString", csvJsonString);
			_L0_output.put("xslJsonMap", xslJsonMap);
 		} catch (CustomStatusException cse) {
			errNode = JsonUtils.updateErrorNode(cse, errNode);
			try {
				downLoadData(response, errNode.toString().getBytes("UTF-8"), cse.getHttpStatus());
			} catch (UnsupportedEncodingException uee) {
				logger.error("UnsupportedEncodingException : "+uee.getMessage());	
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(" General exception caught" + e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			try {
				downLoadData(response, errNode.toString().getBytes("UTF-8"), Constants.HTTPSTATUS_500);
			} catch (UnsupportedEncodingException uee) {
				logger.error("UnsupportedEncodingException : "+uee.getMessage());	
			}
		}
		return _L0_output;
	}
}
