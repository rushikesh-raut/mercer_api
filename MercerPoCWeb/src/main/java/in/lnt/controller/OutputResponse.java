package in.lnt.controller;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

public class OutputResponse {
	
		void downLoadData(HttpServletResponse response, byte[] bs,int status) {
			InputStream is;
			try {
				is = new ByteArrayInputStream(bs);
				response.setStatus(status);
				// MIME type of the file
				response.setContentType("application/json");
				// Response header
				response.setHeader("Content-Disposition", "attachment; filename=\"output.json\"");

				// response.setHeader("Content-Disposition", "inline;
				// filename=\"output.json\"");
				// Read from the file and write into the response
				OutputStream os = response.getOutputStream();
				byte[] buffer = new byte[1024];
				int len;
				while ((len = is.read(buffer)) != -1) {
					os.write(buffer, 0, len);
				}
				os.flush();
				os.close();
				is.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

