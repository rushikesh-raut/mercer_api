package in.lnt.exception;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * 
 * @author Sarang Gandhi
 *
 */
@JsonRootName(value="Message Key")
public class CustomAPIException extends Exception  {

	private static final long serialVersionUID = 1L;

	private int  httpStatusCode; // status: the HTTP status code
	private String message; // message: the error message associated with exception
	private Throwable detail; // error: List of constructed error messages
	
	public CustomAPIException() {
		
	}
	public CustomAPIException(int statusCode, String errorMessage) {
		httpStatusCode = statusCode;
		message = errorMessage;
	}
	
	public CustomAPIException(int statusCode, Throwable statusDescription, String errorMessage) {
			httpStatusCode = statusCode;
			detail = statusDescription;
			message = errorMessage;
	}

	/**
	 * @return the _httpStatus
	 */
	public int getHttpStatus() {
		return httpStatusCode;
	}

	/**
	 * @param _httpStatus the httpStatusCode to set
	 */
	public void setHttpStatus(int httpStatus) {
		httpStatusCode = httpStatus;
	}

	/**
	 * @return the _message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the _message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the detail
	 */
	public Throwable getDetail() {
		return detail;
	}

	/**
	 * @param statusDescription the detail to set
	 */
	public void setDetail(Throwable statusDescription) {
		detail = statusDescription;
	}

	@Override
	/*public String toString(){
		return  getHttpStatus()+ ", "+getMessage()+", "+getDetail();
	}*/
	public String toString(){
		return  getHttpStatus()+ ", "+getMessage()+", "+getDetail();
	}

}