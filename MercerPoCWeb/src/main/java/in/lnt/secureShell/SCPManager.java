/*package in.lnt.secureShell;

//import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
import java.io.InputStream;
//import java.net.URL;

import javax.print.CancelablePrintJob;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import in.lnt.pojo.FileStorageInfo;
import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.Cache;

*//**
 * @author Nikhil Kshirsagar
 * @since 25-09-2017
 * @version 1.0 
 *//*
public class SCPManager {

	private static final Logger logger = LoggerFactory.getLogger(SCPManager.class);
	
	private JSch jSch;
	private String strUsername;
	private String strPassword;
	private String strIp;
	private Session sesConnection;
	private int intConnectionPort;
	private int intTimeOut;
    ChannelSftp sftpChannel;

	private static final int  TIMEOUT =  300000; 

	public SCPManager(String userName, String password, String connectionIP, String knownHostsFileName){
		doCommonConstructorActions(userName, password, connectionIP, knownHostsFileName);
		intConnectionPort = 22;
		intTimeOut = TIMEOUT;
	}
	
	private void doCommonConstructorActions(String userName, String password, String connectionIP, String knownHostsFileName) {
		jSch = new JSch();
		try {
			jSch.setKnownHosts(knownHostsFileName);
		} catch (JSchException jschX) {
			logger.info(jschX.getMessage());
		}
		
		strUsername = userName;
		strPassword = password;
		strIp = connectionIP;
	}
	
	public String connect() {
		String errorMessage = null;
		Channel channel = null;
		
		try {
			sesConnection = jSch.getSession(strUsername, strIp, intConnectionPort);

			// If HDFS_BYPASS_PASSWORD=true, then don't use password. If not specified or false, then set password.
			if(Cache.getProperty(CacheConstats.HDFS_BYPASS_PASSWORD)==null ||
					(Cache.getProperty(CacheConstats.HDFS_BYPASS_PASSWORD)!=null && Boolean.parseBoolean(Cache.getProperty(CacheConstats.HDFS_BYPASS_PASSWORD))==false)){
				sesConnection.setPassword(strPassword);
				logger.debug("HDFS_BYPASS_PASSWORD : "+Cache.getProperty(CacheConstats.HDFS_BYPASS_PASSWORD));
			}

			if(Cache.getProperty(CacheConstats.HDFS_STRICT_HOST_KEY_CHECKING)!=null && 
					(Cache.getProperty(CacheConstats.HDFS_STRICT_HOST_KEY_CHECKING).equals("no") || Cache.getProperty(CacheConstats.HDFS_STRICT_HOST_KEY_CHECKING).equals("yes")) ){
				sesConnection.setConfig("StrictHostKeyChecking", Cache.getProperty(CacheConstats.HDFS_STRICT_HOST_KEY_CHECKING));
				logger.debug("HDFS_STRICT_HOST_KEY_CHECKING : "+Cache.getProperty(CacheConstats.HDFS_STRICT_HOST_KEY_CHECKING));
			}
			
			sesConnection.connect(intTimeOut);
	        logger.info("Connected to SCP session successfully.");
	        
	        channel = sesConnection.openChannel("sftp");
	        channel.connect();
	        logger.info("Connected to SFTP Channel successfully.");
	        
	        sftpChannel = (ChannelSftp) channel;
	        
		} catch (JSchException jschX) {
			errorMessage = jschX.getMessage();
	        logger.info("Error connecting to SCP/SFTP Channel using Username:"+strUsername+", IP:"+strIp+", Port:"+intConnectionPort);
		}

		return errorMessage;
	}
	
	public void put(String destFilepath, InputStream inputStream, String fileName) throws SftpException {
		
//	    This will read file with the name test.txt and write to remote directory
		sftpChannel.cd(destFilepath);
		
//      For File : here you can also change the target file name by replacing f.getName() with your choice of name
		sftpChannel.put(inputStream, fileName);
	}
	
	
public InputStream get(String destFilepath, String  destFile) throws SftpException {
		
//	    This will read file with the name test.txt and write to remote directory
		sftpChannel.cd( destFilepath );
		
		return sftpChannel.get( destFile );
	}
	
	public void removeFile(String filepath) throws SftpException{
		sftpChannel.rm(filepath);
	}
	
	public boolean isPresent(String filepath) throws SftpException{
		if(null != sftpChannel.get(filepath)){
			return true;
		}
		return false;
	}
	
	public void close() {
		sesConnection.disconnect();
	}
	
}
*/