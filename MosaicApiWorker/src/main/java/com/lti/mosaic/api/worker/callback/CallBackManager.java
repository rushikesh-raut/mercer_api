package com.lti.mosaic.api.worker.callback;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.configs.Cache;
import in.lti.mosaic.api.base.configs.CacheConstants;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.exceptions.ExceptionsMessanger;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.loggers.ParamUtils;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;
import sun.misc.BASE64Encoder;

/**
 * 
 * @author aavesh
 *
 */
public class CallBackManager {

  private static final Logger LOGGER = LoggerFactory.getLogger(CallBackManager.class);
  private static final Integer maxRetryCallBack =
      Integer.valueOf(Cache.getProperty(CacheConstants.MAX_RETRY_CALL_BACK));
  private static final Integer waitTimeBetweenCallBack =
      Integer.valueOf(Cache.getProperty(CacheConstants.WAIT_TIME_BETN_CALL_BACK));

  /**
   * 
   * @param inputJson
   * @param urlString
   * @param requestType
   * @param checkCode
   * @param userName
   * @param password
   * @return
   * @throws Exception
   */
  public static HttpsURLConnection makeHttpsConnectionCallToRequest(String inputJson,
      String urlString, String requestType, String userName, String password, String apiKey)
      throws Exception {


    LOGGER.info("calling >> makeHttpConnectionCallToRequest"
        + ParamUtils.getString(inputJson, urlString, requestType));

    URL url;
    try {

      url = new URL(urlString);
      HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

      conn.setDoOutput(true);
      conn.setRequestMethod(requestType);
      conn.setRequestProperty(RequestConstants.CONTENT_TYPE, RequestConstants.APPLICATION_JSON);
      conn.addRequestProperty(RequestConstants.ACCEPT, RequestConstants.APPLICATION_JSON);
      conn.addRequestProperty("apikey", apiKey);

      if (null != userName || null != password) {

        String authString = userName + ":" + password;

        String encoding = new BASE64Encoder().encode(authString.getBytes());

        conn.setRequestProperty(RequestConstants.AUTH, RequestConstants.BASIC_AUTH + encoding);
      }


      if (inputJson != null && !inputJson.isEmpty()) {
        OutputStream os = conn.getOutputStream();
        os.write(inputJson.getBytes());
        os.flush();
      }
      return conn;
    } catch (Exception e) {
      LOGGER.error(LoggerConstants.LOG_MAXIQAPI + " << makeHttpConnectionCallToRequest()", e);
      e.printStackTrace();
      throw new Exception(e.getMessage());
    }
  }

  /**
   * 
   * @param bufferedReader
   * @return
   * @throws IOException
   */
  private static StringBuilder generateResponeStream(BufferedReader br) throws IOException {

    String output = "";
    StringBuilder outputString = new StringBuilder();
    while ((output = br.readLine()) != null) {
      outputString.append(output);
    }
    LOGGER.debug("The Web Response is " + outputString.toString());
    return outputString;
  }

  /**
   * 
   * @param inputJson
   * @param urlString
   * @param requestType
   * @throws Exception
   */
  public static void sendResponseViaCallBackUrl(String inputJson, String urlString,
      String requestType, String apiKey) throws Exception {

    int retryNo = 1;
    HttpsURLConnection connection =
        makeHttpsConnectionCallToRequest(inputJson, urlString, requestType, null, null, apiKey);

    if (connection.getResponseCode() == 500) {
      connection.disconnect();

      LOGGER.debug(LoggerConstants.LOG_MAXIQAPI + " << makeHttpConnectionCallToRequest()");

      while (retryNo <= maxRetryCallBack && connection.getResponseCode() == 500) {

        Thread.sleep(waitTimeBetweenCallBack);

        connection =
            makeHttpsConnectionCallToRequest(inputJson, urlString, requestType, null, null, apiKey);
        retryNo++;

      }

      if (connection.getResponseCode() != 500) {
        handleResponseNot500(connection);

      } else {
        LOGGER.error("Error while call back");
        connection.disconnect();
        ExceptionsMessanger.throwException(new SystemException(), "ERR_008", null);
      }

    } else {
      handleResponseNot500(connection);
    }
  }
  
  /**
   * 
   * @param connection
   * @throws Exception if response is not 200
   */
  private static void handleResponseNot500(HttpsURLConnection connection) throws Exception {

    BufferedReader br;

    if (connection.getResponseCode() == 400) {
      br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
      StringBuilder generateResponeStream = generateResponeStream(br);

      connection.disconnect();

      LOGGER.debug(LoggerConstants.LOG_MAXIQAPI + " << makeHttpConnectionCallToRequest()"
          + ParamUtils.getString(generateResponeStream));
      LOGGER.error("Status 400. Dead-letter");
      ExceptionsMessanger.throwException(new SystemException(), "ERR_007", null);

    } else if (connection.getResponseCode() == 200) {
      br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
      StringBuilder generateResponeStream = generateResponeStream(br);
      connection.disconnect();

      LOGGER.debug(LoggerConstants.LOG_MAXIQAPI + " << makeHttpConnectionCallToRequest()"
          + ParamUtils.getString(generateResponeStream));
    } else {
      LOGGER.error("Unknown Status {}",connection.getResponseCode());
      throw new SystemException("Unknown response code from call Back");
    }

  }

  /**
   * 
   * @param requestId
   * @param documentId
   * @param status
   * @return
   */
  public static String createCallBackJson(String requestId, String documentId, Integer status) {

    Map<String, Object> jsonMap = new HashMap<String, Object>();
    jsonMap.put(Constants.Request.RequestId, requestId);
    jsonMap.put(Constants.Request.DocumentId, documentId);
    jsonMap.put(Constants.Request.Status, status);

    String callBackJson = ObjectSerializationHandler.toString(jsonMap);

    return callBackJson;
  }

  /**
   * @param string
   * @return
   * @throws SystemException
   */
  public static String getCallBackUrlFromEnvironmentName(String string) throws SystemException {

    String callBackUrl = Cache.getProperty(string);

    if (null == callBackUrl) {
      ExceptionsMessanger.throwException(new SystemException(), "ERR_009", null);
    }
    return callBackUrl;
  }

  /**
   * @param string
   * @return
   * @throws SystemException
   */
  public static String getCallBackApiKeyFromEnvironmentName(String string) throws SystemException {

    String callBackApiKey = Cache.getProperty(string + "_API_KEY");

    if (null == callBackApiKey) {
      throw new SystemException("Cannot find call back apiKey");
    }
    return callBackApiKey;
  }

}
