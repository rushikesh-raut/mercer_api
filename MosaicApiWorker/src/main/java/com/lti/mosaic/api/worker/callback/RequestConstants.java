package com.lti.mosaic.api.worker.callback;

/**
 * 
 * @author rushi
 *
 */
public class RequestConstants {

  public static final String CONTENT_TYPE = "Content-Type";
  public static final String ACCEPT = "Accept";
  public static final String APPLICATION_JSON = "application/json";
  public static final String BASIC_AUTH = "Basic ";
  public static final String AUTH = "Authorization";
  public static final String POST = "POST";
  public static final String GET = "GET";

}
