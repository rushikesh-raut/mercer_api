package com.lti.mosaic.api.worker.callback;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;
import sun.misc.BASE64Encoder;

/**
 * @author rushikesh
 *
 */
public class TestCallBack {
  
  public static void main(String[] args) {
    

    String callBackJson =
        createCallBackJson("ui11218_123_abc_api", "5a570f88f2be1500186b5f8f", 200);

    System.out.println("Call Back Json is" + callBackJson);

    String callBackUrl;
    try {
      callBackUrl = args[0];

      System.out.println("Call back url is" + callBackUrl);

      String callBackApikey = args[1];

      System.out.println("Call back apikey is" + callBackApikey);

      sendResponseViaCallBackUrl(callBackJson, callBackUrl, RequestConstants.POST,
          callBackApikey);
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    System.out.println("All done");
  }
  
  private static void sendResponseViaCallBackUrl(String inputJson, String urlString,
      String requestType, String apiKey) throws Exception {

    int retryNo = 1;
    HttpsURLConnection connection =
        makeHttpsConnectionCallToRequest(inputJson, urlString, requestType, null, null, apiKey);

    if (connection.getResponseCode() == 500) {
      connection.disconnect();


      while (retryNo <= 3 && connection.getResponseCode() == 500) {

        Thread.sleep(1000);

        connection =
            makeHttpsConnectionCallToRequest(inputJson, urlString, requestType, null, null, apiKey);
        retryNo++;

      }

      if (connection.getResponseCode() != 500) {
        handleResponseNot500(connection);

      } else {
        connection.disconnect();
        throw new SystemException("Call Back URL Failed(500)");
      }

    } else {
      handleResponseNot500(connection);
    }
  }
  
  private static HttpsURLConnection makeHttpsConnectionCallToRequest(String inputJson,
      String urlString, String requestType, String userName, String password, String apiKey)
      throws Exception {


    URL url;
    try {

      url = new URL(urlString);
      HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

      conn.setDoOutput(true);
      conn.setRequestMethod(requestType);
      conn.setRequestProperty(RequestConstants.CONTENT_TYPE, RequestConstants.APPLICATION_JSON);
      conn.addRequestProperty(RequestConstants.ACCEPT, RequestConstants.APPLICATION_JSON);
      conn.addRequestProperty("apikey", apiKey);

      if (null != userName || null != password) {

        String authString = userName + ":" + password;

        String encoding = new BASE64Encoder().encode(authString.getBytes());

        conn.setRequestProperty(RequestConstants.AUTH, RequestConstants.BASIC_AUTH + encoding);
      }


      if (inputJson != null && !inputJson.isEmpty()) {
        OutputStream os = conn.getOutputStream();
        os.write(inputJson.getBytes());
        os.flush();
      }
      return conn;
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception(e.getMessage());
    }
  }
  
  private static void handleResponseNot500(HttpsURLConnection connection) throws Exception {

    BufferedReader br;

    if (connection.getResponseCode() == 400) {
      br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

      connection.disconnect();

      throw new SystemException("Call Back URL Failed(400)");

    } else if (connection.getResponseCode() == 200) {
      br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
      connection.disconnect();

    } else {
      throw new SystemException("Unknown response code from call Back");
    }

  }
  
  public static String createCallBackJson(String requestId, String documentId, Integer status) {

    Map<String, Object> jsonMap = new HashMap<String, Object>();
    jsonMap.put(Constants.Request.RequestId, requestId);
    jsonMap.put(Constants.Request.DocumentId, documentId);
    jsonMap.put(Constants.Request.Status, status);

    String callBackJson = ObjectSerializationHandler.toString(jsonMap);

    return callBackJson;
  }
}
