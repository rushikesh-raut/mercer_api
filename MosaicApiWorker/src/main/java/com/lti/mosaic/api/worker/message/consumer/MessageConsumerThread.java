package com.lti.mosaic.api.worker.message.consumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.api.worker.capacitymanager.CapacityManagerThread;
import com.lti.mosaic.api.worker.processor.CoreProcessor;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.configs.Cache;
import in.lti.mosaic.api.base.configs.CacheConstants;

/**
 * 
 * @author rushi
 *
 */
@SuppressWarnings("deprecation")
public class MessageConsumerThread implements Runnable {

  public static ConnectionFactory factory = new ConnectionFactory();
  public static String EXCHANGE_NAME = Cache.getProperty(CacheConstants.EXCHANGE_NAME);
  private static List<Address> addresses = new ArrayList<Address>();
  private String topic;

  private static final Logger logger_ = LoggerFactory.getLogger(MessageConsumerThread.class);

  public MessageConsumerThread(String topic) {
    super();
    this.topic = topic;
  }

  public static void init() throws Exception {

    factory.setUsername(Cache.getProperty(CacheConstants.RABBITMQ_USER_NAME));
    factory.setPassword(Cache.getProperty(CacheConstants.RABBITMQ_PASSWORD));

    for (String address : StringUtils.splitByWholeSeparatorPreserveAllTokens(
        Cache.getProperty(CacheConstants.RABBITMQ_ADRESSES), ",")) {

      Address addressOfRabbit = new Address(StringUtils.substringBefore(address, ":"),
          Integer.valueOf(StringUtils.substringAfter(address, ":")));

      addresses.add(addressOfRabbit);
    }
  }

  static {
    try {
      init();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void run() {
    try {
      Map<String, Object> argumentsForConsumer = new HashMap<String, Object>();
      //Max priority is set to 5
      argumentsForConsumer.put("x-max-priority", 5);

      Connection connection = factory.newConnection(addresses);
      Channel channel = connection.createChannel();

      // "quality of service" settings for this channel.
      channel.basicQos(100);

      try {
        channel.queueDeclare(topic, true, false, false, argumentsForConsumer);
        channel.exchangeDeclare(EXCHANGE_NAME, "direct", true);
      } catch (Exception e) {
        logger_.error("Error while declaring queue {}", e);
      }

      channel.queueBind(topic, EXCHANGE_NAME, topic);

      QueueingConsumer consumer = new QueueingConsumer(channel);

      channel.basicConsume(topic, false, "", false, false, null, consumer);
      boolean doContinue = true;
      while (doContinue) {
        if (CapacityManagerThread.getLoadIsBelowThreshold()) {
          QueueingConsumer.Delivery delivery = consumer.nextDelivery();
          String message = new String(delivery.getBody());
          channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
          CoreProcessor.process(message);
          Thread.sleep(100);
        } else {
          logger_.debug(LoggerConstants.LOG_MAXIQAPI + " worker is busy");
          Thread.sleep(1000);
        }
        
      }
      logger_.error("Error in consumer thread - all done ");
    } catch (Throwable e) {
      logger_.error("Error in consumer thread {}", e);
    }
  }
}
