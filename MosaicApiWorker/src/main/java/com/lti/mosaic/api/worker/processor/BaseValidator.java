package com.lti.mosaic.api.worker.processor;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lti.mosaic.api.worker.callback.CallBackManager;
import com.lti.mosaic.api.worker.callback.RequestConstants;

import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.LoggerConstants;
import in.lnt.utility.general.JsonUtils;
import in.lti.mosaic.api.base.bso.StatusBSO;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.constants.Constants.ValidationTypes;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;
import in.lti.mosaic.api.base.services.DatastoreServices;
import in.lti.mosaic.api.base.services.EventAPIService;

/**
 * @author rushikesh
 *
 */
public abstract class BaseValidator implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(L1L2Validator.class);

	private Map<String, String> inputMap;
	private String documentId = null;
	private Integer status = null;
	private String validationType;

	public Map<String, String> getInputMap() {
		return inputMap;
	}

	public void setInputMap(Map<String, String> inputMap) {
		this.inputMap = inputMap;
	}

	public BaseValidator(Map<String, String> map) {
		inputMap = map;
		validationType = map.get(Constants.Request.ValidationType);
	}

	public abstract JsonNode process(Object inputJsonNode) throws Exception;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		logger.info(LoggerConstants.LOG_MAXIQAPI, validationType);

		long documentDownloadedTime = 0;
		long processingTime = 0;
		long documentCreatedTime = 0;
		long startAsyncTime = System.currentTimeMillis();

		String requestId = inputMap.get(Constants.Request.RequestId);
		String documentIdFromRequest = inputMap.get(Constants.Request.DocumentId);
		String envName = inputMap.get(Constants.Request.EnvironmentName);
		try {
			// fetch document from DS using documentId
			String document = DatastoreServices.download(documentIdFromRequest);

			documentDownloadedTime = System.currentTimeMillis();
			StatusBSO.insertStatusForRequestId(requestId, validationType,
					Constants.RequestStatus.InputDocumentDownloaded, status, envName, documentIdFromRequest, null);

			JsonNode finalOutPut = null;

			try {
				JsonNode outPutOfValidations;

				if (StringUtils.equalsIgnoreCase(validationType, ValidationTypes.L3B)) {
					outPutOfValidations = process(document);

				} else {
					JsonNode entitiesNode = (JsonNode) ObjectSerializationHandler.toObject(document, JsonNode.class);
					outPutOfValidations = process(entitiesNode);
				}
				finalOutPut = outPutOfValidations;
				status = 200;

			} catch (CustomStatusException cse) {
				logger.error("Error while performing  {} {}", validationType, cse.getMessage());
				ObjectNode errNode = new ObjectMapper().createObjectNode();
				errNode = JsonUtils.updateErrorNodeForGenericException(errNode, cse);

				status = cse.getHttpStatus();
			} catch (Exception e) {
				logger.error("Error while performing  {} {}", validationType, e.getMessage());
				ObjectNode errNode = new ObjectMapper().createObjectNode();
				errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
				finalOutPut = errNode;
				status = 500;
			}
			processingTime = System.currentTimeMillis();
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.ProcessingFinished,
					status, envName, documentIdFromRequest, null);

			try {
				// Adding event As processing has been finished
				EventAPIService.addEvent(requestId, Constants.RequestStatus.ProcessingFinished);
			} catch (Exception e) {
				logger.error("Error while adding event {}", e.getMessage());
			}

			// Put the output in document Store
			documentId = DatastoreServices.writeDocs(ObjectSerializationHandler.toString(finalOutPut),
					requestId + envName);

			documentCreatedTime = System.currentTimeMillis();
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.OutputDocumentCreated,
					status, envName, documentId, null);

			String callBackJson = CallBackManager.createCallBackJson(requestId, documentId, status);

			// send response via CallBackManager Class

			String callBackUrl = CallBackManager.getCallBackUrlFromEnvironmentName(envName);
			String callBackApikey = CallBackManager.getCallBackApiKeyFromEnvironmentName(envName);
			try {

				CallBackManager.sendResponseViaCallBackUrl(callBackJson, callBackUrl, RequestConstants.POST,
						callBackApikey);

				StatusBSO.insertStatusForRequestId(requestId, validationType,
						Constants.RequestStatus.DeliveredSuccessFully, status, envName, documentId, null);

				// Adding event As processing has been delivered successfully
				try {
					EventAPIService.addEvent(requestId, Constants.RequestStatus.DeliveredSuccessFully);
				} catch (Exception e) {
					logger.error("Error while adding event {}", e.getMessage());
				}

			} catch (Exception e) {

				logger.error("Error while performing  {} {}", validationType, e.getMessage());

				StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeadLetter,
						status, envName, documentId, e.getMessage());

				// Adding event As processing for deadletter
				try {

					EventAPIService.addEvent(requestId, Constants.RequestStatus.DeadLetter);
				} catch (Exception exc) {
					logger.error("Error while performing  {} {}", validationType, exc.getMessage());
				}
			}

		} catch (Exception e) {
			logger.error("Error while performing  {} {}", validationType, e.getMessage());

			if (null == documentId) {
				documentId = inputMap.get(Constants.Request.DocumentId);
			}
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.ERROR, status,
					envName, documentId, e.getMessage());

		}

		logger.error("Info: RequestId is {} Document Downloaded Time (Millis): {}  ", requestId,
				(documentDownloadedTime - startAsyncTime));
		logger.error("Info: RequestId is {} API Async Processing Time (Millis): {}  ", requestId,
				(processingTime - documentDownloadedTime));
		logger.error("Info: RequestId is {} Document Pushed Time (Millis): {}  ", requestId,
				(documentCreatedTime - processingTime));

	}

}