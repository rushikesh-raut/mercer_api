package com.lti.mosaic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;
import in.lti.mosaic.api.base.services.DatastoreServices;
import com.lti.mosaic.api.worker.processor.CoreProcessor;

public class TestWriteDocs {

  @Test
  public void test() throws IOException, SystemException {
    String data = "{'menu': { 'id': 'file', 'value': 'File', 'popup': { 'menuitem': [ {'value': 'NewNewNew', 'onclick': 'NewCreateNewDoc()'}, {'value': 'Open', 'onclick': 'OpenDoc()'}, {'value': 'Close', 'onclick': 'CloseDoc()'} ] } }}";;
    String fileName = "balu.json";
    String download = DatastoreServices.writeDocs(data, fileName);
    System.out.println(download);
  }
  
  @Test
  public void testReadFromFileAndWriteIntoDocsStore() throws IOException, SystemException {
    String fileName = "request.json";

    String data = readFile("/home/pcadmin/Desktop/Mercersss/op.json");
    String download = DatastoreServices.writeDocs(data, fileName);
    System.out.println(download);
  }
  
  public static void main1(String[] args) throws IOException {
    CoreProcessor l1 = new CoreProcessor();

    Map<String, String> inputMap = new HashMap<String, String>();
    inputMap.put(Constants.Request.EnvironmentName, "DEV");
    //Working
    inputMap.put(Constants.Request.DocumentId, "5a537a61f2be1500186b5ed5");
    //Empty Body
    inputMap.put(Constants.Request.DocumentId, "5a548497f2be1500186b5ef6");
    inputMap.put(Constants.Request.RequestId, "api_uuis1_11");
    inputMap.put(Constants.Request.ValidationType, "L3b");

    l1.process(ObjectSerializationHandler.toString(inputMap));
  }

  private static String readFile(String file) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(file));
    String line = null;
    StringBuilder stringBuilder = new StringBuilder();
    String ls = System.getProperty("line.separator");

    try {
      while ((line = reader.readLine()) != null) {
        stringBuilder.append(line);
        stringBuilder.append(ls);
      }

      return stringBuilder.toString();
    } finally {
      reader.close();
    }
  }

}
