package com.lti.mosaic.event;

import java.io.IOException;

import org.junit.Test;

import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.services.EventAPIService;

public class TestAddEvent {

	@Test
	public void test() throws IOException {
		String data = "{ \"event\":{\"AppKey\":\"JLdqHF6y1peFKjZXNjh57cMA7GFNNaJU\", \"Source\": [], \"EventType\":\"Async_progress\", \"AppEventType\":\"\", \"EventBody\":{ \"request_id\":\"uuid_385343\",\"status\":\"3-Picked-up-for-processing\" }} }";
		String request_id="uuid_385343";
		String status=Constants.RequestStatus.PickedUpFroProcessing;
		String addEventMessage = EventAPIService.addEvent(request_id, status);
		System.out.println(addEventMessage);
	}
}
